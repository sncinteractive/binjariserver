<?php
	header("Content-Type:text/html; charset=utf-8");
	
	try {
		require_once ("../inc/config.php");
		require_once_classes(array("CMysqlManager"));
		require_once_services(array("CMemberService", "CAddressService", "COKNameService"));
	
		$mysql_manager = new CMysqlManager();
		$memberService = new CMemberService($mysql_manager->getDb());
		$addressService = new CAddressService($mysql_manager->getDb());
		$okNameService = new COKNameService($mysql_manager->getDb());
		
		$params = json_decode(file_get_contents('php://input'), true, 5, JSON_UNESCAPED_UNICODE);
	
		$apiType = isset($params["apiType"]) ? $params["apiType"] : "";
		$token = isset($params["token"]) ? $params["token"] : "";
	
		if(!isset($apiType) || empty($apiType)) {
			echo snc_return($ERROR_CODES["NOT_ENOUGH_PARAM"]["code"], $ERROR_CODES["NOT_ENOUGH_PARAM"]["message"], null);
			exit;
		}
	
		if(empty($token) || !$memberService->isValidToken($token)) {
			$token = "";
		}
		
		if($apiType == "check_valid_token") {
			if(empty($token)) {
				echo snc_return($ERROR_CODES["INVALID_TOKEN"]["code"], $ERROR_CODES["INVALID_TOKEN"]["message"], null);
				exit;
			}
			
			echo snc_return($ERROR_CODES["SUCCESS"]["code"], $ERROR_CODES["SUCCESS"]["message"], null);
			exit;
		} else if($apiType == "member_join") {
			$memberAccountId = $params["memberAccountId"];
			$memberAccountPw = $params["memberAccountPw"];
			$email = $params["email"];
			$nickName = $params["nickName"];
			$phoneNumber = $params["phoneNumber"];
			$memberTpCd = $params["memberTpCd"];
			$memberDesc = "사용자";
			$memberPhoneCertyn = "Y";
			$userRealNm = $params["userRealNm"];
			$userCi = $params["userCi"];
			$userDi = $params["userDi"];
			$gcmId = isset($params["gcmId"]) ? $params["gcmId"] : "";
			
			$memberId = $memberService->join($memberAccountId, $memberAccountPw, $email, $nickName, $phoneNumber, $memberTpCd, $memberDesc, $memberPhoneCertyn, $userRealNm, $userCi, $userDi, $gcmId);
			
			echo snc_return($ERROR_CODES["SUCCESS"]["code"], $ERROR_CODES["SUCCESS"]["message"], null);
			exit;
		} else if($apiType == "member_manager_join") {
			$memberAccountId = $params["memberAccountId"];
			$memberAccountPw = $params["memberAccountPw"];
			$email = $params["email"];
			$nickName = $params["nickName"];
			$phoneNumber = $params["phoneNumber"];
			$memberTpCd = $params["memberTpCd"];
			$memberDesc = "매니저";
			$memberPhoneCertyn = "Y";
			$shopName = $params["shopName"];
			$shopOwnerName = $params["shopOwnerName"];
			$shopAddress = $params["shopAddress"];
			$shopPostCode = $params["shopPostCode"];
			$shopLat = $params["shopLat"];
			$shopLng = $params["shopLng"];
			$policy_1 = $params["policy_1"];
			$policy_2 = $params["policy_2"];
			$policy_3 = $params["policy_3"];
			$policy_4 = $params["policy_4"];
			$policy_5 = $params["policy_5"];
			$userRealNm = $params["userRealNm"];
			$userCi = $params["userCi"];
			$userDi = $params["userDi"];
			$gcmId = isset($params["gcmId"]) ? $params["gcmId"] : "";
				
			$memberId = $memberService->managerJoin($memberAccountId, $memberAccountPw, $email, $nickName, $phoneNumber, $memberTpCd, $memberDesc, $memberPhoneCertyn, 
							$shopName, $shopOwnerName, $shopAddress, $shopPostCode, $shopLat, $shopLng, $policy_1, $policy_2, $policy_3, $policy_4, $policy_5, $userRealNm, $userCi, $userDi, $gcmId);
				
			echo snc_return($ERROR_CODES["SUCCESS"]["code"], $ERROR_CODES["SUCCESS"]["message"], null);
			exit;
		} else if($apiType == "check_duplicate_id") {
			$memberAccountId = $params["memberAccountId"];
			
			if($memberService->checkDuplicateId($memberAccountId)) {
				echo snc_return($ERROR_CODES["DUPLICATE_ID"]["code"], $ERROR_CODES["DUPLICATE_ID"]["message"], null);
			} else {
				echo snc_return($ERROR_CODES["SUCCESS"]["code"], $ERROR_CODES["SUCCESS"]["message"], null);
			}
			
			exit;
		} else if($apiType == "login") {
			$memberAccountId = $params["memberAccountId"];
			$memberAccountPw = $params["memberAccountPw"];
			$platform = isset($params["platform"]) ? $params["platform"] : "";
			$platformVersion = isset($params["platformVersion"]) ? $params["platformVersion"] : "";
			
			$ret = $memberService->login($memberAccountId, $memberAccountPw, $platform, $platformVersion);
			
			echo snc_return($ERROR_CODES["SUCCESS"]["code"], $ERROR_CODES["SUCCESS"]["message"], $ret);
			exit;
		} else if($apiType == "manager_login") {
			$memberAccountId = $params["memberAccountId"];
			$memberAccountPw = $params["memberAccountPw"];
			$platform = isset($params["platform"]) ? $params["platform"] : "";
			$platformVersion = isset($params["platformVersion"]) ? $params["platformVersion"] : "";
				
			$ret = $memberService->managerLogin($memberAccountId, $memberAccountPw, $platform, $platformVersion);
				
			echo snc_return($ERROR_CODES["SUCCESS"]["code"], $ERROR_CODES["SUCCESS"]["message"], $ret);
			exit;
		} else if($apiType == "searchId") {
			$email = $params["email"];
			$phoneNumber = $params["phoneNumber"];
			
			$memberInfo = $memberService->getMemberInfoByEmailPhoneNumber($email, $phoneNumber);
			
			if(!isset($memberInfo)) {
				echo snc_return($ERROR_CODES["FAIL_TO_SEARCH_ID"]["code"], $ERROR_CODES["FAIL_TO_SEARCH_ID"]["message"], null);
			} else {
				echo snc_return($ERROR_CODES["SUCCESS"]["code"], $ERROR_CODES["SUCCESS"]["message"], $memberInfo);
			}
		} else if($apiType == "searchPw") {
			$memberAccountId = $params["memberAccountId"];
			$phoneNumber = $params["phoneNumber"];
				
			$memberInfo = $memberService->getMemberInfoByIdPhoneNumber($memberAccountId, $phoneNumber);
				
			if(!isset($memberInfo)) {
				echo snc_return($ERROR_CODES["FAIL_TO_SEARCH_ID"]["code"], $ERROR_CODES["FAIL_TO_SEARCH_ID"]["message"], null);
			} else {
				echo snc_return($ERROR_CODES["SUCCESS"]["code"], $ERROR_CODES["SUCCESS"]["message"], $memberInfo);
			}
		} else if($apiType == "savePassword") {
			$memberId = $params["memberId"];
			$phoneNumber = $params["phoneNumber"];
			$memberAccountPwd = $params["memberAccountPwd"];
			
			$ret = $memberService->saveNewPassword($memberId, $phoneNumber, $memberAccountPwd);
			
			if(!$ret) {
				echo snc_return($ERROR_CODES["FAIL_TO_SAVE_PWD"]["code"], $ERROR_CODES["FAIL_TO_SAVE_PWD"]["message"], null);
			} else {
				echo snc_return($ERROR_CODES["SUCCESS"]["code"], $ERROR_CODES["SUCCESS"]["message"], null);
			}
		} else if($apiType == "search_manager_id") {
			$nickName = $params["nickName"];
			$phoneNumber = $params["phoneNumber"];
				
			$memberInfo = $memberService->getMemberInfoByNickNamePhoneNumber($nickName, $phoneNumber);
				
			if(!isset($memberInfo)) {
				echo snc_return($ERROR_CODES["FAIL_TO_SEARCH_ID"]["code"], $ERROR_CODES["FAIL_TO_SEARCH_ID"]["message"], null);
			} else {
				echo snc_return($ERROR_CODES["SUCCESS"]["code"], $ERROR_CODES["SUCCESS"]["message"], $memberInfo);
			}
		} else if($apiType == "recv_coupon") {
			if(empty($token)) {
				echo snc_return($ERROR_CODES["INVALID_TOKEN"]["code"], $ERROR_CODES["INVALID_TOKEN"]["message"], null);
				exit;
			}
				
			$memberId = $memberService->getMemberIdByToken($token);
		
			$shopId = $params["shopId"];
			$couponId = $params["couponId"];
		
			$my_history_list = $shopService->getMyHistoryList($memberId, $startDate, $endDate);
		
			echo snc_return($ERROR_CODES["SUCCESS"]["code"], $ERROR_CODES["SUCCESS"]["message"], $my_history_list);
			exit;
		} else if($apiType == "search_customer") {
			if(empty($token)) {
				echo snc_return($ERROR_CODES["INVALID_TOKEN"]["code"], $ERROR_CODES["INVALID_TOKEN"]["message"], null);
				exit;
			}
			
			$searchType = $params["searchType"];
			$searchText = $params["searchText"];
			
			$searchList = $memberService->searchCustomer($searchType, $searchText);
			
			echo snc_return($ERROR_CODES["SUCCESS"]["code"], $ERROR_CODES["SUCCESS"]["message"], $searchList);
			exit;
		} else if($apiType == "member_detail") {
			if(empty($token)) {
				echo snc_return($ERROR_CODES["INVALID_TOKEN"]["code"], $ERROR_CODES["INVALID_TOKEN"]["message"], null);
				exit;
			}
			
			$memberId = $params["memberId"];
			
			$memberInfo = $memberService->getMemberInfo($memberId);
			
			echo snc_return($ERROR_CODES["SUCCESS"]["code"], $ERROR_CODES["SUCCESS"]["message"], $memberInfo);
			exit;
		} else if($apiType == "get_temp_address") {
			$token = $params["addressToken"];
			
			$addressData = $addressService->getTempAddress($token);
			
			echo snc_return($ERROR_CODES["SUCCESS"]["code"], $ERROR_CODES["SUCCESS"]["message"], $addressData);
			exit;
		} else if($apiType == "get_temp_phone_auth") {
			$token = $params["phoneAuthToken"];
				
			$phoneAuthData = $okNameService->getTempPhoneAuth($token);
				
			echo snc_return($ERROR_CODES["SUCCESS"]["code"], $ERROR_CODES["SUCCESS"]["message"], $phoneAuthData);
			exit;
		}
	} catch (Exception $e) {
		debug_mesg("E", __CLASS__, __FUNCTION__, __LINE__, "api member error : " . $e->getMessage());
	
		if(getErrorCode($e->getMessage()) != null) {
			echo snc_return($ERROR_CODES[$e->getMessage()]["code"], $ERROR_CODES[$e->getMessage()]["message"], null);
		} else {
			echo snc_return($ERROR_CODES["SERVER_INTERNAL_ERROR"]["code"], $ERROR_CODES["SERVER_INTERNAL_ERROR"]["message"], null);
		}
		exit;
	}
?>