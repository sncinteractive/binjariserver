<?php
	header("Content-Type:text/html; charset=utf-8");
	
	try {
		require_once ("../inc/config.php");
		require_once_classes(array("CMysqlManager"));
		require_once_services(array("CMemberService", "CShopService", "CCustomerService"));
		
		$mysql_manager = new CMysqlManager();
		$memberService = new CMemberService($mysql_manager->getDb());
		$shopService = new CShopService($mysql_manager->getDb());
		$custoemrService = new CCustomerService($mysql_manager->getDb());
	
		$params = json_decode(file_get_contents('php://input'), true, 5, JSON_UNESCAPED_UNICODE);
	
		$apiType = isset($params["apiType"]) ? $params["apiType"] : "";
		$token = isset($params["token"]) ? $params["token"] : "";
	
		if(!isset($apiType) || empty($apiType)) {
			echo snc_return($ERROR_CODES["NOT_ENOUGH_PARAM"]["code"], $ERROR_CODES["NOT_ENOUGH_PARAM"]["message"], null);
			exit;
		}
	
		if(empty($token) || !$memberService->isValidToken($token)) {
			$token = "";
		}
	
		if($apiType == "shop_list") {
			$shopListType = $params["shopListType"];
			$lat = $params["lat"];
			$lng = $params["lng"];
			$shopLocCd = $params["shopLocCd"];
			$range = isset($params["range"]) ? $params["range"] : DEFAULT_SHOP_RANGE;
			$page = isset($params["page"]) ? $params["page"] : 1;
			
			$retShopList = array();
			if($shopListType == "distance") {
				$retShopList = $shopService->getShopListByDistance($lat, $lng, $shopLocCd, $range, $page);
			} else if($shopListType == "recommend") {
				$retShopList = $shopService->getShopListByRecommend($shopLocCd, $page);
			} else if($shopListType == "populate") {
				$retShopList = $shopService->getShopListByPopulate($shopLocCd, $page);
			}
			
			echo snc_return($ERROR_CODES["SUCCESS"]["code"], $ERROR_CODES["SUCCESS"]["message"], $retShopList);
			exit;
		} else if($apiType == "shop_detail") {
			$shopId = $params["shopId"];
			
			// 매장 기본 정보
			$retShopDetail = $shopService->getShopDetail($shopId);
			if(!isset($retShopDetail)) {
				echo snc_return($ERROR_CODES["INVALID_SHOP_ID"]["code"], $ERROR_CODES["INVALID_SHOP_ID"]["message"], null);
				exit;
			}
			
			// 매장 상품 리스트 조회
			$retShopPdtList = $shopService->getShopProductList($shopId);
			
			// 매장 포트폴리오 리스트 조회
			$retShopPortfolioList = $shopService->getShopPortfolioList($shopId, 3);
			
			// 매장 방문 후기 리스트 조회
			$retShopReviewList = $shopService->getShopReviewList($shopId);
			
			// 매장 진행중인 이벤트 리스트 조회 
			$retShopEventList = $shopService->getShopEventList($shopId);
			
			// 매장에서 사용가능한 쿠폰 리스트 조회
			$retShopCouponList = $shopService->getShopCouponList($shopId);
			
			$retData = array(
				"shopDetail" => $retShopDetail,
				"shopPdtList" => $retShopPdtList,
				"shopPortfolioList" => $retShopPortfolioList,
				"shopReviewList" => $retShopReviewList,
				"shopEventList" => $retShopEventList,
				"shopCouponList" => $retShopCouponList
			);
			
			echo snc_return($ERROR_CODES["SUCCESS"]["code"], $ERROR_CODES["SUCCESS"]["message"], $retData);
			exit;
		} else if($apiType == "add_favorite_shop") {
			if(empty($token)) {
				echo snc_return($ERROR_CODES["INVALID_TOKEN"]["code"], $ERROR_CODES["INVALID_TOKEN"]["message"], null);
				exit;
			}
			
			$shopId = $params["shopId"];
			$memberId = $memberService->getMemberIdByToken($token);
			
			$shopService->addFavoriteShop($memberId, $shopId);
			
			echo snc_return($ERROR_CODES["SUCCESS"]["code"], $ERROR_CODES["SUCCESS"]["message"], null);
			exit;
		} else if($apiType == "shop_portfolio") {
			$shopId = $params["shopId"];
			
			// 매장 기본 정보
			$retShopDetail = $shopService->getShopDetail($shopId);
			if(!isset($retShopDetail)) {
				echo snc_return($ERROR_CODES["INVALID_SHOP_ID"]["code"], $ERROR_CODES["INVALID_SHOP_ID"]["message"], null);
				exit;
			}
			
			// 매장 포트폴리오 리스트 조회
			$retShopPortfolioList = $shopService->getShopPortfolioList($shopId, null);
			
			$retData = array(
				"shopDetail" => $retShopDetail,
				"shopPortfolioList" => $retShopPortfolioList
			);
			
			echo snc_return($ERROR_CODES["SUCCESS"]["code"], $ERROR_CODES["SUCCESS"]["message"], $retData);
			exit;
		} else if($apiType == "portfolio_detail") {
			$portId = $params["portId"];
			
			// 포트폴리오 상세 조회
			$retPortfolioDetail = $shopService->getPortfolioDetail($portId);
			
			// 매장 기본 정보
			$retShopDetail = $shopService->getShopDetail($retPortfolioDetail["SHOP_ID"]);
			if(!isset($retShopDetail)) {
				echo snc_return($ERROR_CODES["INVALID_SHOP_ID"]["code"], $ERROR_CODES["INVALID_SHOP_ID"]["message"], null);
				exit;
			}
			
			// 포트폴리오 해시 태그 리스트 조회
			$retPortfolioHashList = $shopService->getPortfolioHashTagList($portId);
			
			$retData = array(
				"portfolioDetail" => $retPortfolioDetail,
				"portfolioHashList" => $retPortfolioHashList,
				"shopDetail" => $retShopDetail
			);
			
			echo snc_return($ERROR_CODES["SUCCESS"]["code"], $ERROR_CODES["SUCCESS"]["message"], $retData);
			exit;
		} else if($apiType == "portfolio_list_by_hashtag") {
			$shopId = $params["shopId"];
			$tagNm = $params["tagNm"];
			
			// 매장 기본 정보
			$retShopDetail = $shopService->getShopDetail($shopId);
			if(!isset($retShopDetail)) {
				echo snc_return($ERROR_CODES["INVALID_SHOP_ID"]["code"], $ERROR_CODES["INVALID_SHOP_ID"]["message"], null);
				exit;
			}
			
			// 해시 태그로 포트폴리오 리스트 조회
			$retPortfolioList = $shopService->getPortfolioListByHashTag($tagNm);
			
			$retData = array(
				"portfolioList" => $retPortfolioList,
				"shopDetail" => $retShopDetail
			);
			
			echo snc_return($ERROR_CODES["SUCCESS"]["code"], $ERROR_CODES["SUCCESS"]["message"], $retData);
			exit;
		} else if($apiType == "save_review") {
			if(empty($token)) {
				echo snc_return($ERROR_CODES["INVALID_TOKEN"]["code"], $ERROR_CODES["INVALID_TOKEN"]["message"], null);
				exit;
			}
			
			$shopId = $params["shopId"];
			$starCount = $params["starCount"];
			$title = $params["title"];
			$content = $params["content"];
			
			$memberId = $memberService->getMemberIdByToken($token);
			
			$shopService->addReview($memberId, $shopId, $title, $content, $starCount);
			
			echo snc_return($ERROR_CODES["SUCCESS"]["code"], $ERROR_CODES["SUCCESS"]["message"], null);
			exit;
		} else if($apiType == "shop_list_by_event") {
			$evtId = $params["evtId"];
			
			$retEventDetail = $shopService->getEventDetail($evtId);
			
			$retShopList = $shopService->getShopListByEventId($evtId);
			
			$retData = array(
				"eventDetail" => $retEventDetail,
				"shopList" => $retShopList
			);
			
			echo snc_return($ERROR_CODES["SUCCESS"]["code"], $ERROR_CODES["SUCCESS"]["message"], $retData);
			exit;
		} else if($apiType == "favorite_shop_list") {
			if(empty($token)) {
				echo snc_return($ERROR_CODES["INVALID_TOKEN"]["code"], $ERROR_CODES["INVALID_TOKEN"]["message"], null);
				exit;
			}
			
			$memberId = $memberService->getMemberIdByToken($token);
			
			$retShopList = $shopService->getFavoriteShopList($memberId);
			
			echo snc_return($ERROR_CODES["SUCCESS"]["code"], $ERROR_CODES["SUCCESS"]["message"], $retShopList);
			exit;
		} else if($apiType == "shop_coupon_list") {
			if(empty($token)) {
				echo snc_return($ERROR_CODES["INVALID_TOKEN"]["code"], $ERROR_CODES["INVALID_TOKEN"]["message"], null);
				exit;
			}
			
			$memberId = $memberService->getMemberIdByToken($token);
			
			$shopId = $params["shopId"];
			
			// 매장 기본 정보
			$retShopDetail = $shopService->getShopDetail($shopId);
			if(!isset($retShopDetail)) {
				echo snc_return($ERROR_CODES["INVALID_SHOP_ID"]["code"], $ERROR_CODES["INVALID_SHOP_ID"]["message"], null);
				exit;
			}
			
			$couponList = $shopService->getMyCouponListByShopId($shopId, $memberId);
			
			$ret = array(
				"shopDetail" => $retShopDetail,
				"couponList" => $couponList
			);
			
			echo snc_return($ERROR_CODES["SUCCESS"]["code"], $ERROR_CODES["SUCCESS"]["message"], $ret);
			exit;
		} else if($apiType == "recv_coupon") {
			if(empty($token)) {
				echo snc_return($ERROR_CODES["INVALID_TOKEN"]["code"], $ERROR_CODES["INVALID_TOKEN"]["message"], null);
				exit;
			}
				
			$memberId = $memberService->getMemberIdByToken($token);
				
			$shopId = $params["shopId"];
			$shopCouponId = $params["shopCouponId"];
				
			$custoemrService->sendCoupon($shopId, $memberId, $shopCouponId);
				
			echo snc_return($ERROR_CODES["SUCCESS"]["code"], $ERROR_CODES["SUCCESS"]["message"], null);
			exit;
		} else if($apiType == "coupon_list") {
			if(empty($token)) {
				echo snc_return($ERROR_CODES["INVALID_TOKEN"]["code"], $ERROR_CODES["INVALID_TOKEN"]["message"], null);
				exit;
			}
				
			$memberId = $memberService->getMemberIdByToken($token);
				
			$couponList = $shopService->getMyCouponList($memberId);
				
			echo snc_return($ERROR_CODES["SUCCESS"]["code"], $ERROR_CODES["SUCCESS"]["message"], $couponList);
			exit;
		} else if($apiType == "booking_info") {
			if(empty($token)) {
				echo snc_return($ERROR_CODES["INVALID_TOKEN"]["code"], $ERROR_CODES["INVALID_TOKEN"]["message"], null);
				exit;
			}
			
			$memberId = $memberService->getMemberIdByToken($token);
			
			$shopId = $params["shopId"];
			
			$couponList = $shopService->getMyCouponListByShopId($shopId, $memberId);
			
			$ret = array(
				"couponList" => $couponList
			);
			
			echo snc_return($ERROR_CODES["SUCCESS"]["code"], $ERROR_CODES["SUCCESS"]["message"], $ret);
			exit;
		} else if($apiType == "shop_pdt_list") {
			$shopId = $params["shopId"];
			
			// 매장 상품 리스트 조회
			$retShopPdtList = $shopService->getShopProductList($shopId);
			
			echo snc_return($ERROR_CODES["SUCCESS"]["code"], $ERROR_CODES["SUCCESS"]["message"], $retShopPdtList);
			exit;
		} else if($apiType == "shop_booking_list") {
			if(empty($token)) {
				echo snc_return($ERROR_CODES["INVALID_TOKEN"]["code"], $ERROR_CODES["INVALID_TOKEN"]["message"], null);
				exit;
			}
			
			$shopId = $params["shopId"];
			
			$shopBookingList = $shopService->getShopBookingList($shopId);
			
			$retShopBookingList = array();
			foreach ($shopBookingList as $row) {
				$yearsmonthdate = substr($row["ORDER_DT"], 0, 8);
				
				if(!isset($retShopBookingList[$yearsmonthdate])) {
					$retShopBookingList[$yearsmonthdate] = array();
				}
				
				$retShopBookingList[$yearsmonthdate][] = $row;
			}
			
			$shopManagerList = $shopService->getShopManagerList($shopId);
			
			$availableBookingDate = array();
			for($i = 0; $i <= 14; $i++) {
				$availableBookingDate[] = array(
					"month" => date("Y.m", strtotime("+" . $i . " days")),
					"date" => date("d", strtotime("+" . $i . " days"))
				);
			}
			
			$ret = array(
				"shopManagerList" => $shopManagerList,
				"shopBookingList" => $retShopBookingList,
				"todayMonth" => date("Y.m"),
				"availableBookingDate" => $availableBookingDate
			);
			
			echo snc_return($ERROR_CODES["SUCCESS"]["code"], $ERROR_CODES["SUCCESS"]["message"], $ret);
			exit;
		} else if($apiType == "shop_coupon_list") {
			if(empty($token)) {
				echo snc_return($ERROR_CODES["INVALID_TOKEN"]["code"], $ERROR_CODES["INVALID_TOKEN"]["message"], null);
				exit;
			}
			
			$shopId = $params["shopId"];
			
			$shopCouponList = $shopService->getShopCouponList($shopId);
			
			echo snc_return($ERROR_CODES["SUCCESS"]["code"], $ERROR_CODES["SUCCESS"]["message"], $shopCouponList);
			exit;
		} else if($apiType == "booking") {
			if(empty($token)) {
				echo snc_return($ERROR_CODES["INVALID_TOKEN"]["code"], $ERROR_CODES["INVALID_TOKEN"]["message"], null);
				exit;
			}
			
			$memberId = $memberService->getMemberIdByToken($token);
			$pdtId = $params["pdtId"];
			$managerId = $params["managerId"];
			$orderDt = $params["orderDt"];
			$orderGubunCd = $params["orderGubunCd"];
			$shopId = $params["shopId"];
			$shopCoupId = isset($params["shopCoupId"]) ? $params["shopCoupId"] : "";
			$fromBooking = "customer";
			
			$orderId = $shopService->saveBooking($memberId, $shopId, $pdtId, $managerId, $orderDt, $orderGubunCd, "", "", $shopCoupId, $fromBooking);
			
			echo snc_return($ERROR_CODES["SUCCESS"]["code"], $ERROR_CODES["SUCCESS"]["message"], array("orderId" => $orderId));
			exit;
		} else if($apiType == "offline_booking") {
			if(empty($token)) {
				echo snc_return($ERROR_CODES["INVALID_TOKEN"]["code"], $ERROR_CODES["INVALID_TOKEN"]["message"], null);
				exit;
			}
				
			$memberId = $params["memberId"];
			$userNm = $params["userNm"];
			$userPhoneNumber = $params["userPhoneNumber"];
			$pdtId = $params["pdtId"];
			$managerId = $params["managerId"];
			$orderDt = $params["orderDt"] . "00";
			$orderGubunCd = $params["orderGubunCd"];
			$shopId = $memberService->getShopIdByToken($token);
			$couponId = isset($params["couponId"]) ? $params["couponId"] : "";
			$fromBooking = "manager";
				
			$orderId = $shopService->saveBooking($memberId, $shopId, $pdtId, $managerId, $orderDt, $orderGubunCd, $userNm, $userPhoneNumber, $couponId, $fromBooking);
				
			echo snc_return($ERROR_CODES["SUCCESS"]["code"], $ERROR_CODES["SUCCESS"]["message"], array("orderId" => $orderId));
			exit;
		} else if($apiType == "edit_booking") {
			if(empty($token)) {
				echo snc_return($ERROR_CODES["INVALID_TOKEN"]["code"], $ERROR_CODES["INVALID_TOKEN"]["message"], null);
				exit;
			}
		
			$userId = $params["userId"];
			$bookingId = $params["bookingId"];
			$pdtId = $params["pdtId"];
			$managerId = $params["managerId"];
			$orderDt = $params["orderDt"];
			$shopId = $memberService->getShopIdByToken($token);
			$couponId = isset($params["couponId"]) ? $params["couponId"] : "";
		
			$orderId = $shopService->editBooking($userId, $bookingId, $shopId, $pdtId, $managerId, $orderDt, $couponId);
		
			echo snc_return($ERROR_CODES["SUCCESS"]["code"], $ERROR_CODES["SUCCESS"]["message"], null);
			exit;
		} else if($apiType == "booking_detail") {
			if(empty($token)) {
				echo snc_return($ERROR_CODES["INVALID_TOKEN"]["code"], $ERROR_CODES["INVALID_TOKEN"]["message"], null);
				exit;
			}
			
			$orderId = $params["orderId"];
			
			$bookingDetail = $shopService->getBookingDetail($orderId);
			
			echo snc_return($ERROR_CODES["SUCCESS"]["code"], $ERROR_CODES["SUCCESS"]["message"], $bookingDetail);
			exit;
		} else if($apiType == "my_booking_list") {
			if(empty($token)) {
				echo snc_return($ERROR_CODES["INVALID_TOKEN"]["code"], $ERROR_CODES["INVALID_TOKEN"]["message"], null);
				exit;
			}
				
			$memberId = $memberService->getMemberIdByToken($token);
			
			$my_booking_list = $shopService->getMyBookingList($memberId);
			
			echo snc_return($ERROR_CODES["SUCCESS"]["code"], $ERROR_CODES["SUCCESS"]["message"], $my_booking_list);
			exit;
		} else if($apiType == "my_history_list") {
			if(empty($token)) {
				echo snc_return($ERROR_CODES["INVALID_TOKEN"]["code"], $ERROR_CODES["INVALID_TOKEN"]["message"], null);
				exit;
			}
				
			$memberId = $memberService->getMemberIdByToken($token);
			
			$startDate = $params["startDate"];
			$endDate = $params["endDate"];
		
			$my_history_list = $shopService->getMyHistoryList($memberId, $startDate, $endDate);
		
			echo snc_return($ERROR_CODES["SUCCESS"]["code"], $ERROR_CODES["SUCCESS"]["message"], $my_history_list);
			exit;
		} else if($apiType == "addr2coord") {
			$address = $params["address"];
			
			$url = "https://apis.daum.net/local/geo/addr2coord?apikey=" . DAUM_API_KEY . "&q=" . urlencode($address) . "&output=json";
			
			$jsonResult = get_web_page($url);
		
			echo snc_return($ERROR_CODES["SUCCESS"]["code"], $ERROR_CODES["SUCCESS"]["message"], json_decode($jsonResult, JSON_UNESCAPED_UNICODE));
			exit;
		} else if($apiType == "shop_booking_list_by_month") {
			if(empty($token)) {
				echo snc_return($ERROR_CODES["INVALID_TOKEN"]["code"], $ERROR_CODES["INVALID_TOKEN"]["message"], null);
				exit;
			}
			
			$shopId = $memberService->getShopIdByToken($token);
			
			$start = str_replace('-', '', $params["start"]);
			$end = str_replace('-', '', $params["end"]);
			
			$shopBookingList = $shopService->getShopBookingCountListByMonth($shopId, $start, $end);
			
			echo snc_return($ERROR_CODES["SUCCESS"]["code"], $ERROR_CODES["SUCCESS"]["message"], $shopBookingList);
			exit;
		} else if($apiType == "shop_booking_list_by_date") {
			if(empty($token)) {
				echo snc_return($ERROR_CODES["INVALID_TOKEN"]["code"], $ERROR_CODES["INVALID_TOKEN"]["message"], null);
				exit;
			}
				
			$shopId = $memberService->getShopIdByToken($token);
			
			$date = date("Ymd", $params["date"] / 1000);
			
			$shopBookingList = $shopService->getShopBookingListByDate($shopId, $date);
				
			echo snc_return($ERROR_CODES["SUCCESS"]["code"], $ERROR_CODES["SUCCESS"]["message"], $shopBookingList);
			exit;
		} else if($apiType == "shop_booking_detail") {
			if(empty($token)) {
				echo snc_return($ERROR_CODES["INVALID_TOKEN"]["code"], $ERROR_CODES["INVALID_TOKEN"]["message"], null);
				exit;
			}
			
			$shopId = $memberService->getShopIdByToken($token);
			$bookingId = $params["bookingId"];
			
			// 매장 예약 상세정보 조회
			$shopBookingDetail = $shopService->getShopBookingDetail($shopId, $bookingId);
			
			// 매장 매니저 리스트 조회
			$shopManagerList = $shopService->getShopManagerList($shopId);
			
			// 매장 상품 리스트 조회
			$shopPdtList = $shopService->getShopProductList($shopId);
				
			// 매장 쿠폰 리스트 조회
			$couponList = array();
			if(isset($shopBookingDetail["USER_ID"]) && !empty($shopBookingDetail["USER_ID"])) {				
				$couponList = $shopService->getCouponMasterList($shopId);
			}
			
			$ret = array(
				"bookingDetail" => $shopBookingDetail,
				"shopManagerList" => $shopManagerList,
				"shopPdtList" => $shopPdtList,
				"couponList" => $couponList
			);
			
			echo snc_return($ERROR_CODES["SUCCESS"]["code"], $ERROR_CODES["SUCCESS"]["message"], $ret);
		} else if($apiType == "cancel_shop_booking") {
			if(empty($token)) {
				echo snc_return($ERROR_CODES["INVALID_TOKEN"]["code"], $ERROR_CODES["INVALID_TOKEN"]["message"], null);
				exit;
			}
				
			$shopId = $memberService->getShopIdByToken($token);
			$bookingId = $params["bookingId"];
			$cancelFrom = isset($params["cancelFrom"]) ? $params["cancelFrom"] : "";
			
			$shopService->cancelShopBooking($shopId, $bookingId, $cancelFrom);
			
			echo snc_return($ERROR_CODES["SUCCESS"]["code"], $ERROR_CODES["SUCCESS"]["message"], null);
		} else if($apiType == "cancel_shop_booking_customer") {
			if(empty($token)) {
				echo snc_return($ERROR_CODES["INVALID_TOKEN"]["code"], $ERROR_CODES["INVALID_TOKEN"]["message"], null);
				exit;
			}
		
			$shopId = $params["shopId"];
			$bookingId = $params["bookingId"];
			$cancelFrom = isset($params["cancelFrom"]) ? $params["cancelFrom"] : "";
				
			$shopService->cancelShopBooking($shopId, $bookingId, $cancelFrom);
				
			echo snc_return($ERROR_CODES["SUCCESS"]["code"], $ERROR_CODES["SUCCESS"]["message"], null);
		} else if($apiType == "shop_register_info") {
			if(empty($token)) {
				echo snc_return($ERROR_CODES["INVALID_TOKEN"]["code"], $ERROR_CODES["INVALID_TOKEN"]["message"], null);
				exit;
			}
			
			$shopId = $memberService->getShopIdByToken($token);
			
			$memberId = $params["memberId"];
				
			$memberDetail = $memberService->getMemberInfo($memberId);
			
			// 매장 매니저 리스트 조회
			$shopManagerList = $shopService->getShopManagerList($shopId);
			
			// 매장 상품 리스트 조회
			$shopPdtList = $shopService->getShopProductList($shopId);
			
			// 매장 쿠폰 리스트 조회
			$couponList = $shopService->getCouponList($shopId, $memberId);
				
			$ret = array(
				"memberDetail" => $memberDetail,
				"shopManagerList" => $shopManagerList,
				"shopPdtList" => $shopPdtList,
				"couponList" => $couponList
			);
				
			echo snc_return($ERROR_CODES["SUCCESS"]["code"], $ERROR_CODES["SUCCESS"]["message"], $ret);
			exit;
		} else if($apiType == "add_shop_dayoff") {
			if(empty($token)) {
				echo snc_return($ERROR_CODES["INVALID_TOKEN"]["code"], $ERROR_CODES["INVALID_TOKEN"]["message"], null);
				exit;
			}
				
			$shopId = $memberService->getShopIdByToken($token);
			$startDt = $params["startDt"];
			$endDt = $params["endDt"];
			$alldayYn = $params["alldayYn"];
			
			$shopService->addShopDayOff($shopId, $startDt, $endDt, $alldayYn);
			
			echo snc_return($ERROR_CODES["SUCCESS"]["code"], $ERROR_CODES["SUCCESS"]["message"], null);
			exit;
		} else if($apiType == "edit_shop_dayoff") {
			if(empty($token)) {
				echo snc_return($ERROR_CODES["INVALID_TOKEN"]["code"], $ERROR_CODES["INVALID_TOKEN"]["message"], null);
				exit;
			}
		
			$shopId = $memberService->getShopIdByToken($token);
			$startDt = $params["startDt"];
			$endDt = $params["endDt"];
			$alldayYn = $params["alldayYn"];
				
			$shopService->editShopDayOff($shopId, $startDt, $endDt, $alldayYn);
				
			echo snc_return($ERROR_CODES["SUCCESS"]["code"], $ERROR_CODES["SUCCESS"]["message"], null);
			exit;
		} else if($apiType == "shop_detail_for_manager") {
			if(empty($token)) {
				echo snc_return($ERROR_CODES["INVALID_TOKEN"]["code"], $ERROR_CODES["INVALID_TOKEN"]["message"], null);
				exit;
			}
			
			$shopId = $memberService->getShopIdByToken($token);
				
			// 매장 기본 정보
			$retShopDetail = $shopService->getShopDetail($shopId);
			if(!isset($retShopDetail)) {
				echo snc_return($ERROR_CODES["INVALID_SHOP_ID"]["code"], $ERROR_CODES["INVALID_SHOP_ID"]["message"], null);
				exit;
			}
				
			// 매장 상품 리스트 조회
			$retShopPdtList = $shopService->getShopProductList($shopId);
				
			// 매장 포트폴리오 리스트 조회
			$retShopPortfolioList = $shopService->getShopPortfolioList($shopId, 3);
				
			// 매장 방문 후기 리스트 조회
			$retShopReviewList = $shopService->getShopReviewList($shopId);
				
			// 전체 이벤트 리스트 조회
			$retShopEventList = $shopService->getTotalShopEventList();
			
			// 매장 진행중인 이벤트 리스트 조회
			$retAvailableShopEventList = $shopService->getAvailableShopEventList($shopId);
				
			// 매장에서 사용가능한 쿠폰 리스트 조회
			$retShopCouponList = $shopService->getShopCouponList($shopId);
			
			$retData = array(
				"shopDetail" => $retShopDetail,
				"shopPdtList" => $retShopPdtList,
				"shopPortfolioList" => $retShopPortfolioList,
				"shopReviewList" => $retShopReviewList,
				"shopEventList" => $retShopEventList,
				"availableEventList" => $retAvailableShopEventList,
				"shopCouponList" => $retShopCouponList
			);
				
			echo snc_return($ERROR_CODES["SUCCESS"]["code"], $ERROR_CODES["SUCCESS"]["message"], $retData);
			exit;
		} else if($apiType == "add_shop_product") {
			if(empty($token)) {
				echo snc_return($ERROR_CODES["INVALID_TOKEN"]["code"], $ERROR_CODES["INVALID_TOKEN"]["message"], null);
				exit;
			}
				
			$shopId = $memberService->getShopIdByToken($token);
			$pdtNm = $params["pdtNm"];
			$pdtDesc = $params["pdtDesc"];
			$pdtTpCd = $params["pdtTpCd"];
			$eventId = $params["eventId"];
			$elapseTm = $params["elapseTm"];
			$pdtPrice = $params["pdtPrice"];
			
			$pdtIdx = $shopService->getLastPdtIdx($shopId) + 1;
			
			$shopService->addShopProduct($shopId, $pdtNm, $pdtDesc, $pdtTpCd, $pdtIdx, $pdtPrice, $elapseTm, 0, 0, $eventId);
			
			echo snc_return($ERROR_CODES["SUCCESS"]["code"], $ERROR_CODES["SUCCESS"]["message"], null);
			exit;
		} else if($apiType == "delete_shop_product") {
			if(empty($token)) {
				echo snc_return($ERROR_CODES["INVALID_TOKEN"]["code"], $ERROR_CODES["INVALID_TOKEN"]["message"], null);
				exit;
			}
			
			$shopId = $memberService->getShopIdByToken($token);
			$pdtId = $params["pdtId"];
			
			$shopService->deleteShopProduct($shopId, $pdtId);
			
			echo snc_return($ERROR_CODES["SUCCESS"]["code"], $ERROR_CODES["SUCCESS"]["message"], null);
			exit;
		} else if($apiType == "add_shop_event") {
			if(empty($token)) {
				echo snc_return($ERROR_CODES["INVALID_TOKEN"]["code"], $ERROR_CODES["INVALID_TOKEN"]["message"], null);
				exit;
			}
				
			$shopId = $memberService->getShopIdByToken($token);
			
			$evtNm = $params["evtNm"];
			$evtType = $params["evtType"];
			$evtDiscPrice = $params["evtDiscPrice"];
			$evtDesc = $params["evtDesc"];
			
			echo snc_return($ERROR_CODES["SUCCESS"]["code"], $ERROR_CODES["SUCCESS"]["message"], null);
			exit;
		} else if($apiType == "sales_list_by_year") {
			if(empty($token)) {
				echo snc_return($ERROR_CODES["INVALID_TOKEN"]["code"], $ERROR_CODES["INVALID_TOKEN"]["message"], null);
				exit;
			}
			
			$shopId = $memberService->getShopIdByToken($token);
			
			$year = $params["year"];
			
			$totalAmount = $shopService->getTotalSalesAmountByYear($shopId, $year);
			
			$monthListByYear = $shopService->getSalesMonthListByYear($shopId, $year);
			
			$ret = array(
				"totalAmount" => $totalAmount,
				"monthList" => $monthListByYear
			);
			
			echo snc_return($ERROR_CODES["SUCCESS"]["code"], $ERROR_CODES["SUCCESS"]["message"], $ret);
			exit;
		} else if($apiType == "sales_list_by_month") {
			if(empty($token)) {
				echo snc_return($ERROR_CODES["INVALID_TOKEN"]["code"], $ERROR_CODES["INVALID_TOKEN"]["message"], null);
				exit;
			}
				
			$shopId = $memberService->getShopIdByToken($token);
				
			$month = $params["month"];
				
			$totalAmount = $shopService->getTotalSalesAmountByMonth($shopId, $month);
				
			$dayListByMonth = $shopService->getSalesMonthListByMonth($shopId, $month);
			
			// 상품별 매출통계
			$salesListByPdt = $shopService->getSalesInfoByProduct($shopId, $month);
			
			// 예약별 매출통계
			$salesListByOnOffline = $shopService->getSalesInfoByOnOffline($shopId, $month);
			
			// 결제별 매출통계
			$salesListByPayType = $shopService->getSalesInfoByPayType($shopId, $month);
			
			// 고객별 매출통계
			$salesListByMemberType = $shopService->getSalesInfoByMemberType($shopId, $month);
			
			$ret = array(
				"totalAmount" => $totalAmount,
				"dayList" => $dayListByMonth,
				"salesListByPdt" => $salesListByPdt,
				"salesListByOnOffline" => $salesListByOnOffline,
				"salesListByPayType" => $salesListByPayType,
				"salesListByMemberType" => $salesListByMemberType
			);
				
			echo snc_return($ERROR_CODES["SUCCESS"]["code"], $ERROR_CODES["SUCCESS"]["message"], $ret);
			exit;
		} else if($apiType == "sales_list_by_date") {
			if(empty($token)) {
				echo snc_return($ERROR_CODES["INVALID_TOKEN"]["code"], $ERROR_CODES["INVALID_TOKEN"]["message"], null);
				exit;
			}
			
			$shopId = $memberService->getShopIdByToken($token);
			
			$date = $params["date"];
			
			$list = $shopService->getSalesListByDate($shopId, $date);
			
			echo snc_return($ERROR_CODES["SUCCESS"]["code"], $ERROR_CODES["SUCCESS"]["message"], $list);
			exit;
		} else if($apiType == "event_list") {
			if(empty($token)) {
				echo snc_return($ERROR_CODES["INVALID_TOKEN"]["code"], $ERROR_CODES["INVALID_TOKEN"]["message"], null);
				exit;
			}
				
			$shopId = $memberService->getShopIdByToken($token);
			
			$totalEventList = $shopService->getTotalShopEventList();
				
			echo snc_return($ERROR_CODES["SUCCESS"]["code"], $ERROR_CODES["SUCCESS"]["message"], $totalEventList);
			exit;
		} else if($apiType == "delete_sales_data") {
			if(empty($token)) {
				echo snc_return($ERROR_CODES["INVALID_TOKEN"]["code"], $ERROR_CODES["INVALID_TOKEN"]["message"], null);
				exit;
			}
			
			$shopId = $memberService->getShopIdByToken($token);
			
			$orderId = $params["orderId"];
			
			$shopService->deleteShopSalesData($shopId, $orderId);
			
			echo snc_return($ERROR_CODES["SUCCESS"]["code"], $ERROR_CODES["SUCCESS"]["message"], null);
			exit;
		} else if($apiType == "get_sales_detail") {
			if(empty($token)) {
				echo snc_return($ERROR_CODES["INVALID_TOKEN"]["code"], $ERROR_CODES["INVALID_TOKEN"]["message"], null);
				exit;
			}
				
			$shopId = $memberService->getShopIdByToken($token);
			
			$orderId = $params["orderId"];
				
			$salesDetail = $shopService->getSalesDetail($shopId, $orderId);
			
			// 매장 매니저 리스트 조회
			$shopManagerList = $shopService->getShopManagerList($shopId);
				
			// 매장 상품 리스트 조회
			$shopPdtList = $shopService->getShopProductList($shopId);
				
			// 매장 쿠폰 리스트 조회
			$couponList = $shopService->getShopCouponList($shopId);
			
			$ret = array(
				"salesDetail" => $salesDetail,
				"shopManagerList" => $shopManagerList,
				"productList" => $shopPdtList,
				"couponList" => $couponList
			);
				
			echo snc_return($ERROR_CODES["SUCCESS"]["code"], $ERROR_CODES["SUCCESS"]["message"], $ret);
			exit;
		} else if($apiType == "edit_sales_data") {
			if(empty($token)) {
				echo snc_return($ERROR_CODES["INVALID_TOKEN"]["code"], $ERROR_CODES["INVALID_TOKEN"]["message"], null);
				exit;
			}
			
			$orderId = $params["orderId"];
			$pdtId = $params["pdtId"];
			$managerId = $params["managerId"];
			$orderDt = $params["orderDt"];
			$shopCouponId = isset($params["shopCouponId"]) ? $params["shopCouponId"] : 0;
			$totalPrice = $params["totalPrice"];
			$shopId = $memberService->getShopIdByToken($token);
			
			$orderId = $shopService->editSalesData($orderId, $shopId, $pdtId, $managerId, $orderDt, $totalPrice, $shopCouponId);
			
			echo snc_return($ERROR_CODES["SUCCESS"]["code"], $ERROR_CODES["SUCCESS"]["message"], null);
			exit;
		} else if($apiType == "update_shop_data_pdt") {
			if(empty($token)) {
				echo snc_return($ERROR_CODES["INVALID_TOKEN"]["code"], $ERROR_CODES["INVALID_TOKEN"]["message"], null);
				exit;
			}
			
			$shopId = $memberService->getShopIdByToken($token);
			$addedShopPdtList = json_decode($params["addedShopPdtList"], true);
			$deletedShopPdtList = json_decode($params["deletedShopPdtList"], true);
			$selectedEventList = json_decode($params["selectedEventList"], true);
			$deletedShopImages = json_decode($params["deletedShopImages"], true);
			
			$shopService->updateShopInfoTab1($shopId, $addedShopPdtList, $deletedShopPdtList, $selectedEventList, $deletedShopImages);
			
			// 매장 상품 리스트 조회
			$shopPdtList = $shopService->getShopProductList($shopId);
			
			$ret = array(
				"shopPdtList" => $shopPdtList
			);
			
			echo snc_return($ERROR_CODES["SUCCESS"]["code"], $ERROR_CODES["SUCCESS"]["message"], $ret);
			exit;
		} else if($apiType == "update_shop_data_addr") {
			if(empty($token)) {
				echo snc_return($ERROR_CODES["INVALID_TOKEN"]["code"], $ERROR_CODES["INVALID_TOKEN"]["message"], null);
				exit;
			}
			
			$shopId = $memberService->getShopIdByToken($token);
			$shopAddr = $params["shopAddr"];
			$shopPostalCd = $params["shopPostalCd"];
			$shopTel = $params["shopTel"];
			$shopMobile = $params["shopMobile"];
			$shopOpTime = $params["shopOpTime"];
			$shopOpTime2 = $params["shopOpTime2"];
			$shopEtc = $params["shopEtc"];
			$shopLat = $params["shopLat"];
			$shopLng = $params["shopLng"];
			$shopLocCd = $params["shopLocCd"];
			$deletedShopImages = json_decode($params["deletedShopImages"], true);
			
			$shopService->updateShopInfoTab2($shopId, $shopAddr, $shopPostalCd, $shopLat, $shopLng, $shopTel, $shopMobile, $shopOpTime, $shopOpTime2, $shopEtc, $deletedShopImages, $shopLocCd);
						
			echo snc_return($ERROR_CODES["SUCCESS"]["code"], $ERROR_CODES["SUCCESS"]["message"], null);
			exit;
		} else if($apiType == "delete_review") {
			if(empty($token)) {
				echo snc_return($ERROR_CODES["INVALID_TOKEN"]["code"], $ERROR_CODES["INVALID_TOKEN"]["message"], null);
				exit;
			}
				
			$shopId = $memberService->getShopIdByToken($token);
			
			$cmtId = $params["cmtId"];
			
			$shopService->deleteReview($shopId, $cmtId);
			
			echo snc_return($ERROR_CODES["SUCCESS"]["code"], $ERROR_CODES["SUCCESS"]["message"], null);
			exit;
		} else if($apiType == "register_review_reply") {
			if(empty($token)) {
				echo snc_return($ERROR_CODES["INVALID_TOKEN"]["code"], $ERROR_CODES["INVALID_TOKEN"]["message"], null);
				exit;
			}
			
			$shopId = $memberService->getShopIdByToken($token);
				
			$cmtId = $params["cmtId"];
			$reply = $params["reply"];
			
			$reviewReplyDetail = $shopService->registerReviewReply($shopId, $cmtId, $reply);
			
			echo snc_return($ERROR_CODES["SUCCESS"]["code"], $ERROR_CODES["SUCCESS"]["message"], $reviewReplyDetail);
			exit;
		} else if($apiType == "delete_review_reply") {
			if(empty($token)) {
				echo snc_return($ERROR_CODES["INVALID_TOKEN"]["code"], $ERROR_CODES["INVALID_TOKEN"]["message"], null);
				exit;
			}
		
			$shopId = $memberService->getShopIdByToken($token);
				
			$cmtId = $params["cmtId"];
			$rplId = $params["rplId"];
				
			$shopService->deleteReviewReply($shopId, $cmtId, $rplId);
				
			echo snc_return($ERROR_CODES["SUCCESS"]["code"], $ERROR_CODES["SUCCESS"]["message"], null);
			exit;
		} else if($apiType == "update_review_reply") {
			if(empty($token)) {
				echo snc_return($ERROR_CODES["INVALID_TOKEN"]["code"], $ERROR_CODES["INVALID_TOKEN"]["message"], null);
				exit;
			}
				
			$shopId = $memberService->getShopIdByToken($token);
		
			$cmtId = $params["cmtId"];
			$rplId = $params["rplId"];
			$reply = $params["reply"];
				
			$reviewReplyDetail = $shopService->updateReviewReply($shopId, $cmtId, $rplId, $reply);
				
			echo snc_return($ERROR_CODES["SUCCESS"]["code"], $ERROR_CODES["SUCCESS"]["message"], $reviewReplyDetail);
			exit;
		} else if($apiType == "shop_portfolio_for_edit") {
			if(empty($token)) {
				echo snc_return($ERROR_CODES["INVALID_TOKEN"]["code"], $ERROR_CODES["INVALID_TOKEN"]["message"], null);
				exit;
			}
			
			$shopId = $memberService->getShopIdByToken($token);
			
			// 매장 기본 정보
			$retShopDetail = $shopService->getShopDetail($shopId);
			if(!isset($retShopDetail)) {
				echo snc_return($ERROR_CODES["INVALID_SHOP_ID"]["code"], $ERROR_CODES["INVALID_SHOP_ID"]["message"], null);
				exit;
			}
				
			// 매장 포트폴리오 리스트 조회
			$retShopPortfolioList = $shopService->getShopPortfolioList($shopId, null);
				
			$retData = array(
				"shopDetail" => $retShopDetail,
				"shopPortfolioList" => $retShopPortfolioList
			);
				
			echo snc_return($ERROR_CODES["SUCCESS"]["code"], $ERROR_CODES["SUCCESS"]["message"], $retData);
			exit;
		} else if($apiType == "save_portfolio") {
			if(empty($token)) {
				echo snc_return($ERROR_CODES["INVALID_TOKEN"]["code"], $ERROR_CODES["INVALID_TOKEN"]["message"], null);
				exit;
			}
				
			$shopId = $memberService->getShopIdByToken($token);
			$portNm = $params["portNm"];
			$portDesc = $params["portDesc"];
			$portTpCd = $params["portTpCd"];
			$tags = isset($params["tags"]) ? $params["tags"] : "";
			
			$portImg1 = "images/sample_img2.png";
			
			$shopService->savePortfolio($shopId, $portImg1, $portNm, $portDesc, $portTpCd, $tags);
			
			echo snc_return($ERROR_CODES["SUCCESS"]["code"], $ERROR_CODES["SUCCESS"]["message"], null);
			exit;
		} else if($apiType == "registerPayment") {
			if(empty($token)) {
				echo snc_return($ERROR_CODES["INVALID_TOKEN"]["code"], $ERROR_CODES["INVALID_TOKEN"]["message"], null);
				exit;
			}
			
			$shopId = $memberService->getShopIdByToken($token);
			$bookingId = $params["bookingId"];
			
			$shopService->registerPayment($shopId, $bookingId);
				
			echo snc_return($ERROR_CODES["SUCCESS"]["code"], $ERROR_CODES["SUCCESS"]["message"], null);
			exit;
		} else if($apiType == "save_portfolio_sort") {
			if(empty($token)) {
				echo snc_return($ERROR_CODES["INVALID_TOKEN"]["code"], $ERROR_CODES["INVALID_TOKEN"]["message"], null);
				exit;
			}
				
			$shopId = $memberService->getShopIdByToken($token);
			
			$portfolioSort = $params["portfolioSort"];
			
			foreach ($portfolioSort as $row) {
				$shopService->savePortfolioSort($shopId, $row["PORT_ID"], $row["PORT_IDX"]);
			}
			
			echo snc_return($ERROR_CODES["SUCCESS"]["code"], $ERROR_CODES["SUCCESS"]["message"], null);
			exit;
		} else if($apiType == "check_shop_holiday") {
			if(empty($token)) {
				echo snc_return($ERROR_CODES["INVALID_TOKEN"]["code"], $ERROR_CODES["INVALID_TOKEN"]["message"], null);
				exit;
			}
			
			$shopId = $memberService->getShopIdByToken($token);
				
			$selectedDate = $params["selectedDate"];
			
			$isHoliday = $shopService->checkShopHoliday($shopId, $selectedDate);
			
			$ret = array(
				"isHoliday" => $isHoliday ? "Y" : "N"
			);
				
			echo snc_return($ERROR_CODES["SUCCESS"]["code"], $ERROR_CODES["SUCCESS"]["message"], $ret);
			exit;
		} else if($apiType == "delete_dayoff") {
			if(empty($token)) {
				echo snc_return($ERROR_CODES["INVALID_TOKEN"]["code"], $ERROR_CODES["INVALID_TOKEN"]["message"], null);
				exit;
			}
				
			$shopId = $memberService->getShopIdByToken($token);
			
			$offId = $params["offId"];
				
			$shopService->deleteShopHoliday($shopId, $offId);
			
			echo snc_return($ERROR_CODES["SUCCESS"]["code"], $ERROR_CODES["SUCCESS"]["message"], null);
			exit;
		} else if($apiType == "delete_portfolio") {
			if(empty($token)) {
				echo snc_return($ERROR_CODES["INVALID_TOKEN"]["code"], $ERROR_CODES["INVALID_TOKEN"]["message"], null);
				exit;
			}
			
			$shopId = $memberService->getShopIdByToken($token);
				
			$portId = $params["portId"];
			
			$shopService->deleteShopPortfolio($shopId, $portId);
				
			echo snc_return($ERROR_CODES["SUCCESS"]["code"], $ERROR_CODES["SUCCESS"]["message"], null);
			exit;
		} else if($apiType == "event_list_all") {
			$totalEventList = $shopService->getTotalShopEventList();
		
			echo snc_return($ERROR_CODES["SUCCESS"]["code"], $ERROR_CODES["SUCCESS"]["message"], $totalEventList);
			exit;
		}
	} catch (Exception $e) {
		debug_mesg("E", __CLASS__, __FUNCTION__, __LINE__, "api member error : " . $e->getMessage());
	
		if(getErrorCode($e->getMessage()) != null) {
			echo snc_return($ERROR_CODES[$e->getMessage()]["code"], $ERROR_CODES[$e->getMessage()]["message"], null);
		} else {
			echo snc_return($ERROR_CODES["SERVER_INTERNAL_ERROR"]["code"], $ERROR_CODES["SERVER_INTERNAL_ERROR"]["message"], null);
		}
		exit;
	}
?>