<?php
	header("Content-Type:text/html; charset=utf-8");
	
	try {
		require_once ("../inc/config.php");
		require_once_classes(array("CMysqlManager"));
		require_once_services(array("CMemberService"));
	
		$mysql_manager = new CMysqlManager();
		$memberService = new CMemberService($mysql_manager->getDb());
	
		$apiType = isset($_REQUEST["apiType"]) ? $_REQUEST["apiType"] : "";
		$token = isset($_REQUEST["token"]) ? $_REQUEST["token"] : "";
	
		if(!isset($apiType) || empty($apiType)) {
			echo snc_return($ERROR_CODES["NOT_ENOUGH_PARAM"]["code"], $ERROR_CODES["NOT_ENOUGH_PARAM"]["message"], null);
			exit;
		}
	
		if(empty($token) || !$memberService->isValidToken($token)) {
			$token = "";
		}
	
		if($apiType == "memberInfo") {
			
		}
	} catch (Exception $e) {
		debug_mesg("E", __CLASS__, __FUNCTION__, __LINE__, "api member error : " . $e->getMessage());

		if(getErrorCode($e->getMessage()) != null) {
			echo snc_return($ERROR_CODES[$e->getMessage()]["code"], $ERROR_CODES[$e->getMessage()]["message"], null);
		} else {
			echo snc_return($ERROR_CODES["SERVER_INTERNAL_ERROR"]["code"], $ERROR_CODES["SERVER_INTERNAL_ERROR"]["message"], null);
		}
		exit;
	}
?>