<?php
	header("Content-Type:text/html; charset=utf-8");
	
	try {
		require_once ("../inc/config.php");
		require_once_classes(array("CMysqlManager"));
		require_once_services(array("COKNameService"));
	
		$mysql_manager = new CMysqlManager();
		$okNameService = new COKNameService($mysql_manager->getDb());
	
		$apiType = isset($_REQUEST["apiType"]) ? $_REQUEST["apiType"] : "";
	
		if(!isset($apiType) || empty($apiType)) {
			echo snc_return($ERROR_CODES["NOT_ENOUGH_PARAM"]["code"], $ERROR_CODES["NOT_ENOUGH_PARAM"]["message"], null);
			exit;
		}
	
		if($apiType == "save_temp_phone_auth") {
			$token = $_POST["phoneAuthToken"];
			$phoneNumber = $_POST["phoneNumber"];
			$userRealNM = $_POST["userRealNM"];
			$userCi = $_POST["userCi"];
			$userDi = $_POST["userDi"];
			
			$okNameService->saveTempPhoneAuth($token, $phoneNumber, $userRealNM, $userCi, $userDi);
			
			echo snc_return($ERROR_CODES["SUCCESS"]["code"], $ERROR_CODES["SUCCESS"]["message"], null);
			exit;
		}
	} catch (Exception $e) {
		debug_mesg("E", __CLASS__, __FUNCTION__, __LINE__, "api member error : " . $e->getMessage());

		if(getErrorCode($e->getMessage()) != null) {
			echo snc_return($ERROR_CODES[$e->getMessage()]["code"], $ERROR_CODES[$e->getMessage()]["message"], null);
		} else {
			echo snc_return($ERROR_CODES["SERVER_INTERNAL_ERROR"]["code"], $ERROR_CODES["SERVER_INTERNAL_ERROR"]["message"], null);
		}
		exit;
	}
?>