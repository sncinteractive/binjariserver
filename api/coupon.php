<?php
	header("Content-Type:text/html; charset=utf-8");
	
	try {
		require_once ("../inc/config.php");
		require_once_classes(array("CMysqlManager"));
		require_once_services(array("CMemberService", "CShopService"));
		
		$mysql_manager = new CMysqlManager();
		$memberService = new CMemberService($mysql_manager->getDb());
		$shopService = new CShopService($mysql_manager->getDb());
	
		$params = json_decode(file_get_contents('php://input'), true, 5, JSON_UNESCAPED_UNICODE);
	
		$apiType = isset($params["apiType"]) ? $params["apiType"] : "";
		$token = isset($params["token"]) ? $params["token"] : "";
	
		if(!isset($apiType) || empty($apiType)) {
			echo snc_return($ERROR_CODES["NOT_ENOUGH_PARAM"]["code"], $ERROR_CODES["NOT_ENOUGH_PARAM"]["message"], null);
			exit;
		}
	
		if(empty($token) || !$memberService->isValidToken($token)) {
			$token = "";
		}
	
		if($apiType == "shop_coupon_list") {
			if(empty($token)) {
				echo snc_return($ERROR_CODES["INVALID_TOKEN"]["code"], $ERROR_CODES["INVALID_TOKEN"]["message"], null);
				exit;
			}
				
			$shopId = $memberService->getShopIdByToken($token);
			
			$shopCouponList = $shopService->getShopCouponListAll($shopId);
			
			echo snc_return($ERROR_CODES["SUCCESS"]["code"], $ERROR_CODES["SUCCESS"]["message"], $shopCouponList);
			exit;
		} else if($apiType == "delete_coupon") {
			if(empty($token)) {
				echo snc_return($ERROR_CODES["INVALID_TOKEN"]["code"], $ERROR_CODES["INVALID_TOKEN"]["message"], null);
				exit;
			}
			
			$shopId = $memberService->getShopIdByToken($token);
			$shopCouponId = $params["shopCouponId"];
			
			$shopService->deleteShopCoupon($shopId, $shopCouponId);
				
			echo snc_return($ERROR_CODES["SUCCESS"]["code"], $ERROR_CODES["SUCCESS"]["message"], null);
			exit;
		} else if($apiType == "register_coupon") {
			if(empty($token)) {
				echo snc_return($ERROR_CODES["INVALID_TOKEN"]["code"], $ERROR_CODES["INVALID_TOKEN"]["message"], null);
				exit;
			}
			
			$shopId = $memberService->getShopIdByToken($token);
			
			$shopCouponId = isset($params["shopCouponId"]) && !empty($params["shopCouponId"]) ? $params["shopCouponId"] : 0;
			$couponNm = $params["coupon_nm"];
			$startDt = $params["start_dt"];
			$endDt = $params["end_dt"];
			$couponTypeCd = $params["coupon_type_cd"];
			$couponDispPrice = $params["coupon_disp_price"];
			$useYn = $params["use_yn"];
			
			if($shopCouponId == 0) {
				$shopService->registerShopCoupon($shopId, $couponNm, $startDt, $endDt, $couponTypeCd, $couponDispPrice, $useYn);
			} else {
				$shopService->updateShopCoupon($shopCouponId, $shopId, $couponNm, $startDt, $endDt, $couponTypeCd, $couponDispPrice, $useYn);
			}
			
			echo snc_return($ERROR_CODES["SUCCESS"]["code"], $ERROR_CODES["SUCCESS"]["message"], null);
			exit;
		} else if($apiType == "coupon_detail") {
			if(empty($token)) {
				echo snc_return($ERROR_CODES["INVALID_TOKEN"]["code"], $ERROR_CODES["INVALID_TOKEN"]["message"], null);
				exit;
			}
				
			$shopId = $memberService->getShopIdByToken($token);
				
			$shopCouponId = $params["shopCouponId"];
			
			$couponDetail = $shopService->getShopCouponDetail($shopId, $shopCouponId);
				
			echo snc_return($ERROR_CODES["SUCCESS"]["code"], $ERROR_CODES["SUCCESS"]["message"], $couponDetail);
		}
	} catch (Exception $e) {
		debug_mesg("E", __CLASS__, __FUNCTION__, __LINE__, "api member error : " . $e->getMessage());
	
		if(getErrorCode($e->getMessage()) != null) {
			echo snc_return($ERROR_CODES[$e->getMessage()]["code"], $ERROR_CODES[$e->getMessage()]["message"], null);
		} else {
			echo snc_return($ERROR_CODES["SERVER_INTERNAL_ERROR"]["code"], $ERROR_CODES["SERVER_INTERNAL_ERROR"]["message"], null);
		}
		exit;
	}
?>