<?php
	header("Content-Type:text/html; charset=utf-8");
	
	try {
		require_once ("../inc/config.php");
		require_once_classes(array("CMysqlManager"));
		require_once_services(array("CMemberService", "CShopService"));
	
		$mysql_manager = new CMysqlManager();
		$memberService = new CMemberService($mysql_manager->getDb());
		$shopService = new CShopService($mysql_manager->getDb());
	
// 		echo "<pre>";
// 		print_r($_REQUEST);
// 		print_r($_POST);
// 		print_r(json_encode($_FILES, JSON_UNESCAPED_UNICODE));
		
// 		exit;
		
		$apiType = isset($_POST["apiType"]) ? $_POST["apiType"] : "";
	
		if(!isset($apiType) || empty($apiType)) {
			echo snc_return($ERROR_CODES["NOT_ENOUGH_PARAM"]["code"], $ERROR_CODES["NOT_ENOUGH_PARAM"]["message"], null);
			exit;
		}
	
		if($apiType == "save_portfolio") {
			$token = $_POST["token"];
			$portNm = $_POST["portNm"];
			$portDesc = $_POST["portDesc"];
			$portTpCd = $_POST["portTpCd"];
			$tags = isset($_POST["tags"]) ? $_POST["tags"] : "";
			
			if(empty($token)) {
				echo snc_return($ERROR_CODES["INVALID_TOKEN"]["code"], $ERROR_CODES["INVALID_TOKEN"]["message"], null);
				exit;
			}
			
			$shopId = $memberService->getShopIdByToken($token);
			
			if(!isset($_FILES)) {
				echo snc_return($ERROR_CODES["FAIL_TO_INSERT_PORTFOLIO"]["code"], $ERROR_CODES["FAIL_TO_INSERT_PORTFOLIO"]["message"], null);
				exit;
			}
			
			$path = "upload/portfolio/" . $shopId . "/";
			$dirName = trim(CONF_PATH_ROOT);
			
			if(!is_dir($dirName . $path)) {
				mkdir($dirName . $path, 0777, true);
			}
			
			$fileName = $shopId . "_" . time() . ".jpg";
			$fullPath = $dirName . $path . $fileName;
			
			$imgString = file_get_contents($_FILES['file']['tmp_name']);
			/* create image from string */
			$image = imagecreatefromstring($imgString);
			
			switch ($_FILES['file']['type']) {
				case 'image/jpeg':
					imagejpeg($image, $fullPath, 60);
					break;
				case 'image/png':
					imagepng($image, $fullPath, 0);
					break;
				case 'image/gif':
					imagegif($image, $fullPath);
					break;
				default:
					exit;
					break;
			}
			
			$portImg1 = "/" . $path . $fileName;
			
			$shopService->savePortfolio($shopId, $portImg1, $portNm, $portDesc, $portTpCd, $tags);
				
			echo $ERROR_CODES["SUCCESS"]["code"];
			exit;
		} else if($apiType == "save_shop_image") {
			$token = $_POST["token"];
			$imageColumn = $_POST["imageColumn"];
				
			if(empty($token)) {
				echo snc_return($ERROR_CODES["INVALID_TOKEN"]["code"], $ERROR_CODES["INVALID_TOKEN"]["message"], null);
				exit;
			}
				
			$shopId = $memberService->getShopIdByToken($token);
				
			if(!isset($_FILES)) {
				echo snc_return($ERROR_CODES["FAIL_TO_UPDATE_SHOP_DATA"]["code"], $ERROR_CODES["FAIL_TO_UPDATE_SHOP_DATA"]["message"], null);
				exit;
			}
				
			$path = "upload/shop/" . $shopId . "/";
			$dirName = trim(CONF_PATH_ROOT);
			
			if(!is_dir($dirName . $path)) {
				mkdir($dirName . $path, 0777, true);
			}
				
			$fileName = $shopId . "_" . time() . ".jpg";
			$fullPath = $dirName . $path . $fileName;
			
			$imgString = file_get_contents($_FILES['file']['tmp_name']);
			/* create image from string */
			$image = imagecreatefromstring($imgString);
				
			switch ($_FILES['file']['type']) {
				case 'image/jpeg':
					imagejpeg($image, $fullPath, 60);
					break;
				case 'image/png':
					imagepng($image, $fullPath, 0);
					break;
				case 'image/gif':
					imagegif($image, $fullPath);
					break;
				default:
					exit;
					break;
			}
				
			$shopImg = "/" . $path . $fileName;
				
			$shopService->updateShopImage($shopId, $imageColumn, $shopImg);
			
			echo $ERROR_CODES["SUCCESS"]["code"];
			exit;
		}
	} catch (Exception $e) {
		debug_mesg("E", __CLASS__, __FUNCTION__, __LINE__, "api member error : " . $e->getMessage());

		if(getErrorCode($e->getMessage()) != null) {
			echo $ERROR_CODES[$e->getMessage()]["code"];
		} else {
			echo $ERROR_CODES["SERVER_INTERNAL_ERROR"]["code"];
		}
		exit;
	}
?>