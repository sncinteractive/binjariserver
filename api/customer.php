<?php
	header("Content-Type:text/html; charset=utf-8");
	
	try {
		require_once ("../inc/config.php");
		require_once_classes(array("CMysqlManager"));
		require_once_services(array("CCustomerService", "CMemberService"));
	
		$mysql_manager = new CMysqlManager();
		$memberService = new CMemberService($mysql_manager->getDb());
		$custoemrService = new CCustomerService($mysql_manager->getDb());
		
		$params = json_decode(file_get_contents('php://input'), true, 5, JSON_UNESCAPED_UNICODE);
	
		$apiType = isset($params["apiType"]) ? $params["apiType"] : "";
		$token = isset($params["token"]) ? $params["token"] : "";
	
		if(!isset($apiType) || empty($apiType)) {
			echo snc_return($ERROR_CODES["NOT_ENOUGH_PARAM"]["code"], $ERROR_CODES["NOT_ENOUGH_PARAM"]["message"], null);
			exit;
		}
	
		if(empty($token) || !$memberService->isValidToken($token)) {
			$token = "";
		}
		
		if($apiType == "search_customer") {
			if(empty($token)) {
				echo snc_return($ERROR_CODES["INVALID_TOKEN"]["code"], $ERROR_CODES["INVALID_TOKEN"]["message"], null);
				exit;
			}
				
			$shopId = $memberService->getShopIdByToken($token);
			
			$searchType = $params["searchType"];
			$searchVal = $params["searchVal"];
			$shopGradeCd = $params["shopGradeCd"];
			
			$customerList = $custoemrService->getCustomerList($shopId, $searchType, $searchVal, $shopGradeCd);
			
			echo snc_return($ERROR_CODES["SUCCESS"]["code"], $ERROR_CODES["SUCCESS"]["message"], $customerList);
			exit;
		} else if($apiType == "register_customer") {
			if(empty($token)) {
				echo snc_return($ERROR_CODES["INVALID_TOKEN"]["code"], $ERROR_CODES["INVALID_TOKEN"]["message"], null);
				exit;
			}
			
			$shopId = $memberService->getShopIdByToken($token);
			
			$userName = $params["userName"];
			$userPhone = $params["userPhone"];
			$userEmail = $params["userEmail"];
			$userNickName = $params["userNickName"];
			$userTpCd = $params["userTpCd"];
			$userDesc = $params["userDesc"];
			$shopGradeCd = $params["shopGradeCd"];
			
			$custoemrService->registerCustomer($shopId, $userName, $userPhone, $userEmail, $userNickName, $userTpCd, $userDesc, $shopGradeCd);
			
			echo snc_return($ERROR_CODES["SUCCESS"]["code"], $ERROR_CODES["SUCCESS"]["message"], null);
			exit;
		} else if($apiType == "edit_customer") {
			if(empty($token)) {
				echo snc_return($ERROR_CODES["INVALID_TOKEN"]["code"], $ERROR_CODES["INVALID_TOKEN"]["message"], null);
				exit;
			}
				
			$shopId = $memberService->getShopIdByToken($token);
			
			$memberId = $params["memberId"];
			$userName = $params["userName"];
			$userPhone = $params["userPhone"];
			$userEmail = $params["userEmail"];
			$userNickName = $params["userNickName"];
			$shopGradeCd = $params["shopGradeCd"];
				
			$custoemrService->editCustomer($shopId, $memberId, $userName, $userPhone, $userEmail, $userNickName, $shopGradeCd);
				
			echo snc_return($ERROR_CODES["SUCCESS"]["code"], $ERROR_CODES["SUCCESS"]["message"], null);
			exit;
		} else if($apiType == "customer_detail") {
			if(empty($token)) {
				echo snc_return($ERROR_CODES["INVALID_TOKEN"]["code"], $ERROR_CODES["INVALID_TOKEN"]["message"], null);
				exit;
			}
				
			$shopId = $memberService->getShopIdByToken($token);
				
			$memberId = $params["memberId"];
			
			$customerDetail = $custoemrService->getCustomerDetail($shopId, $memberId);
				
			echo snc_return($ERROR_CODES["SUCCESS"]["code"], $ERROR_CODES["SUCCESS"]["message"], $customerDetail);
			exit;
		} else if($apiType == "delete_customer") {
			if(empty($token)) {
				echo snc_return($ERROR_CODES["INVALID_TOKEN"]["code"], $ERROR_CODES["INVALID_TOKEN"]["message"], null);
				exit;
			}
			
			$shopId = $memberService->getShopIdByToken($token);
			
			$memberId = $params["memberId"];
				
			$custoemrService->deleteCustomer($shopId, $memberId);
			
			echo snc_return($ERROR_CODES["SUCCESS"]["code"], $ERROR_CODES["SUCCESS"]["message"], null);
			exit;
		} else if($apiType == "edit_regular_customer") {
			if(empty($token)) {
				echo snc_return($ERROR_CODES["INVALID_TOKEN"]["code"], $ERROR_CODES["INVALID_TOKEN"]["message"], null);
				exit;
			}
			
			$shopId = $memberService->getShopIdByToken($token);
				
			$memberId = $params["memberId"];
			$shopGradeCd = $params["shopGradeCd"];
			$shopGradeMemo = $params["shopGradeMemo"];
			
			$custoemrService->editRegularCustomer($shopId, $memberId, $shopGradeCd, $shopGradeMemo);
			
			echo snc_return($ERROR_CODES["SUCCESS"]["code"], $ERROR_CODES["SUCCESS"]["message"], null);
			exit;
		} else if($apiType == "edit_interception") {
			if(empty($token)) {
				echo snc_return($ERROR_CODES["INVALID_TOKEN"]["code"], $ERROR_CODES["INVALID_TOKEN"]["message"], null);
				exit;
			}
				
			$shopId = $memberService->getShopIdByToken($token);
			
			$memberId = $params["memberId"];
			$interceptionYn = $params["interceptionYn"];
			
			$custoemrService->editCustomerInterception($shopId, $memberId, $interceptionYn);
				
			echo snc_return($ERROR_CODES["SUCCESS"]["code"], $ERROR_CODES["SUCCESS"]["message"], null);
			exit;
		} else if($apiType == "customer_booking_list") {
			if(empty($token)) {
				echo snc_return($ERROR_CODES["INVALID_TOKEN"]["code"], $ERROR_CODES["INVALID_TOKEN"]["message"], null);
				exit;
			}
			
			$shopId = $memberService->getShopIdByToken($token);
				
			$memberId = $params["memberId"];
			
			$customerBookingList = $custoemrService->getCustomerBookingList($shopId, $memberId);
			
			echo snc_return($ERROR_CODES["SUCCESS"]["code"], $ERROR_CODES["SUCCESS"]["message"], $customerBookingList);
			exit;
		} else if($apiType == "customer_coupon_list") {
			if(empty($token)) {
				echo snc_return($ERROR_CODES["INVALID_TOKEN"]["code"], $ERROR_CODES["INVALID_TOKEN"]["message"], null);
				exit;
			}
				
			$shopId = $memberService->getShopIdByToken($token);
			
			$memberId = $params["memberId"];
				
			$customerCouponList = $custoemrService->getCustomerCouponList($shopId, $memberId);
				
			echo snc_return($ERROR_CODES["SUCCESS"]["code"], $ERROR_CODES["SUCCESS"]["message"], $customerCouponList);
			exit;
		} else if($apiType == "send_coupon") {
			if(empty($token)) {
				echo snc_return($ERROR_CODES["INVALID_TOKEN"]["code"], $ERROR_CODES["INVALID_TOKEN"]["message"], null);
				exit;
			}
			
			$shopId = $memberService->getShopIdByToken($token);
				
			$memberId = $params["memberId"];
			$shopCouponId = $params["shopCouponId"];
			
			$custoemrService->sendCoupon($shopId, $memberId, $shopCouponId);
			
			echo snc_return($ERROR_CODES["SUCCESS"]["code"], $ERROR_CODES["SUCCESS"]["message"], null);
			exit;
		} else if($apiType == "recent_customer") {
			if(empty($token)) {
				echo snc_return($ERROR_CODES["INVALID_TOKEN"]["code"], $ERROR_CODES["INVALID_TOKEN"]["message"], null);
				exit;
			}
				
			$shopId = $memberService->getShopIdByToken($token);
			
			$customerList = $custoemrService->getRecentCustomerList($shopId);
				
			echo snc_return($ERROR_CODES["SUCCESS"]["code"], $ERROR_CODES["SUCCESS"]["message"], $customerList);
			exit;
		}
	} catch (Exception $e) {
		debug_mesg("E", __CLASS__, __FUNCTION__, __LINE__, "api member error : " . $e->getMessage());
	
		if(getErrorCode($e->getMessage()) != null) {
			echo snc_return($ERROR_CODES[$e->getMessage()]["code"], $ERROR_CODES[$e->getMessage()]["message"], null);
		} else {
			echo snc_return($ERROR_CODES["SERVER_INTERNAL_ERROR"]["code"], $ERROR_CODES["SERVER_INTERNAL_ERROR"]["message"], null);
		}
		exit;
	}
?>