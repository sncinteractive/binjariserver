<?php
	class CMysqlManager {
		var $ip;
		var $port;
		var $db;
		var $user;
		var $pass;
		var $mysql = null;
		
		function CMysqlManager($_alias = "DEF") {
			$this->ip = constant($_alias. '_MYSQL_IP');
			
			if(!isset($this->ip)) {
				throw new Exception('Not found database IP address!');
				return;
			}
			
			$this->port = constant($_alias. '_MYSQL_PORT');
			$this->db = constant($_alias. '_MYSQL_DB');
			$this->user = constant($_alias. '_MYSQL_USER');
			$this->pass = constant($_alias. '_MYSQL_PASS');
			
			$this->createDb();
		}
		
		function createDb() {
			require_once_classes(array("MysqliDb"));
			
			$mysqli = new mysqli ($this->ip, $this->user, $this->pass, $this->db);
			$this->mysql = new MysqliDb ($mysqli);
		}
		
		function getDb() {
			if(!isset($this->mysql)) {
				$this->createDb();
			}
			
			return $this->mysql;
		}
	}
?>