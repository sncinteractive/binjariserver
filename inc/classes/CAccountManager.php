<?php
	class CAccountManager {
		var $mysql;
				
		function CAccountManager($_mysql) {
			$this->mysql = $_mysql;
		}
		
		function getAccountList() {
			try {
				$account_list = $this->mysql->get("part_order_account", null, array("part_order_account_pk", "account_id", "staff_name", "store_id", "role", "account_status", "create_date"));
				
				return $account_list;
			} catch (Exception $e) {
				debug_mesg("E", __CLASS__, __FUNCTION__, __LINE__, $e->getMessage());
				throw new Exception("Fail to get account list; getAccountList(); ERROR[" . $e->getMessage() . "]");
				
				return null;
			}
		}
		
		function getAccountDataById($_account_id) {
			try {
				$this->mysql->where("account_id", $_account_id);
				$account_data = $this->mysql->getOne("part_order_account");
		
				return $account_data;
			} catch (Exception $e) {
				debug_mesg("E", __CLASS__, __FUNCTION__, __LINE__, $e->getMessage());
				throw new Exception("Fail to get account data; getAccountDataById(); ERROR[" . $e->getMessage() . "]");
		
				return null;
			}
		}
		
		function login($_id, $_pw, $_store_id) {
			try {
				$this->mysql->where('account_id', $_id)->where('account_pw', $_pw)->where('store_id', $_store_id);
				$account_info = $this->mysql->getOne("part_order_account", array("part_order_account_pk", "account_id", "staff_name", "store_id", "role", "account_status"));
			
				return $account_info;
			} catch (Exception $e) {
				debug_mesg("E", __CLASS__, __FUNCTION__, __LINE__, $e->getMessage());
				throw new Exception("Fail to login; Login(); ERROR[" . $e->getMessage() . "]");
			
				return null;
			}
		}
		
		function insertAccount($_account_id, $_account_pw, $_staff_name, $_store_id, $_role, $_account_status) {
			try {
				$newData = array(
					"account_id" => $_account_id,
					"account_pw" => md5($_account_pw),
					"staff_name" => $_staff_name,
					"store_id" => $_store_id,
					"role" => $_role,
					"account_status" => $_account_status,
					"create_date" => date("Y-m-d H:i:s")
				);
				
				$newData['part_order_account_pk'] = $this->mysql->insert("part_order_account", $newData);
						
				return $newData;
			} catch (Exception $e) {
				debug_mesg("E", __CLASS__, __FUNCTION__, __LINE__, $e->getMessage());
				throw new Exception("Fail to insert account; insertAccount(); ERROR[" . $e->getMessage() . "]");
		
				return null;
			}
		}
		
		function updateAccount($_part_order_account_pk, $_account_id, $_account_pw, $_staff_name, $_store_id, $_role, $_account_status) {
			try {
				$updateData = array(
					"account_id" => $_account_id,
					"staff_name" => $_staff_name,
					"store_id" => $_store_id,
					"role" => $_role,
					"account_status" => $_account_status,
					"create_date" => date("Y-m-d H:i:s")
				);
				
				if(isset($_account_pw) && !empty($_account_pw)) {
					$updateData["account_pw"] = md5($_account_pw);
				}
		
				$this->mysql->where("part_order_account_pk", $_part_order_account_pk);
				$ret = $this->mysql->update("part_order_account", $updateData);
				if($ret) {
					unset($updateData["account_pw"]);
					$updateData["part_order_account_pk"] = $_part_order_account_pk;
					
					return $updateData;
				} else {
					return null;
				}
			} catch (Exception $e) {
				debug_mesg("E", __CLASS__, __FUNCTION__, __LINE__, $e->getMessage());
				throw new Exception("Fail to update account; updateAccount(); ERROR[" . $e->getMessage() . "]");
		
				return null;
			}
		}
		
		function updateAccountStatus($_part_order_account_pk, $_account_status) {
			try {
				$updateData = array(
					"account_status" => $_account_status
				);
				
				$this->mysql->where("part_order_account_pk", $_part_order_account_pk);
				$ret = $this->mysql->update("part_order_account", $updateData);
				if($ret) {
					return $updateData;
				} else {
					return null;
				}
			} catch (Exception $e) {
				debug_mesg("E", __CLASS__, __FUNCTION__, __LINE__, $e->getMessage());
				throw new Exception("Fail to update account; updateAccountStatus(); ERROR[" . $e->getMessage() . "]");
		
				return null;
			}
		}
	}
?>