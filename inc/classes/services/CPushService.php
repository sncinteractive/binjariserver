<?php
	class CPushService {
		var $TAG = "CPushService";
		var $mysql;
		
		function CPushService($_mysql) {
			$this->mysql = $_mysql;
		}
		
		function savePushData($_userId, $_userGcmId, $_title, $_body, $_data, $_sendTarget, $_sendDt, $_sendYn, $_allUserYn) {
			try {
				$newData = array(
					'USER_ID' => $_userId,
					'USER_GCM_ID' => $_userGcmId,
					'TITLE' => $_title,
					'BODY' => $_body,
					'DATA' => $_data,
					'SEND_TARGET' => $_sendTarget,
					'SEND_DT' => $_sendDt,
					'SEND_YN' => $_sendYn,
					'ALL_USER_YN' => $_allUserYn,
					'UPDATE_DT' => date("YmdHis"),
					'REG_DT' => date("YmdHis")
				);
				
				$this->mysql->insert("PUSH", $newData);
			} catch (Exception $e) {
				debug_mesg("E", __CLASS__, __FUNCTION__, __LINE__, $this->TAG . "[savePushData()] : " . $e->getMessage());
				
				if(getErrorCode($e->getMessage()) != null) {
					throw new Exception($e->getMessage());
				}
				
				throw new Exception("SERVER_INTERNAL_ERROR");
			}
		}
		
		function getPushList() {
			try {
				$this->mysql->where("SEND_YN", "N")->where("SEND_DT <= " . date("YmdHis"));
				$pushList = $this->mysql->get("PUSH", null, "PUSH_ID, USER_ID, USER_GCM_ID, TITLE, BODY, DATA, SEND_TARGET, SEND_DT, SEND_YN, REG_DT, ALL_USER_YN");
				
				if(!isset($pushList)) {
					$pushList = array();
				}
				
				$pushIdList = array();
				foreach ($pushList as $row) {
					$pushIdList[] = $row["PUSH_ID"];
				}
				
				$updateData = array(
					"SEND_YN" => "Y",
					'UPDATE_DT' => date("YmdHis")
				);
				
				$pushIdListStr = join(",", $pushIdList);
				
				$this->mysql->rawQuery("UPDATE PUSH SET SEND_YN = 'Y', UPDATE_DT = '" . date("YmdHis") . "' WHERE PUSH_ID IN (" . $pushIdListStr . ");");
				
				return $pushList;
			} catch (Exception $e) {
				debug_mesg("E", __CLASS__, __FUNCTION__, __LINE__, $this->TAG . "[getPushList()] : " . $e->getMessage());
		
				if(getErrorCode($e->getMessage()) != null) {
					throw new Exception($e->getMessage());
				}
		
				throw new Exception("SERVER_INTERNAL_ERROR");
			}
		}
	}
?>