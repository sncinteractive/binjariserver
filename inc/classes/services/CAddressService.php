<?php
	class CAddressService {
		var $TAG = "CAddressService";
		var $mysql;
		
		function CAddressService($_mysql) {
			$this->mysql = $_mysql;
		}
		
		function saveTempAddress($_token, $_fullAddress, $_postCd) {
			try {
				$newData = array(
					'TOKEN' => $_token,
					'FULL_ADDRESS' => $_fullAddress,
					'POST_CD' => $_postCd
				);
				
				$this->mysql->insert("user_temp_address", $newData);
			} catch (Exception $e) {
				debug_mesg("E", __CLASS__, __FUNCTION__, __LINE__, $this->TAG . "[saveTempAddress()] : " . $e->getMessage());
				
				if(getErrorCode($e->getMessage()) != null) {
					throw new Exception($e->getMessage());
				}
				
				throw new Exception("SERVER_INTERNAL_ERROR");
			}
		}
		
		function getTempAddress($_token) {
			try {
				$this->mysql->where('TOKEN', $_token);
				$addressData = $this->mysql->getOne("user_temp_address", 'FULL_ADDRESS, POST_CD');
				
				if(isset($addressData)) {
					$this->deleteTempAddress($_token);
				}
				
				return $addressData;
			} catch (Exception $e) {
				debug_mesg("E", __CLASS__, __FUNCTION__, __LINE__, $this->TAG . "[getTempAddress()] : " . $e->getMessage());
		
				if(getErrorCode($e->getMessage()) != null) {
					throw new Exception($e->getMessage());
				}
		
				throw new Exception("SERVER_INTERNAL_ERROR");
			}
		}
		
		function deleteTempAddress($_token) {
			try {
				$this->mysql->where('TOKEN', $_token);
				$this->mysql->delete("user_temp_address");
			} catch (Exception $e) {
				debug_mesg("E", __CLASS__, __FUNCTION__, __LINE__, $this->TAG . "[deleteTempAddress()] : " . $e->getMessage());
		
				if(getErrorCode($e->getMessage()) != null) {
					throw new Exception($e->getMessage());
				}
		
				throw new Exception("SERVER_INTERNAL_ERROR");
			}
		}
	}
?>