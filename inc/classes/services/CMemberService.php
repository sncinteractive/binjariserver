<?php
	class CMemberService {
		var $TAG = "CMemberService";
		var $mysql;
		
		var $ShopService;

		function CMemberService($_mysql) {
			$this->mysql = $_mysql;
		}
		
		function getShopServiceClass() {
			if ($this->ShopService) {
				return;
			}
		
			require_once_services(Array('CShopService'));
			$this->ShopService = new CShopService($this->mysql);
		}
		
		function isValidToken($_token) {
			try {
				$this->mysql->where("token", $_token);
				$tokenInfo = $this->mysql->getOne("user_token");
				
				if(isset($tokenInfo) && !isset($tokenInfo["EXPIRED_DT"])) {
					return true;
				}
			} catch (Exception $e) {
				debug_mesg("E", __CLASS__, __FUNCTION__, __LINE__, $this->TAG . "[join()] : " . $e->getMessage());
				
				if(getErrorCode($e->getMessage()) != null) {
					throw new Exception($e->getMessage());
				}
				
				throw new Exception("SERVER_INTERNAL_ERROR");
			}
		}
		
		function checkDuplicateId($_memberAccountId) {
			try {
				$this->mysql->where("USER_ACCOUNT_ID", $_memberAccountId);
				$memberIdCount = $this->mysql->getValue("user_m", "COUNT(1)");
		
				return $memberIdCount > 0;
			} catch (Exception $e) {
				debug_mesg("E", __CLASS__, __FUNCTION__, __LINE__, $this->TAG . "[join()] : " . $e->getMessage());
				
				if(getErrorCode($e->getMessage()) != null) {
					throw new Exception($e->getMessage());
				}
				
				throw new Exception("SERVER_INTERNAL_ERROR");
			}
		}
		
		function join($_memberAccountId, $_memberAccountPw, $_email, $_nickName, $_phoneNumber, $_memberTpCd, $_memberDesc, $_memberPhoneCertyn, $_userRealNm, $_userCi, $_userDi, $_gcmID) {
			try {
				if($this->checkDuplicateId($_memberAccountId)) {
					throw new Exception("DUPLICATE_ID");
				}
				
				$newData = array(
					"USER_ACCOUNT_ID" => $_memberAccountId,
					"USER_ACCOUNT_PWD" => md5($_memberAccountPw),
					"USER_EMAIL" => $_email,
					"USER_NM" => $_nickName,
					"USER_REAL_NM" => $_userRealNm,
					"USER_TP_CD" => $_memberTpCd,
					"USER_DESC" => $_memberDesc,
					"USER_PHONE1" => $_phoneNumber,
					"PHONE_CERT_YN" => $_memberPhoneCertyn,
					"GCM_ID" => $_gcmID,
					"DEL_YN" => "N",
					"REG_DT" => date("YmdHis")
				);
				
				$this->mysql->startTransaction();
				
				$ret = $this->mysql->insert("user_m", $newData);
				if(!$ret) {
					debug_mesg("E", __CLASS__, __FUNCTION__, __LINE__, $this->TAG . "[join()] : " . $this->mysql->getLastError());
					throw new Exception("FAIL_TO_JOIN");
				}
				
				$memberId = $this->mysql->getInsertId();
					
				$agreementData = array(
					"USER_ID" => $memberId,
					"SERV_USE_AGREE_YN" => "Y",
					"SERV_USE_AGREE_DT" => date("YmdHis"),
					"PRIV_USE_AGREE_YN" => "Y",
					"PRIV_USE_AGREE_DT" => date("YmdHis"),
					"LOC_USE_AGREE_YN" => "Y",
					"LOC_USE_AGREE_DT" => date("YmdHis")
				);
					
				$ret = $this->mysql->insert("user_agreement_s", $agreementData);
				
				if(!$ret) {
					debug_mesg("E", __CLASS__, __FUNCTION__, __LINE__, $this->TAG . "[join()] : " . $this->mysql->getLastError());
					throw new Exception("FAIL_TO_JOIN");
				}
				
				$userPhoneAuthNewData = array(
					"USER_ID" => $memberId,
					"USER_PHONE1" => $_phoneNumber,
					"USER_REAL_NM" => $_userRealNm,
					"USER_CI" => $_userCi,
					"USER_DI" => $_userDi,
					"REG_DT" => date("YmdHis")
				);
				
				$ret = $this->mysql->insert("user_phone_auth_r", $userPhoneAuthNewData);
				if(!$ret) {
					debug_mesg("E", __CLASS__, __FUNCTION__, __LINE__, $this->TAG . "[join()] : " . $this->mysql->getLastError());
					throw new Exception("FAIL_TO_JOIN");
				}
				
				$this->mysql->commit();
		
				return $memberId;
			} catch (Exception $e) {
				$this->mysql->rollback();
				
				debug_mesg("E", __CLASS__, __FUNCTION__, __LINE__, $this->TAG . "[join()] : " . $e->getMessage());
				
				if(getErrorCode($e->getMessage()) != null) {
					throw new Exception($e->getMessage());
				}

				throw new Exception("SERVER_INTERNAL_ERROR");
			}
		}
		
		function managerJoin($_memberAccountId, $_memberAccountPw, $_email, $_nickName, $_phoneNumber, $_memberTpCd, $_memberDesc, $_memberPhoneCertyn, 
							$_shopName, $_shopOwnerName, $_shopAddress, $_shopPostCode, $_shopLat, $_shopLng, $_policy_1, $_policy_2, $_policy_3, $_policy_4, $_policy_5, $_userRealNm, $_userCi, $_userDi, $_gcmID) {
			try {
				if($this->checkDuplicateId($_memberAccountId)) {
					throw new Exception("DUPLICATE_ID");
				}
		
				$newData = array(
					"USER_ACCOUNT_ID" => $_memberAccountId,
					"USER_ACCOUNT_PWD" => md5($_memberAccountPw),
					"USER_EMAIL" => $_email,
					"USER_NM" => $_nickName,
					'USER_REAL_NM' => $_userRealNm,
					"USER_TP_CD" => $_memberTpCd,
					"USER_DESC" => $_memberDesc,
					"USER_PHONE1" => $_phoneNumber,
					"PHONE_CERT_YN" => $_memberPhoneCertyn,
					"GCM_ID" => $_gcmID,
					"DEL_YN" => "N",
					"REG_DT" => date("YmdHis")
				);
		
				$this->mysql->startTransaction();

				$ret = $this->mysql->insert("user_m", $newData);
				if(!$ret) {
					debug_mesg("E", __CLASS__, __FUNCTION__, __LINE__, $this->TAG . "[join()] : " . $this->mysql->getLastError());
					throw new Exception("FAIL_TO_JOIN");
				}
		
				$memberId = $this->mysql->getInsertId();
					
				$agreementData = array(
					"USER_ID" => $memberId,
					"SERV_USE_AGREE_YN" => $_policy_1,
					"SERV_USE_AGREE_DT" => date("YmdHis"),
					"PRIV_USE_AGREE_YN" => $_policy_2,
					"PRIV_USE_AGREE_DT" => date("YmdHis"),
					"LOC_USE_AGREE_YN" => $_policy_3,
					"LOC_USE_AGREE_DT" => date("YmdHis"),
					"EMAIL_RECV_AGREE_YN" => $_policy_4,
					"EMAIL_RECV_AGREE_DT" => date("YmdHis"),
					"SMS_RECV_AGREE_YN" => $_policy_5,
					"SMS_RECV_AGREE_DT" => date("YmdHis")
				);
					
				$ret = $this->mysql->insert("user_agreement_s", $agreementData);
				if(!$ret) {
					debug_mesg("E", __CLASS__, __FUNCTION__, __LINE__, $this->TAG . "[join()] : " . $this->mysql->getLastError());
					throw new Exception("FAIL_TO_JOIN");
				}
				
				$shopNewData = array(
					"SHOP_NM" => $_shopName,
					"SHOP_DESC" => "",
					"SHOP_IMG1" => "",
					"SHOP_IMG2" => "",
					"SHOP_IMG3" => "",
					"PDT_HIT" => 0,
					"PDT_AVG" => 0,
					"SHOP_LNG" => $_shopLng,
					"SHOP_LAT" => $_shopLat,
					"SHOP_LOC_CD" => "",
					"SHOP_ADDR" => $_shopAddress,
					"SHOP_POSTAL_CD" => $_shopPostCode,
					"SHOP_TEL" => "",
					"SHOP_OP_TIME" => "",
					"SHOP_ETC" => "",
					"DEL_YN" => "N",
					"REG_DT" => date("YmdHis")
				);
				
				$ret = $this->mysql->insert("shop_m", $shopNewData);
				if(!$ret) {
					debug_mesg("E", __CLASS__, __FUNCTION__, __LINE__, $this->TAG . "[join()] : " . $this->mysql->getLastError());
					throw new Exception("FAIL_TO_JOIN");
				}
				
				$shopId = $this->mysql->getInsertId();
				
				$managerNewData = array(
					"MANAGER_ID" => $memberId,
					"SHOP_ID" => $shopId,
					"APPLY_YN" => "Y",
					"APPLY_DT" => date("YmdHis")
				);
				
				$ret = $this->mysql->insert("user_manager_r", $managerNewData);
				if(!$ret) {
					debug_mesg("E", __CLASS__, __FUNCTION__, __LINE__, $this->TAG . "[join()] : " . $this->mysql->getLastError());
					throw new Exception("FAIL_TO_JOIN");
				}
				
				$userPhoneAuthNewData = array(
					"USER_ID" => $memberId,
					"USER_PHONE1" => $_phoneNumber,
					"USER_REAL_NM" => $_userRealNm,
					"USER_CI" => $_userCi,
					"USER_DI" => $_userDi,
					"REG_DT" => date("YmdHis")
				);
				
				$ret = $this->mysql->insert("user_phone_auth_r", $userPhoneAuthNewData);
				if(!$ret) {
					debug_mesg("E", __CLASS__, __FUNCTION__, __LINE__, $this->TAG . "[join()] : " . $this->mysql->getLastError());
					throw new Exception("FAIL_TO_JOIN");
				}
		
				$this->mysql->commit();
		
				return $memberId;
			} catch (Exception $e) {
				$this->mysql->rollback();
		
				debug_mesg("E", __CLASS__, __FUNCTION__, __LINE__, $this->TAG . "[join()] : " . $e->getMessage());
		
				if(getErrorCode($e->getMessage()) != null) {
					throw new Exception($e->getMessage());
				}
		
				throw new Exception("SERVER_INTERNAL_ERROR");
			}
		}
		
		function getMemberToken($_memberId, $_platform, $_platformVersion) {
			try {
				$updateData = array(
					"EXPIRED_DT" => date("YmdHis")
				);
				
				$this->mysql->where("USER_ID", $_memberId);
				$ret = $this->mysql->update("user_token", $updateData);
				
				$memberToken = generateMemberToken(time());
				$insertData = array(
					"USER_ID" => $_memberId,
					"TOKEN" => $memberToken,
					"PLATFORM" => $_platform,
					"PLATFORM_VERSION" => $_platformVersion,
					"REG_DT" => date("YmdHis")
				);
				
				$ret = $this->mysql->insert("user_token", $insertData);
				
				if(!$ret) {
					throw new Exception("FAIL_TO_GEN_TOKEN");
				}
				
				return $memberToken;
			} catch (Exception $e) {
				debug_mesg("E", __CLASS__, __FUNCTION__, __LINE__, $this->TAG . "[getMemberToken()] : " . $e->getMessage());
				
				if(getErrorCode($e->getMessage()) != null) {
					throw new Exception($e->getMessage());
				}
				
				throw new Exception("SERVER_INTERNAL_ERROR");
			}
		}
		
		function getMemberIdByToken($_token) {
			try {
				$this->mysql->where("TOKEN", $_token)->where("EXPIRED_DT", NULL, "IS");
				$memberId = $this->mysql->getValue("user_token", "USER_ID");
				
				if(!isset($memberId) || empty($memberId)) {
					throw new Exception("INVALID_TOKEN");
				}
		
				return $memberId;
			} catch (Exception $e) {
				debug_mesg("E", __CLASS__, __FUNCTION__, __LINE__, $this->TAG . "[getMemberIdByToken()] : " . $e->getMessage());
				
				if(getErrorCode($e->getMessage()) != null) {
					throw new Exception($e->getMessage());
				}
				
				throw new Exception("SERVER_INTERNAL_ERROR");
			}
		}
		
		function getShopIdByToken($_token) {
			try {
				$this->mysql->where("TOKEN", $_token)->where("EXPIRED_DT", NULL, "IS");
				$memberId = $this->mysql->getValue("user_token", "USER_ID");
		
				if(!isset($memberId) || empty($memberId)) {
					throw new Exception("INVALID_TOKEN");
				}
				
				$this->mysql->where("MANAGER_ID", $memberId)->where("APPLY_YN", "Y");
				$shopId = $this->mysql->getValue("user_manager_r", "SHOP_ID");
				
				if(!isset($shopId) || empty($shopId)) {
					throw new Exception("INVALID_TOKEN");
				}
		
				return $shopId;
			} catch (Exception $e) {
				debug_mesg("E", __CLASS__, __FUNCTION__, __LINE__, $this->TAG . "[getShopIdByToken()] : " . $e->getMessage());
		
				if(getErrorCode($e->getMessage()) != null) {
					throw new Exception($e->getMessage());
				}
		
				throw new Exception("SERVER_INTERNAL_ERROR");
			}
		}
		
		function getMemberInfo($_memberId) {
			try {
				$this->mysql->where("USER_ID", $_memberId);
				$memberInfo = $this->mysql->getOne("user_m", "USER_ID, USER_NM, USER_REAL_NM, USER_DESC, USER_EMAIL, USER_TP_CD, USER_ACCOUNT_ID, USER_PHONE1, PHONE_CERT_YN, GCM_ID");
				
				return $memberInfo;
			} catch (Exception $e) {
				debug_mesg("E", __CLASS__, __FUNCTION__, __LINE__, $this->TAG . "[getMemberInfo()] : " . $e->getMessage());
				
				if(getErrorCode($e->getMessage()) != null) {
					throw new Exception($e->getMessage());
				}
				
				throw new Exception("SERVER_INTERNAL_ERROR");
			}
		}
		
		function getMemberInfoByIdPw($_memberAccountId, $_memberAccountPw) {
			try {
				$this->mysql->where("USER_ACCOUNT_ID", $_memberAccountId)->where("USER_ACCOUNT_PWD", md5($_memberAccountPw));
				$memberInfo = $this->mysql->getOne("user_m", "USER_ID, USER_NM, USER_REAL_NM, USER_DESC, USER_EMAIL, USER_TP_CD, USER_ACCOUNT_ID, USER_PHONE1, PHONE_CERT_YN, GCM_ID");

				return $memberInfo;
			} catch (Exception $e) {
				debug_mesg("E", __CLASS__, __FUNCTION__, __LINE__, $this->TAG . "[getMemberInfoByIdPw()] : " . $e->getMessage());
		
				if(getErrorCode($e->getMessage()) != null) {
					throw new Exception($e->getMessage());
				}
		
				throw new Exception("SERVER_INTERNAL_ERROR");
			}
		}
		
		function getMemberInfoByEmailPhoneNumber($_email, $_phoneNumber) {
			try {
				$this->mysql->where("USER_EMAIL", $_email)->where("USER_PHONE1", $_phoneNumber)->where("USER_TP_CD", "003");
				$memberInfo = $this->mysql->getOne("user_m", "USER_ID, USER_NM, USER_DESC, USER_EMAIL, USER_TP_CD, USER_ACCOUNT_ID, USER_PHONE1, PHONE_CERT_YN");
		
				return $memberInfo;
			} catch (Exception $e) {
				debug_mesg("E", __CLASS__, __FUNCTION__, __LINE__, $this->TAG . "[getMemberInfoByEmailPhoneNumber()] : " . $e->getMessage());
		
				if(getErrorCode($e->getMessage()) != null) {
					throw new Exception($e->getMessage());
				}
		
				throw new Exception("SERVER_INTERNAL_ERROR");
			}
		}
		
		function getMemberInfoByNickNamePhoneNumber($_nickName, $_phoneNumber) {
			try {
				$this->mysql->where("USER_NM", $_nickName)->where("USER_PHONE1", $_phoneNumber)->where("USER_TP_CD", "001");
				$memberInfo = $this->mysql->getOne("user_m", "USER_ID, USER_NM, USER_DESC, USER_EMAIL, USER_TP_CD, USER_ACCOUNT_ID, USER_PHONE1, PHONE_CERT_YN");
		
				return $memberInfo;
			} catch (Exception $e) {
				debug_mesg("E", __CLASS__, __FUNCTION__, __LINE__, $this->TAG . "[getMemberInfoByNickNamePhoneNumber()] : " . $e->getMessage());
		
				if(getErrorCode($e->getMessage()) != null) {
					throw new Exception($e->getMessage());
				}
		
				throw new Exception("SERVER_INTERNAL_ERROR");
			}
		}
		
		function getMemberInfoByIdPhoneNumber($_memberAccountId, $_phoneNumber) {
			try {
				$this->mysql->where("USER_ACCOUNT_ID", $_memberAccountId)->where("USER_PHONE1", $_phoneNumber);
				$memberInfo = $this->mysql->getOne("user_m", "USER_ID, USER_NM, USER_DESC, USER_EMAIL, USER_TP_CD, USER_ACCOUNT_ID, USER_PHONE1, PHONE_CERT_YN");
		
				return $memberInfo;
			} catch (Exception $e) {
				debug_mesg("E", __CLASS__, __FUNCTION__, __LINE__, $this->TAG . "[getMemberInfoByIdPhoneNumber()] : " . $e->getMessage());
		
				if(getErrorCode($e->getMessage()) != null) {
					throw new Exception($e->getMessage());
				}
		
				throw new Exception("SERVER_INTERNAL_ERROR");
			}
		}
		
		function saveNewPassword($_memberId, $_phoneNumber, $_memberAccountPwd) {
			try {
				$updateData = array(
					"USER_ACCOUNT_PWD" => md5($_memberAccountPwd)
				);
				
				$this->mysql->where("USER_ID", $_memberId)->where("USER_PHONE1", $_phoneNumber);
				$ret = $this->mysql->update("user_m", $updateData);
		
				return $ret;
			} catch (Exception $e) {
				debug_mesg("E", __CLASS__, __FUNCTION__, __LINE__, $this->TAG . "[getMemberInfoByIdPhoneNumber()] : " . $e->getMessage());
		
				if(getErrorCode($e->getMessage()) != null) {
					throw new Exception($e->getMessage());
				}
		
				throw new Exception("SERVER_INTERNAL_ERROR");
			}
		}
		
		function getManagerInfo($_memberId) {
			try {
				$this->mysql->where("MANAGER_ID", $_memberId);
				$memberInfo = $this->mysql->getOne("user_manager_r", "MANAGER_ID, SHOP_ID, APPLY_YN, APPLY_DT");
		
				return $memberInfo;
			} catch (Exception $e) {
				debug_mesg("E", __CLASS__, __FUNCTION__, __LINE__, $this->TAG . "[getManagerInfo()] : " . $e->getMessage());
		
				if(getErrorCode($e->getMessage()) != null) {
					throw new Exception($e->getMessage());
				}
		
				throw new Exception("SERVER_INTERNAL_ERROR");
			}
		}
		
		function login($_memberAccountId, $_memberAccountPw, $_platform, $_platformVersion) {
			try {
				$memberInfo = $this->getMemberInfoByIdPw($_memberAccountId, $_memberAccountPw);
				if(!isset($memberInfo) || empty($memberInfo)) {
					throw new Exception("INVALID_ID_PW");
				}
				
				if($memberInfo["USER_TP_CD"] != "003") {
					throw new Exception("INVALID_ID_PW");
				}
				
				$this->mysql->startTransaction();
				
				$updateData = array(
					"LAST_LOGIN_DT" => date("YmdHis")
				);
				
				$this->mysql->where("USER_ID", $memberInfo["USER_ID"]);
				$ret = $this->mysql->update("user_m", $updateData);
				
				if(!$ret) {
					throw new Exception("FAIL_TO_LOGIN");
				}
				
				$memberToken = $this->getMemberToken($memberInfo["USER_ID"], $_platform, $_platformVersion);
				
				$this->mysql->commit();
				
				return array(
					"token" => $memberToken,
					"memberInfo" => $memberInfo
				);
			} catch (Exception $e) {
				$this->mysql->rollback();
				
				debug_mesg("E", __CLASS__, __FUNCTION__, __LINE__, $this->TAG . "[login()] : " . $e->getMessage());
				
				if(getErrorCode($e->getMessage()) != null) {
					throw new Exception($e->getMessage());
				}
				
				throw new Exception("SERVER_INTERNAL_ERROR");
			}
		}
		
		function managerLogin($_memberAccountId, $_memberAccountPw, $_platform, $_platformVersion) {
			try {
				$memberInfo = $this->getMemberInfoByIdPw($_memberAccountId, $_memberAccountPw, '001');
				if(!isset($memberInfo) || empty($memberInfo)) {
					throw new Exception("INVALID_ID_PW");
				}
		
				$managerInfo = $this->getManagerInfo($memberInfo["USER_ID"]);
				if(!isset($managerInfo) || empty($managerInfo)) {
					throw new Exception("ALLOW_MANAGER_ONLY");
				}
		
				if(!isset($managerInfo["APPLY_YN"]) || $managerInfo["APPLY_YN"] == "N") {
					throw new Exception("NOT_APPLY_MANAGER");
				}
		
				$this->mysql->startTransaction();
		
				$updateData = array(
					"LAST_LOGIN_DT" => date("YmdHis")
				);
		
				$this->mysql->where("USER_ID", $memberInfo["USER_ID"]);
				$ret = $this->mysql->update("user_m", $updateData);
		
				if(!$ret) {
					throw new Exception("FAIL_TO_LOGIN");
				}
		
				$memberToken = $this->getMemberToken($memberInfo["USER_ID"], $_platform, $_platformVersion);
		
				$this->mysql->commit();
		
				return array(
					"token" => $memberToken,
					"memberInfo" => $memberInfo
				);
			} catch (Exception $e) {
				$this->mysql->rollback();
		
				debug_mesg("E", __CLASS__, __FUNCTION__, __LINE__, $this->TAG . "[managerLogin()] : " . $e->getMessage());
		
				if(getErrorCode($e->getMessage()) != null) {
					throw new Exception($e->getMessage());
				}
		
				throw new Exception("SERVER_INTERNAL_ERROR");
			}
		}
		
		function recvCoupon($_memberId, $_shopId, $_couponId) {
			try {
				$this->getShopServiceClass();
				
				$couponDetail = $this->ShopService->getShopCouponDetail($_shopId, $_couponId);
				if(!isset($couponDetail)) {
					throw new Exception("FAIL_TO_GET_COUPON");
				}
		
				$newData = array(
					"" => ""
				);
		
				$this->mysql->where("T1.USER_ID", $_memberId)->where("T1.ORDER_DT BETWEEN '" . $_startDate . "' AND '" . $_endDate . "'");
				$this->mysql->join('SHOP_PRODUCT_M T2', 'T1.PDT_ID = T2.PDT_ID', 'INNER');
				$this->mysql->join('SHOP_M T3', 'T1.SHOP_ID = T3.SHOP_ID', 'INNER');
				$this->mysql->orderBy('T1.ORDER_ID', 'desc');
				$my_history_list = $this->mysql->get("ORDER_M T1", null, "T1.ORDER_ID, T1.ORDER_DT, T2.PDT_NM, T2.ELAPSE_TM, T3.SHOP_NM, T3.SHOP_TEL,
																			T3.SHOP_ADDR, T3.SHOP_ID, T3.SHOP_IMG1, T3.SHOP_IMG2, T3.SHOP_IMG3, (SELECT CODE_NM FROM CODE_M C WHERE C.TBL_NM = 'SHOP_M' AND C.COL_NM = 'SHOP_LOC_CD' AND C.SUB_CD = SHOP_LOC_CD) AS SHOP_LOC_CD_TXT,
																			T2.PDT_ID");
		
				if(!isset($my_history_list)) {
					$my_history_list = array();
				}
		
				return $my_history_list;
			} catch (Exception $e) {
				debug_mesg("E", __CLASS__, __FUNCTION__, __LINE__, $this->TAG . "[recvCoupon()] : " . $e->getMessage());
		
				if(getErrorCode($e->getMessage()) != null) {
					throw new Exception($e->getMessage());
				}
		
				throw new Exception("SERVER_INTERNAL_ERROR");
			}
		}
		
		function searchCustomer($_searchType, $_searchText) {
			try {
				if($_searchType == "name") {
					$this->mysql->where("USER_REAL_NM LIKE '" . $_searchText . "%'");
				} else if($_searchType == "phone") {
					$this->mysql->where("USER_PHONE1 LIKE '%" . $_searchText . "'");
				} else {
					throw new Exception("NOT_ENOUGH_PARAM");
				}
				
				$this->mysql->where("USER_TP_CD", "003");
				
				$memberList = $this->mysql->get("user_m", null, "USER_ID, USER_NM, USER_PHONE1");
				
				if(!isset($memberList)) {
					$memberList = array();
				}
		
				return $memberList;
			} catch (Exception $e) {
				debug_mesg("E", __CLASS__, __FUNCTION__, __LINE__, $this->TAG . "[searchCustomer()] : " . $e->getMessage());
		
				if(getErrorCode($e->getMessage()) != null) {
					throw new Exception($e->getMessage());
				}
		
				throw new Exception("SERVER_INTERNAL_ERROR");
			}
		}
		
		function updateGcmId($_memberId, $_gcmId) {
			try {
				$updateData = array(
					"GCM_ID" => $_gcmId
				);
		
				$this->mysql->where("USER_ID", $_memberId);
				$ret = $this->mysql->update("user_m", $updateData);
		
				return $ret;
			} catch (Exception $e) {
				debug_mesg("E", __CLASS__, __FUNCTION__, __LINE__, $this->TAG . "[updateGcmId()] : " . $e->getMessage());
		
				if(getErrorCode($e->getMessage()) != null) {
					throw new Exception($e->getMessage());
				}
		
				throw new Exception("SERVER_INTERNAL_ERROR");
			}
		}
		
		function getMemberGcmId($_memberId) {
			try {
				$this->mysql->where("USER_ID", $_memberId);
				$memberGcmId = $this->mysql->getValue("USER_M", "GCM_ID");
		
				return $memberGcmId;
			} catch (Exception $e) {
				debug_mesg("E", __CLASS__, __FUNCTION__, __LINE__, $this->TAG . "[getMemberGcmId()] : " . $e->getMessage());
		
				if(getErrorCode($e->getMessage()) != null) {
					throw new Exception($e->getMessage());
				}
		
				throw new Exception("SERVER_INTERNAL_ERROR");
			}
		}
	}
?>