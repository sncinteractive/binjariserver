<?php
	class COKNameService {
		var $TAG = "COKNameService";
		var $mysql;
		
		function COKNameService($_mysql) {
			$this->mysql = $_mysql;
		}
		
		function saveTempPhoneAuth($_token, $_phoneNumber, $_userRealNM, $_userCi, $_userDi) {
			try {
				$newData = array(
					'TOKEN' => $_token,
					'USER_PHONE1' => $_phoneNumber,
					'USER_REAL_NM' => $_userRealNM,
					'USER_CI' => $_userCi,
					'USER_DI' => $_userDi
				);
				
				$this->mysql->insert("TEMP_PHONE_AUTH", $newData);
			} catch (Exception $e) {
				debug_mesg("E", __CLASS__, __FUNCTION__, __LINE__, $this->TAG . "[saveTempPhoneAuth()] : " . $e->getMessage());
				
				if(getErrorCode($e->getMessage()) != null) {
					throw new Exception($e->getMessage());
				}
				
				throw new Exception("SERVER_INTERNAL_ERROR");
			}
		}
		
		function getTempPhoneAuth($_token) {
			try {
				$this->mysql->where('TOKEN', $_token);
				$phoneAuthData = $this->mysql->getOne("TEMP_PHONE_AUTH", 'PHONE_AUTH_TOKEN_ID, TOKEN, USER_PHONE1, USER_REAL_NM, USER_CI, USER_DI');
				
				if(isset($phoneAuthData)) {
					$this->deleteTempPhoneAuth($_token);
				}
				
				return $phoneAuthData;
			} catch (Exception $e) {
				debug_mesg("E", __CLASS__, __FUNCTION__, __LINE__, $this->TAG . "[getTempPhoneAuth()] : " . $e->getMessage());
		
				if(getErrorCode($e->getMessage()) != null) {
					throw new Exception($e->getMessage());
				}
		
				throw new Exception("SERVER_INTERNAL_ERROR");
			}
		}
		
		function deleteTempPhoneAuth($_token) {
			try {
				$this->mysql->where('TOKEN', $_token);
				$this->mysql->delete("TEMP_PHONE_AUTH");
			} catch (Exception $e) {
				debug_mesg("E", __CLASS__, __FUNCTION__, __LINE__, $this->TAG . "[deleteTempPhoneAuth()] : " . $e->getMessage());
		
				if(getErrorCode($e->getMessage()) != null) {
					throw new Exception($e->getMessage());
				}
		
				throw new Exception("SERVER_INTERNAL_ERROR");
			}
		}
	}
?>