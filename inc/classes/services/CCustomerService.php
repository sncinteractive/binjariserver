<?php
	class CCustomerService {
		var $TAG = "CCustomerService";
		var $mysql;
		
		var $ShopService;
		var $MemberService;

		function CCustomerService($_mysql) {
			$this->mysql = $_mysql;
		}
		
		function getShopServiceClass() {
			if ($this->ShopService) {
				return;
			}
		
			require_once_services(Array('CShopService'));
			$this->ShopService = new CShopService($this->mysql);
		}
		
		function getMemberServiceClass() {
			if ($this->MemberService) {
				return;
			}
		
			require_once_services(Array('CMemberService'));
			$this->MemberService = new CMemberService($this->mysql);
		}
		
		function getCustomerList($_shopId, $_searchType, $_searchVal, $_shopGradeCd) {
			try {
				$sql = "";
				
				if($_shopGradeCd == "000") {
					if($_searchType == "visit") {
						$sql .= "SELECT A.*, IFNULL(B.TOTAL_ORDERED_PRICE, 0) AS TOTAL_ORDERED_PRICE, C.FIRST_ORDER_DT, C.LAST_ORDER_DT,  IFNULL(C.VISIT_CNT, 0) AS VISIT_CNT, D.SHOP_GR_CD, D.INTERCEPTION_YN";
						$sql .= " FROM USER_M A LEFT OUTER JOIN";
						$sql .= "	(";
						$sql .= "		SELECT O.USER_ID, SUM(O.ORDERED_PRICE) AS TOTAL_ORDERED_PRICE";
						$sql .= "		FROM ORDER_M O";
						$sql .= "		WHERE O.SHOP_ID = " . $_shopId . " AND O.ORDER_TP_CD = '003'";
						$sql .= "		GROUP BY O.USER_ID";
						$sql .= "	) B ON A.USER_ID = B.USER_ID";
						$sql .= "	LEFT OUTER JOIN";
						$sql .= "	(";
						$sql .= "		SELECT O.USER_ID, COUNT(O.ORDER_ID) AS VISIT_CNT, MIN(O.ORDER_DT) AS FIRST_ORDER_DT, MAX(O.ORDER_DT) AS LAST_ORDER_DT";
						$sql .= "		FROM ORDER_M O";
						$sql .= "		WHERE O.SHOP_ID = " . $_shopId;
						$sql .= "		GROUP BY O.USER_ID";
						$sql .= "	) C ON A.USER_ID = C.USER_ID,";
						$sql .= "	SHOP_USER_GRADE_R D";
						$sql .= " WHERE A.USER_ID = D.USER_ID AND D.SHOP_ID = " . $_shopId . " AND A.DEL_YN = 'N'";
						$sql .= " AND C.VISIT_CNT > " . $_searchVal;		 // 방문 횟수 검색
					} else if($_searchType == "price") {
						$sql .= "SELECT A.*, IFNULL(B.TOTAL_ORDERED_PRICE, 0) AS TOTAL_ORDERED_PRICE, C.FIRST_ORDER_DT, C.LAST_ORDER_DT,  IFNULL(C.VISIT_CNT, 0) AS VISIT_CNT, D.SHOP_GR_CD, D.INTERCEPTION_YN";
						$sql .= " FROM USER_M A LEFT OUTER JOIN";
						$sql .= "	(";
						$sql .= "		SELECT O.USER_ID, SUM(O.ORDERED_PRICE) AS TOTAL_ORDERED_PRICE";
						$sql .= "		FROM ORDER_M O";
						$sql .= "		WHERE O.SHOP_ID = " . $_shopId . " AND O.ORDER_TP_CD = '003'";
						$sql .= "		GROUP BY O.USER_ID";
						$sql .= "	) B ON A.USER_ID = B.USER_ID";
						$sql .= "	LEFT OUTER JOIN";
						$sql .= "	(";
						$sql .= "		SELECT O.USER_ID, COUNT(O.ORDER_ID) AS VISIT_CNT, MIN(O.ORDER_DT) AS FIRST_ORDER_DT, MAX(O.ORDER_DT) AS LAST_ORDER_DT";
						$sql .= "		FROM ORDER_M O";
						$sql .= "		WHERE O.SHOP_ID = " . $_shopId;
						$sql .= "		GROUP BY O.USER_ID";
						$sql .= "	) C ON A.USER_ID = C.USER_ID,";
						$sql .= "	SHOP_USER_GRADE_R D";
						$sql .= " WHERE A.USER_ID = D.USER_ID AND D.SHOP_ID = " . $_shopId . " AND A.DEL_YN = 'N'";
						$sql .= " AND B.TOTAL_ORDERED_PRICE > " . $_searchVal; // 결제 금액 검색
					} else if($_searchType == "name") {
						$sql .= "SELECT A.* FROM USER_M A INNER JOIN SHOP_USER_GRADE_R B ON A.USER_ID = B.USER_ID";
						$sql .= " WHERE A.DEL_YN = 'N' AND B.SHOP_ID = " . $_shopId . " AND A.USER_REAL_NM like '%" . $_searchVal . "%'";	// 이름 검색
					} else if($_searchType == "phone") {
						$sql .= "SELECT A.* FROM USER_M A INNER JOIN SHOP_USER_GRADE_R B ON A.USER_ID = B.USER_ID";
						$sql .= " WHERE A.DEL_YN = 'N' AND B.SHOP_ID = " . $_shopId . " AND A.USER_PHONE1 like '%" . $_searchVal . "'";		// 전화번호 검색
					}
				} else if($_shopGradeCd == "001" || $_shopGradeCd == "002") {
					$sql .= "SELECT A.*, B.SHOP_GR_CD, B.SHOP_GR_MEMO, B.INTERCEPTION_YN FROM USER_M A INNER JOIN SHOP_USER_GRADE_R B ON A.USER_ID = B.USER_ID";
					$sql .= " WHERE A.DEL_YN = 'N' AND B.SHOP_ID = " . $_shopId . " AND B.SHOP_GR_CD = '" . $_shopGradeCd . "'";	// -- 단골/진상 고객 선별 조회
				}

				$sql .= " ORDER BY A.USER_NM;";
								
				$list = $this->mysql->rawQuery($sql);
		
				if(!isset($list) || empty($list)) {
					$list = array();
				}
					
				return $list;
			} catch (Exception $e) {
				debug_mesg("E", __CLASS__, __FUNCTION__, __LINE__, $this->TAG . "[getCustomerList()] : " . $e->getMessage());
				throw new Exception("SERVER_INTERNAL_ERROR");
			}
		}
		
		function registerCustomer($_shopId, $_userName, $_userPhone, $_userEmail, $_userNickName, $_userTpCd, $_userDesc, $_shopGradeCd) {
			try {
				$newData = array(
					"USER_ACCOUNT_ID" => "",
					"USER_ACCOUNT_PWD" => "",
					"USER_EMAIL" => $_userEmail,
					"USER_NM" => $_userNickName,
					"USER_REAL_NM" => $_userName,
					"USER_TP_CD" => $_userTpCd,
					"USER_DESC" => $_userDesc,
					"USER_PHONE1" => $_userPhone,
					"PHONE_CERT_YN" => "N",
					"DEL_YN" => "N",
					"REG_DT" => date("YmdHis")
				);
			
				$this->mysql->startTransaction();
			
				$ret = $this->mysql->insert("user_m", $newData);
				if(!$ret) {
					debug_mesg("E", __CLASS__, __FUNCTION__, __LINE__, $this->TAG . "[registerCustomer() - user_m] : " . $this->mysql->getLastError());
					throw new Exception("FAIL_TO_REGISTER");
				}
			
				$memberId = $this->mysql->getInsertId();
					
				$agreementData = array(
					"USER_ID" => $memberId,
					"SERV_USE_AGREE_YN" => "N",
					"SERV_USE_AGREE_DT" => date("YmdHis"),
					"PRIV_USE_AGREE_YN" => "N",
					"PRIV_USE_AGREE_DT" => date("YmdHis"),
					"LOC_USE_AGREE_YN" => "N",
					"LOC_USE_AGREE_DT" => date("YmdHis")
				);
					
				$ret = $this->mysql->insert("user_agreement_s", $agreementData);
			
				if(!$ret) {
					debug_mesg("E", __CLASS__, __FUNCTION__, __LINE__, $this->TAG . "[registerCustomer() - user_agreement_s] : " . $this->mysql->getLastError());
					throw new Exception("FAIL_TO_REGISTER");
				}
				
				$shopUserGradeData = array(
					"SHOP_ID" => $_shopId,
					"USER_ID" => $memberId,
					"SHOP_GR_CD" => $_shopGradeCd,
					"SHOP_GR_MEMO" => "",
					"INTERCEPTION_YN" => "N",
					"REG_DT" => date("YmdHis")
				);
				
				$ret = $this->mysql->insert("SHOP_USER_GRADE_R", $shopUserGradeData);
					
				if(!$ret) {
					debug_mesg("E", __CLASS__, __FUNCTION__, __LINE__, $this->TAG . "[registerCustomer() - shop_user_grade_r] : " . $this->mysql->getLastError());
					throw new Exception("FAIL_TO_REGISTER");
				}
			
				$this->mysql->commit();
			
				return $memberId;
			} catch (Exception $e) {
				$this->mysql->rollback();
			
				debug_mesg("E", __CLASS__, __FUNCTION__, __LINE__, $this->TAG . "[registerCustomer()] : " . $e->getMessage());
			
				if(getErrorCode($e->getMessage()) != null) {
					throw new Exception($e->getMessage());
				}
			
				throw new Exception("SERVER_INTERNAL_ERROR");
			}
		}
		
		function editCustomer($_shopId, $_memberId, $_userName, $_userPhone, $_userEmail, $_userNickName, $_shopGradeCd) {
			try {
				$this->mysql->where("USER_ID", $_memberId);
				$customerTpCd = $this->mysql->getValue("user_m", "USER_TP_CD");
					
// 				if($customerTpCd != "004") {
// 					throw new Exception("ALLOW_EDIT_NON_MEMBER_ONLY");
// 				}

				$this->mysql->startTransaction();
				
				if($customerTpCd == "004") {
					$updateData = array(
						"USER_REAL_NM" => $_userName,
						"USER_EMAIL" => $_userEmail,
						"USER_NM" => $_userNickName,
						"USER_PHONE1" => $_userPhone
					);
					
					$this->mysql->where("USER_ID", $_memberId);
					$ret = $this->mysql->update("USER_M", $updateData);
					if(!$ret) {
						debug_mesg("E", __CLASS__, __FUNCTION__, __LINE__, $this->TAG . "[editCustomer() - user_m] : " . $this->mysql->getLastError());
						throw new Exception("FAIL_TO_EDIT");
					}
				}
					
				$shopUserGradeData = array(
					"SHOP_GR_CD" => $_shopGradeCd
				);
				
				if($_shopGradeCd == "000" || $_shopGradeCd == "001") {
					$shopUserGradeData["INTERCEPTION_YN"] = "Y";
// 					$shopUserGradeData["SHOP_GR_MEMO"] = "";
				}
					
				$this->mysql->where("USER_ID", $_memberId)->where("SHOP_ID", $_shopId);
				$ret = $this->mysql->update("SHOP_USER_GRADE_R", $shopUserGradeData);
				
				if(!$ret) {
					debug_mesg("E", __CLASS__, __FUNCTION__, __LINE__, $this->TAG . "[editCustomer() - shop_user_grade_r] : " . $this->mysql->getLastError());
					throw new Exception("FAIL_TO_EDIT");
				}
				
				$this->mysql->commit();
			} catch (Exception $e) {
				$this->mysql->rollback();
			
				debug_mesg("E", __CLASS__, __FUNCTION__, __LINE__, $this->TAG . "[editCustomer()] : " . $e->getMessage());
			
				if(getErrorCode($e->getMessage()) != null) {
					throw new Exception($e->getMessage());
				}
			
				throw new Exception("SERVER_INTERNAL_ERROR");
			}
				
			return true;
		}
		
		function getCustomerDetail($_shopId, $_memberId) {
			try {
				$sql = "SELECT A.*, IFNULL(B.TOTAL_ORDERED_PRICE, 0) AS TOTAL_ORDERED_PRICE, C.FIRST_ORDER_DT, C.LAST_ORDER_DT,  IFNULL(C.VISIT_CNT, 0) AS VISIT_CNT, D.SHOP_GR_CD, D.INTERCEPTION_YN
						FROM USER_M A LEFT OUTER JOIN
						 (
						  SELECT O.USER_ID, SUM(O.ORDERED_PRICE) AS TOTAL_ORDERED_PRICE
						        FROM ORDER_M O
						        WHERE O.SHOP_ID = " . $_shopId . " AND O.ORDER_TP_CD = '003'
						        GROUP BY O.USER_ID
						 ) B ON A.USER_ID = B.USER_ID
						    LEFT OUTER JOIN
						 (
						  SELECT O.USER_ID, COUNT(O.ORDER_ID) AS VISIT_CNT, MIN(O.ORDER_DT) AS FIRST_ORDER_DT, MAX(O.ORDER_DT) AS LAST_ORDER_DT
						        FROM ORDER_M O
						        WHERE O.SHOP_ID = " . $_shopId . " AND O.ORDER_TP_CD = '003'
						        GROUP BY O.USER_ID
						 ) C ON A.USER_ID = C.USER_ID,
						    SHOP_USER_GRADE_R D
				
						WHERE A.USER_ID = D.USER_ID AND D.SHOP_ID = " . $_shopId . " AND D.USER_ID = " . $_memberId;
					
				$customerDetail = $this->mysql->rawQuery($sql);
					
				if(!isset($customerDetail) || empty($customerDetail)) {
					throw new Exception("FAIL_TO_GET_CUSTOMER_DETAIL");
				}
				
				return $customerDetail[0];
			} catch (Exception $e) {
				debug_mesg("E", __CLASS__, __FUNCTION__, __LINE__, $this->TAG . "[getCustomerDetail()] : " . $e->getMessage());
					
				if(getErrorCode($e->getMessage()) != null) {
					throw new Exception($e->getMessage());
				}
					
				throw new Exception("SERVER_INTERNAL_ERROR");
			}
		}
		
		function deleteCustomer($_shopId, $_memberId) {
			try {
				$this->mysql->where("USER_ID", $_memberId);
				$customerTpCd = $this->mysql->getValue("user_m", "USER_TP_CD");
				
				if($customerTpCd != "004") {
					throw new Exception("ALLOW_DELETE_NON_MEMBER_ONLY");
				}
					
				$updateData = array(
					"DEL_YN" => "Y"
				);
				
				$this->mysql->where("USER_ID", $_memberId);
				$ret = $this->mysql->update("user_m", $updateData);
				if(!$ret) {
					debug_mesg("E", __CLASS__, __FUNCTION__, __LINE__, $this->TAG . "[deleteCustomer] : " . $this->mysql->getLastError());
					throw new Exception("FAIL_TO_DELETE_CUSTOMER");
				}
			} catch (Exception $e) {
				debug_mesg("E", __CLASS__, __FUNCTION__, __LINE__, $this->TAG . "[deleteCustomer()] : " . $e->getMessage());
					
				if(getErrorCode($e->getMessage()) != null) {
					throw new Exception($e->getMessage());
				}
					
				throw new Exception("SERVER_INTERNAL_ERROR");
			}
		}
		
		function editRegularCustomer($_shopId, $_memberId, $_shopGradeCd, $_shopGradeMemo) {
			try {
// 				$this->mysql->where("USER_ID", $_memberId);
// 				$customerTpCd = $this->mysql->getValue("user_m", "USER_TP_CD");
// 				if($customerTpCd != "004") {
// 					throw new Exception("ALLOW_EDIT_NON_MEMBER_ONLY");
// 				}
				
				$shopUserGradeData = array(
					"SHOP_GR_CD" => $_shopGradeCd,
					"SHOP_GR_MEMO" => $_shopGradeMemo
				);
				
				$this->mysql->where("USER_ID", $_memberId)->where("SHOP_ID", $_shopId);
				$ret = $this->mysql->update("shop_user_grade_r", $shopUserGradeData);
					
				if(!$ret) {
					debug_mesg("E", __CLASS__, __FUNCTION__, __LINE__, $this->TAG . "[editRegularCustomer()] : " . $this->mysql->getLastError());
					throw new Exception("FAIL_TO_EDIT");
				}
					
				return true;
			} catch (Exception $e) {
				debug_mesg("E", __CLASS__, __FUNCTION__, __LINE__, $this->TAG . "[editRegularCustomer()] : " . $e->getMessage());
					
				if(getErrorCode($e->getMessage()) != null) {
					throw new Exception($e->getMessage());
				}
					
				throw new Exception("SERVER_INTERNAL_ERROR");
			}
		}
		
		function editCustomerInterception($_shopId, $_memberId, $_interceptionYn) {
			try {
				$this->mysql->where("USER_ID", $_memberId)->where("SHOP_ID", $_shopId);
				$shopGradeCd = $this->mysql->getValue("shop_user_grade_r", "SHOP_GR_CD");
					
				if($shopGradeCd != "002") {
					throw new Exception("ALLOW_EDIT_BLOCKMEMBER_ONLY");
				}
					
				$shopUserGradeData = array(
						"INTERCEPTION_YN" => $_interceptionYn
				);
					
				$this->mysql->where("USER_ID", $_memberId)->where("SHOP_ID", $_shopId);
				$ret = $this->mysql->update("shop_user_grade_r", $shopUserGradeData);
				
				if(!$ret) {
					debug_mesg("E", __CLASS__, __FUNCTION__, __LINE__, $this->TAG . "[editCustomerInterception()] : " . $this->mysql->getLastError());
					throw new Exception("FAIL_TO_EDIT");
				}
				
				return true;
			} catch (Exception $e) {
				debug_mesg("E", __CLASS__, __FUNCTION__, __LINE__, $this->TAG . "[editCustomerInterception()] : " . $e->getMessage());
			
				if(getErrorCode($e->getMessage()) != null) {
					throw new Exception($e->getMessage());
				}
			
				throw new Exception("SERVER_INTERNAL_ERROR");
			}
		}
		
		function getCustomerBookingList($_shopId, $_memberId) {
			try {
				$sql = "SELECT A.*, (SELECT CODE_NM FROM CODE_M WHERE TBL_NM = 'ORDER_M' AND COL_NM = 'ORDER_GUBUN_CD' AND SUB_CD = A.ORDER_GUBUN_CD) AS ORDER_GUBUN_CD_TXT, B.SHOP_COUP_ID, D.EVT_ID, M.USER_NM AS MANAGER_NM, P.PDT_NM, P.ELAPSE_TM
					FROM ORDER_M A  LEFT OUTER JOIN ORDER_COUP_R B ON A.ORDER_ID = B.ORDER_ID
					    LEFT OUTER JOIN PRODUCT_EVENT_R C ON A.PDT_ID = C.PDT_ID AND A.SHOP_ID = C.SHOP_ID AND C.DEL_YN = 'N'
					    LEFT OUTER JOIN EVENT_M D ON C.EVT_ID = D.EVT_ID AND NOW()+0 BETWEEN D.EVT_START_DT AND D.EVT_END_DT AND D.DEL_YN = 'N',
					  USER_M M,
					     SHOP_PRODUCT_M P
					WHERE A.SHOP_ID = " . $_shopId . " AND A.USER_ID = " . $_memberId . " AND A.MANAGER_ID = M.USER_ID AND A.PDT_ID = P.PDT_ID
					ORDER BY A.ORDER_DT DESC";
				
				$customerBookingList = $this->mysql->rawQuery($sql);
				
				if(!isset($customerBookingList) || empty($customerBookingList)) {
					$customerBookingList = array();
				}
					
				return $customerBookingList;
			} catch (Exception $e) {
				debug_mesg("E", __CLASS__, __FUNCTION__, __LINE__, $this->TAG . "[getCustomerBookingList()] : " . $e->getMessage());
					
				if(getErrorCode($e->getMessage()) != null) {
					throw new Exception($e->getMessage());
				}
					
				throw new Exception("SERVER_INTERNAL_ERROR");
			}
		}
		
		function getCustomerCouponList($_shopId, $_memberId) {
			try {
				$sql = "SELECT A.*, IF(ISNULL(B.SHOP_COUP_ID), 'Y', 'N') AS CAN_SEND_YN 
						FROM SHOP_COUPON_R A LEFT OUTER JOIN USER_COUP_R B ON A.SHOP_COUP_ID = B.SHOP_COUP_ID AND B.USER_ID = " . $_memberId . "
						WHERE A.SHOP_ID = " . $_shopId . " AND A.DEL_YN = 'N' AND A.USE_YN = 'Y' AND NOW()+0 < A.END_DT 
						ORDER BY A.COUP_NM";
			
				$customerCouponList = $this->mysql->rawQuery($sql);
			
				if(!isset($customerCouponList) || empty($customerCouponList)) {
					$customerCouponList = array();
				}
					
				return $customerCouponList;
			} catch (Exception $e) {
				debug_mesg("E", __CLASS__, __FUNCTION__, __LINE__, $this->TAG . "[getCustomerCouponList()] : " . $e->getMessage());
					
				if(getErrorCode($e->getMessage()) != null) {
					throw new Exception($e->getMessage());
				}
					
				throw new Exception("SERVER_INTERNAL_ERROR");
			}
		}
		
		function sendCoupon($_shopId, $_memberId, $_shopCouponId) {
			try {
				$newData = array(
					"SHOP_COUP_ID" => $_shopCouponId,
					"USER_ID" => $_memberId,
					"DEL_YN" => "N",
					"REG_DT" => date("YmdHis")
				);
					
				$ret = $this->mysql->insert("USER_COUP_R", $newData);
				if(!$ret) {
					debug_mesg("E", __CLASS__, __FUNCTION__, __LINE__, $this->TAG . "[sendCoupon()] : " . $this->mysql->getLastError());
					throw new Exception("FAIL_TO_SEND_COUPON");
				}
				
				$this->getMemberServiceClass();
				$this->getShopServiceClass();
				
				$memberInfo = $this->MemberService->getMemberInfo($_memberId);
				$shopInfo = $this->ShopService->getShopDetail($_shopId);
					
				if(isset($memberInfo) && isset($shopInfo)) {
					$notification = array(
						"title" => "쿠폰 발행",
						"body" => $shopInfo["SHOP_NM"] . "에서 쿠폰이 발행되었습니다."
					);
						
					$data = array(
						"messageType" => "sendCoupon",
						"customerName" => $memberInfo["USER_REAL_NM"],
						"shopName" => $shopInfo["SHOP_NM"]
					);
						
					send_notification(GOOGLE_SERVER_KEY_USER, $memberInfo["GCM_ID"], $data, $notification);
				}
					
				return true;
			} catch (Exception $e) {
				debug_mesg("E", __CLASS__, __FUNCTION__, __LINE__, $this->TAG . "[sendCoupon()] : " . $e->getMessage());
					
				if(getErrorCode($e->getMessage()) != null) {
					throw new Exception($e->getMessage());
				}
					
				throw new Exception("SERVER_INTERNAL_ERROR");
			}
		}
		
		function getRecentCustomerList($_shopId) {
			try {
				$sql = "SELECT T1.SHOP_ID, T1.SHOP_GR_CD, T1.SHOP_GR_MEMO, T1.INTERCEPTION_YN, T2.USER_ID, T2.USER_NM, T2.USER_REAL_NM, T2.USER_DESC, T2.USER_EMAIL, T2.USER_TP_CD, T2.USER_ACCOUNT_ID, T2.USER_PHONE1
						FROM SHOP_USER_GRADE_R T1 INNER JOIN USER_M T2 ON T1.USER_ID = T2.USER_ID AND T2.DEL_YN = 'N'
						WHERE T1.SHOP_ID = " . $_shopId . " AND T1.REG_DT >= (NOW() - INTERVAL 1 MONTH) + 0;";
					
				$customerList = $this->mysql->rawQuery($sql);
					
				if(!isset($customerList) || empty($customerList)) {
					$customerList = array();
				}
					
				return $customerList;
			} catch (Exception $e) {
				debug_mesg("E", __CLASS__, __FUNCTION__, __LINE__, $this->TAG . "[getRecentCustomerList()] : " . $e->getMessage());
					
				if(getErrorCode($e->getMessage()) != null) {
					throw new Exception($e->getMessage());
				}
					
				throw new Exception("SERVER_INTERNAL_ERROR");
			}
		}
	}
?>