<?php
	class CShopService {
		var $TAG = "CShopService";
		var $mysql;
		
		var $MemberService;
		var $PushService;
				
		function CShopService($_mysql) {
			$this->mysql = $_mysql;
		}
		
		function getMemberServiceClass() {
			if ($this->MemberService) {
				return;
			}
		
			require_once_services(Array('CMemberService'));
			$this->MemberService = new CMemberService($this->mysql);
		}
		
		function getPushServiceClass() {
			if ($this->PushService) {
				return;
			}
		
			require_once_services(Array('CPushService'));
			$this->PushService = new CPushService($this->mysql);
		}
		
		function getShopListByDistance($_lat, $_lng, $_shopLocCd, $_range, $_page) {
			try {
				$this->mysql->where("T2.DEL_YN", "N")->where("T2.SHOP_LOC_CD", $_shopLocCd);
// 				$this->mysql->having('T2.DISTANCE <= ' . $_range);
				$this->mysql->orderBy('DISTANCE', 'asc')->orderBy('T2.SHOP_NM', 'asc');
				$shop_list = $this->mysql->paginate("SHOP_M T2", $_page, "T2.SHOP_ID, T2.MANAGER_ID, T2.SHOP_NM, T2.SHOP_DESC, T2.SHOP_IMG1, T2.SHOP_IMG2, T2.SHOP_IMG3, T2.PDT_HIT, T2.PDT_AVG, 
																		T2.SHOP_LNG, T2.SHOP_LAT,
																		(6371*acos(cos(radians(" . $_lat . "))*cos(radians(T2.SHOP_LAT))*cos(radians(T2.SHOP_LNG)-radians(" . $_lng . "))+sin(radians(" . $_lat . "))*sin(radians(T2.SHOP_LAT)))) AS DISTANCE,
																		T2.SHOP_LOC_CD, (SELECT C.CODE_NM FROM CODE_M C WHERE C.TBL_NM = 'SHOP_M' AND C.COL_NM = 'SHOP_LOC_CD' AND C.SUB_CD = T2.SHOP_LOC_CD) AS SHOP_LOC_CD_TXT,
																		IFNULL((SELECT T1.PDT_PRIC FROM SHOP_PRODUCT_M T1 WHERE T1.SHOP_ID = T2.SHOP_ID ORDER BY T1.PDT_PRIC DESC LIMIT 1), 0) AS MIN_PDT_PRICE,
																		T2.SHOP_ADDR, T2.SHOP_POSTAL_CD, T2.SHOP_TEL, T2.SHOP_OP_TIME, T2.SHOP_ETC");
				
				return $shop_list;
			} catch (Exception $e) {
				debug_mesg("E", __CLASS__, __FUNCTION__, __LINE__, $this->TAG . "[getShopListByDistance()] : " . $e->getMessage());
				throw new Exception("SERVER_INTERNAL_ERROR");
			}
		}
		
		function getShopListByRecommend($_shopLocCd, $_page) {
			try {
				$this->mysql->where("T2.DEL_YN", "N")->where("T2.SHOP_LOC_CD", $_shopLocCd);
				$this->mysql->orderBy('T2.PDT_AVG', 'desc')->orderBy('T2.SHOP_NM', 'asc');
				$shop_list = $this->mysql->paginate("SHOP_M T2", $_page, "T2.SHOP_ID, T2.MANAGER_ID, T2.SHOP_NM, T2.SHOP_DESC, T2.SHOP_IMG1, T2.SHOP_IMG2, T2.SHOP_IMG3, T2.PDT_HIT, T2.PDT_AVG, 
																		T2.SHOP_LNG, T2.SHOP_LAT, T2.SHOP_LOC_CD, 
																		(SELECT C.CODE_NM FROM CODE_M C WHERE C.TBL_NM = 'SHOP_M' AND C.COL_NM = 'SHOP_LOC_CD' AND C.SUB_CD = T2.SHOP_LOC_CD) AS SHOP_LOC_CD_TXT,
																		IFNULL((SELECT T1.PDT_PRIC FROM SHOP_PRODUCT_M T1 WHERE T1.SHOP_ID = T2.SHOP_ID ORDER BY T1.PDT_PRIC DESC LIMIT 1), 0) AS MIN_PDT_PRICE,
																		T2.SHOP_ADDR, T2.SHOP_POSTAL_CD, T2.SHOP_TEL, T2.SHOP_OP_TIME, T2.SHOP_ETC");
		
				return $shop_list;
			} catch (Exception $e) {
				debug_mesg("E", __CLASS__, __FUNCTION__, __LINE__, $this->TAG . "[getShopListByRecommend()] : " . $e->getMessage());
				throw new Exception("SERVER_INTERNAL_ERROR");
			}
		}
		
		function getShopListByPopulate($_shopLocCd, $_page) {
			try {
				$this->mysql->where("T2.DEL_YN", "N")->where("T2.SHOP_LOC_CD", $_shopLocCd);
				$this->mysql->orderBy('T2.PDT_HIT', 'desc')->orderBy('T2.SHOP_NM', 'asc');
				$shop_list = $this->mysql->paginate("SHOP_M T2", $_page, "T2.SHOP_ID, T2.MANAGER_ID, T2.SHOP_NM, T2.SHOP_DESC, T2.SHOP_IMG1, T2.SHOP_IMG2, T2.SHOP_IMG3, T2.PDT_HIT, T2.PDT_AVG,
																		T2.SHOP_LNG, T2.SHOP_LAT, T2.SHOP_LOC_CD, 
																		(SELECT CODE_NM FROM CODE_M C WHERE C.TBL_NM = 'SHOP_M' AND C.COL_NM = 'SHOP_LOC_CD' AND C.SUB_CD = T2.SHOP_LOC_CD) AS SHOP_LOC_CD_TXT,
																		IFNULL((SELECT T1.PDT_PRIC FROM SHOP_PRODUCT_M T1 WHERE T1.SHOP_ID = T2.SHOP_ID ORDER BY T1.PDT_PRIC DESC LIMIT 1), 0) AS MIN_PDT_PRICE,
																		T2.SHOP_ADDR, T2.SHOP_POSTAL_CD, T2.SHOP_TEL, T2.SHOP_OP_TIME, T2.SHOP_ETC");
		
				return $shop_list;
			} catch (Exception $e) {
				debug_mesg("E", __CLASS__, __FUNCTION__, __LINE__, $this->TAG . "[getShopListByPopulate()] : " . $e->getMessage());
				throw new Exception("SERVER_INTERNAL_ERROR");
			}
		}
		
		function getShopDetail($_shopId) {
			try {
				$this->mysql->where("SHOP_ID", $_shopId)->where("DEL_YN", "N");
				$shop_detail = $this->mysql->getOne("SHOP_M", "SHOP_ID, MANAGER_ID, SHOP_NM, SHOP_DESC, SHOP_IMG1, SHOP_IMG2, SHOP_IMG3, PDT_HIT, PDT_AVG,
																SHOP_LNG, SHOP_LAT, SHOP_LOC_CD, (SELECT CODE_NM FROM CODE_M C WHERE C.TBL_NM = 'SHOP_M' AND C.COL_NM = 'SHOP_LOC_CD' AND C.SUB_CD = SHOP_LOC_CD) AS SHOP_LOC_CD_TXT,
																SHOP_ADDR, SHOP_POSTAL_CD, SHOP_TEL, SHOP_MOBILE, SHOP_OP_TIME, SHOP_OP_TIME2, SHOP_ETC");
				
				return $shop_detail;
			} catch (Exception $e) {
				debug_mesg("E", __CLASS__, __FUNCTION__, __LINE__, $this->TAG . "[getShopDetail()] : " . $e->getMessage());
				throw new Exception("SERVER_INTERNAL_ERROR");
			}
		}
		
		function getShopProductList($_shopId) {
			try {
				$this->mysql->where("SHOP_ID", $_shopId)->where("DEL_YN", "N");
				$this->mysql->orderBy('PDT_IDX', 'asc')->orderBy('PDT_NM', 'asc');
				$shop_pdt_list = $this->mysql->get("SHOP_PRODUCT_M", null, "PDT_ID, SHOP_ID, PDT_NM, PDT_DESC, PDT_TP_CD, (SELECT CODE_NM FROM CODE_M C WHERE C.TBL_NM = 'SHOP_PRODUCT_M' AND C.COL_NM = 'PDT_TP_CD' AND C.SUB_CD = PDT_TP_CD) AS PDT_TP_CD_TXT,
																		PDT_IDX, PDT_PRIC, ELAPSE_TM, PDT_AVG, PDT_HIT");
				
				if(!isset($shop_pdt_list)) {
					$shop_pdt_list = array();
				}
				
				return $shop_pdt_list;
			} catch (Exception $e) {
				debug_mesg("E", __CLASS__, __FUNCTION__, __LINE__, $this->TAG . "[getShopProductList()] : " . $e->getMessage());
				throw new Exception("SERVER_INTERNAL_ERROR");
			}
		}
		
		function getShopPortfolioList($_shopId, $_limit) {
			try {
				$this->mysql->where("SHOP_ID", $_shopId)->where("DEL_YN", "N");
				$this->mysql->orderBy('PORT_IDX', 'asc');
				$shop_portfolio_list = $this->mysql->get("SHOP_PORTFOLIO_M", $_limit, "PORT_ID, SHOP_ID, PORT_IMG1, PORT_NM, PORT_DESC, PORT_TP_CD, (SELECT CODE_NM FROM CODE_M C WHERE C.TBL_NM = 'SHOP_PORTFOLIO_M' AND C.COL_NM = 'PORT_TP_CD' AND C.SUB_CD = PORT_TP_CD) AS PORT_TP_CD_TXT, PORT_IDX");
		
				if(!isset($shop_portfolio_list)) {
					$shop_portfolio_list = array();
				}
				
				return $shop_portfolio_list;
			} catch (Exception $e) {
				debug_mesg("E", __CLASS__, __FUNCTION__, __LINE__, $this->TAG . "[getShopPortfolioList()] : " . $e->getMessage());
				throw new Exception("SERVER_INTERNAL_ERROR");
			}
		}
		
		function getShopReviewList($_shopId) {
			try {
				$list = $this->mysql->rawQuery("SELECT
													T1.CMT_ID, T1.SHOP_ID, T1.USER_ID, T1.CMT_TITLE, T1.CMT_DESC, T1.CMT_GRADE, T1.REG_DT,
													T2.USER_NM,
													T3.RPL_ID, T3.RPL_TITLE, T3.RPL_DESC, T3.RPL_GRADE, T3.REG_DT AS RPL_REG_DT
												FROM 
													SHOP_COMMENT_S T1 
													LEFT OUTER JOIN SHOP_COMMENT_REPLY_S T3 ON T1.CMT_ID = T3.CMT_ID AND T3.SHOP_ID = T1.SHOP_ID AND T3.DEL_YN = 'N'
													INNER JOIN USER_M T2 ON T1.USER_ID = T2.USER_ID AND T2.DEL_YN = 'N'
												WHERE 
													T1.SHOP_ID = " . $_shopId . " AND T1.DEL_YN = 'N'
												ORDER BY T1.REG_DT DESC");
		
				if(!isset($list)) {
					$list = array();
				}
				
				return $list;
			} catch (Exception $e) {
				debug_mesg("E", __CLASS__, __FUNCTION__, __LINE__, $this->TAG . "[getShopReviewList()] : " . $e->getMessage());
				throw new Exception("SERVER_INTERNAL_ERROR");
			}
		}
		
		function getShopReviewReplyList($_shopId) {
			try {
				$this->mysql->where("SHOP_ID", $_shopId)->where("DEL_YN", "N");
				$shop_review_reply_list = $this->mysql->get("SHOP_COMMENT_REPLY_S", null, "RPL_ID, CMT_ID, SHOP_ID, USER_ID, RPL_TITLE, RPL_DESC, RPL_GRADE, REG_DT");
		
				if(!isset($shop_review_reply_list)) {
					$shop_review_reply_list = array();
				}
				
				$ret_shop_review_reply_list = array();
				foreach ($shop_review_reply_list as $row) {
					if(!isset($ret_shop_review_reply_list[$row["CMT_ID"]])) {
						$ret_shop_review_reply_list[$row["CMT_ID"]] = array();
					}
					
					$ret_shop_review_reply_list[$row["CMT_ID"]][] = $row;
				}
		
				return $ret_shop_review_reply_list;
			} catch (Exception $e) {
				debug_mesg("E", __CLASS__, __FUNCTION__, __LINE__, $this->TAG . "[getShopReviewReplyList()] : " . $e->getMessage());
				throw new Exception("SERVER_INTERNAL_ERROR");
			}
		}
		
		function getShopEventList($_shopId) {
			try {
				$shop_event_list = $this->mysql->rawQuery("SELECT T1.SHOP_ID, T1.EVT_ID, T2.EVT_TP_CD, T2.EVT_NM, T2.EVT_DESC, T2.EVT_IDX, T2.EVT_DISC_PRIC, T2.EVT_BAN_IMG, 
																T2.EVT_START_DT, T2.EVT_END_DT, T2.REG_DT
															FROM SHOP_EVENT_R T1 INNER JOIN EVENT_M T2 ON T1.EVT_ID = T2.EVT_ID
															WHERE T1.SHOP_ID = " . $_shopId . " AND DEL_YN = 'N'
															ORDER BY EVT_ID ASC;");
		
				if(!isset($shop_event_list)) {
					$shop_event_list = array();
				}
		
				return $shop_event_list;
			} catch (Exception $e) {
				debug_mesg("E", __CLASS__, __FUNCTION__, __LINE__, $this->TAG . "[getShopEventList()] : " . $e->getMessage());
				throw new Exception("SERVER_INTERNAL_ERROR");
			}
		}
		
		function getShopCouponListAll($_shopId) {
			try {
				$this->mysql->where("SHOP_ID", $_shopId)->where("DEL_YN", "N");
				$this->mysql->orderBy('SHOP_COUP_ID', 'asc');
				$shop_coupon_list = $this->mysql->get("SHOP_COUPON_R", null, "SHOP_COUP_ID, SHOP_ID, COUP_TP_CD, COUP_NM, COUP_DISP_PRIC, START_DT, END_DT, USE_YN");
		
				if(!isset($shop_coupon_list)) {
					$shop_coupon_list = array();
				}
		
				return $shop_coupon_list;
			} catch (Exception $e) {
				debug_mesg("E", __CLASS__, __FUNCTION__, __LINE__, $this->TAG . "[getShopCouponList()] : " . $e->getMessage());
				throw new Exception("SERVER_INTERNAL_ERROR");
			}
		}
		
		function getShopCouponList($_shopId) {
			try {
				$this->mysql->where("SHOP_ID", $_shopId)->where("DEL_YN", "N")->where("USE_YN", "Y")->where("NOW()+0 < END_DT");
				$this->mysql->orderBy('SHOP_COUP_ID', 'asc');
				$shop_coupon_list = $this->mysql->get("SHOP_COUPON_R", null, "SHOP_COUP_ID, SHOP_ID, COUP_TP_CD, COUP_NM, COUP_DISP_PRIC, START_DT, END_DT, USE_YN");
		
				if(!isset($shop_coupon_list)) {
					$shop_coupon_list = array();
				}
		
				return $shop_coupon_list;
			} catch (Exception $e) {
				debug_mesg("E", __CLASS__, __FUNCTION__, __LINE__, $this->TAG . "[getShopCouponList()] : " . $e->getMessage());
				throw new Exception("SERVER_INTERNAL_ERROR");
			}
		}
		
		function getShopCouponDetail($_shopId, $_couponId) {
			try {
				$this->mysql->where("SHOP_ID", $_shopId)->where("SHOP_COUP_ID", $_couponId)->where("DEL_YN", "N");
				$shop_coupon_detail = $this->mysql->getOne("SHOP_COUPON_R", "SHOP_COUP_ID, SHOP_ID, COUP_NM, COUP_TP_CD, COUP_DISP_PRIC, START_DT, END_DT, USE_YN");
		
				return $shop_coupon_detail;
			} catch (Exception $e) {
				debug_mesg("E", __CLASS__, __FUNCTION__, __LINE__, $this->TAG . "[getShopCouponDetail()] : " . $e->getMessage());
				throw new Exception("SERVER_INTERNAL_ERROR");
			}
		}
		
		function checkDuplicateFavoriteShop($_memberId, $_shopId) {
			try {
				$this->mysql->where("USER_ID", $_memberId)->where("SHOP_ID", $_shopId);
				$favoriteShopCount = $this->mysql->getValue("USER_FAVORITESHOP_R", "COUNT(1)");
			
				return $favoriteShopCount > 0;
			} catch (Exception $e) {
				debug_mesg("E", __CLASS__, __FUNCTION__, __LINE__, $this->TAG . "[checkDuplicateFavoriteShop()] : " . $e->getMessage());
				throw new Exception("SERVER_INTERNAL_ERROR");
			}
		}
		
		function addFavoriteShop($_memberId, $_shopId) {
			try {
				if($this->checkDuplicateFavoriteShop($_memberId, $_shopId)) {
					throw new Exception("DUPLICATE_FAVORITE_SHOP");
				}
				
				$newData = array(
					"USER_ID" => $_memberId,
					"SHOP_ID" => $_shopId
				);
				
				$ret = $this->mysql->insert("USER_FAVORITESHOP_R", $newData);
				
				return $ret;
			} catch (Exception $e) {
				debug_mesg("E", __CLASS__, __FUNCTION__, __LINE__, $this->TAG . "[addFavoriteShop()] : " . $e->getMessage());
				
				if(getErrorCode($e->getMessage()) != null) {
					throw new Exception($e->getMessage());
				}
				
				throw new Exception("SERVER_INTERNAL_ERROR");
			}
		}
		
		function getPortfolioDetail($_portId) {
			try {
				$this->mysql->where("PORT_ID", $_portId)->where("DEL_YN", "N");
				$portfolio_detail = $this->mysql->getOne("SHOP_PORTFOLIO_M P", "PORT_ID, SHOP_ID, PORT_IMG1, PORT_NM, PORT_DESC, PORT_TP_CD, (SELECT CODE_NM FROM CODE_M C WHERE C.TBL_NM = 'SHOP_PORTFOLIO_M' AND C.COL_NM = 'PORT_TP_CD' AND C.SUB_CD = PORT_TP_CD) AS PORT_TP_CD_TXT,
																		PORT_IDX");
		
				if(!isset($portfolio_detail)) {
					throw new Exception("FAIL_TO_GET_PORTFOLIO_DETAIL");
				}
				
				return $portfolio_detail;
			} catch (Exception $e) {
				debug_mesg("E", __CLASS__, __FUNCTION__, __LINE__, $this->TAG . "[getPortfolioDetail()] : " . $e->getMessage());
				
				if(getErrorCode($e->getMessage()) != null) {
					throw new Exception($e->getMessage());
				}
				
				throw new Exception("SERVER_INTERNAL_ERROR");
			}
		}
		
		function getPortfolioHashTagList($_portId) {
			try {
				$this->mysql->where("PORT_ID", $_portId);
				$portfolio_hash_list = $this->mysql->get("PORTFOLIO_HASHTAG_R", null, "PORT_ID, TAG_NM");
		
				if(!isset($portfolio_hash_list)) {
					$portfolio_hash_list = array();
				}
				
				return $portfolio_hash_list;
			} catch (Exception $e) {
				debug_mesg("E", __CLASS__, __FUNCTION__, __LINE__, $this->TAG . "[getPortfolioHashTagList()] : " . $e->getMessage());
				throw new Exception("SERVER_INTERNAL_ERROR");
			}
		}
		
		function getPortfolioListByHashTag($_tagNm) {
			try {
				$this->mysql->where("A.DEL_YN", "N")->where("B.DEL_YN", "N")
							->where("A.PORT_ID IN (SELECT PORT_ID FROM PORTFOLIO_HASHTAG_R WHERE TAG_NM = '" . $_tagNm . "')");
				$this->mysql->orderBy('B.PDT_AVG', 'asc')->orderBy('A.PORT_NM', 'asc');
				$this->mysql->join('SHOP_M B', 'A.SHOP_ID = B.SHOP_ID', 'INNER');
				$portfolio_hash_list = $this->mysql->get("SHOP_PORTFOLIO_M A", null, "A.PORT_ID, A.SHOP_ID, B.SHOP_NM, A.PORT_IMG1, A.PORT_NM, 
																						A.PORT_DESC, A.PORT_TP_CD, (SELECT CODE_NM FROM CODE_M C WHERE C.TBL_NM = 'SHOP_PORTFOLIO_M' AND C.COL_NM = 'PORT_TP_CD' AND C.SUB_CD = PORT_TP_CD) AS PORT_TP_CD_TXT,
																						A.PORT_IDX");
		
				if(!isset($portfolio_hash_list)) {
					$portfolio_hash_list = array();
				}
		
				return $portfolio_hash_list;
			} catch (Exception $e) {
				debug_mesg("E", __CLASS__, __FUNCTION__, __LINE__, $this->TAG . "[getPortfolioListByHashTag()] : " . $e->getMessage());
				throw new Exception("SERVER_INTERNAL_ERROR");
			}
		}
		
		function addReview($_memberId, $_shopId, $_title, $_content, $_starCount) {
			try {
				$newData = array(
					"USER_ID" => $_memberId,
					"SHOP_ID" => $_shopId,
					"CMT_TITLE" => $_title,
					"CMT_DESC" => $_content,
					"CMT_GRADE" => $_starCount,
					"REG_DT" => date("YmdHis"),
					"DEL_YN" => "N"
				);
		
				$ret = $this->mysql->insert("SHOP_COMMENT_S", $newData);
		
				return $ret;
			} catch (Exception $e) {
				debug_mesg("E", __CLASS__, __FUNCTION__, __LINE__, $this->TAG . "[addReview()] : " . $e->getMessage());
		
				if(getErrorCode($e->getMessage()) != null) {
					throw new Exception($e->getMessage());
				}
		
				throw new Exception("SERVER_INTERNAL_ERROR");
			}
		}
		
		function getEventDetail($_evtId) {
			try {
				$this->mysql->where("EVT_ID", $_evtId)->where("DEL_YN", "N");
				$event_detail = $this->mysql->getOne("EVENT_M", "EVT_ID, EVT_TP_CD, EVT_NM, EVT_DESC, EVT_IDX, EVT_DISC_PRIC, EVT_BAN_IMG, 
															EVT_START_DT,EVT_END_DT");
				
				if(!isset($event_detail)) {
					throw new Exception("FAIL_TO_GET_EVENT_DETAIL");
				}
				
				return $event_detail;
			} catch (Exception $e) {
				debug_mesg("E", __CLASS__, __FUNCTION__, __LINE__, $this->TAG . "[getEventDetail()] : " . $e->getMessage());
		
				if(getErrorCode($e->getMessage()) != null) {
					throw new Exception($e->getMessage());
				}
		
				throw new Exception("SERVER_INTERNAL_ERROR");
			}
		}
		
		function getShopListByEventId($_evtId) {
			try {
				$shop_list = $this->mysql->rawQuery("SELECT T3.SHOP_ID, T3.SHOP_NM, T3.SHOP_IMG1, T3.SHOP_IMG2, T3.SHOP_IMG3, T1.EVT_ID, T1.EVT_NM, T1.EVT_DESC, (SELECT COUNT(1) FROM SHOP_EVENT_R WHERE SHOP_ID = T3.SHOP_ID) AS EVENT_COUNT
														FROM EVENT_M T1 INNER JOIN SHOP_EVENT_R T2 ON T1.EVT_ID = T2.EVT_ID
															INNER JOIN SHOP_M T3 ON T2.SHOP_ID = T3.SHOP_ID AND T3.DEL_YN = 'N'
														WHERE T1.EVT_ID = " . $_evtId . " AND T1.DEL_YN = 'N'");
				
				if(!isset($shop_list)) {
					$shop_list = array();
				}
				
				return $shop_list;
			} catch (Exception $e) {
				debug_mesg("E", __CLASS__, __FUNCTION__, __LINE__, $this->TAG . "[getShopListByEventId()] : " . $e->getMessage());
		
				if(getErrorCode($e->getMessage()) != null) {
					throw new Exception($e->getMessage());
				}
		
				throw new Exception("SERVER_INTERNAL_ERROR");
			}
		}
		
		function getFavoriteShopList($_memberId) {
			try {
				$this->mysql->where("B.DEL_YN", "N")->where("A.USER_ID", $_memberId);
				$this->mysql->orderBy('B.SHOP_NM', 'asc');
				$this->mysql->join('SHOP_M B', 'A.SHOP_ID = B.SHOP_ID', 'INNER');
				$shop_list = $this->mysql->get("USER_FAVORITESHOP_R A", null, "B.SHOP_ID, B.MANAGER_ID, B.SHOP_NM, B.SHOP_DESC, B.SHOP_IMG1, B.SHOP_IMG2, B.SHOP_IMG3, B.PDT_HIT, B.PDT_AVG,
																				B.SHOP_LNG, B.SHOP_LAT, B.SHOP_LOC_CD, (SELECT CODE_NM FROM CODE_M C WHERE C.TBL_NM = 'SHOP_M' AND C.COL_NM = 'SHOP_LOC_CD' AND C.SUB_CD = B.SHOP_LOC_CD) AS SHOP_LOC_CD_TXT,
																				B.SHOP_ADDR, B.SHOP_POSTAL_CD, B.SHOP_TEL, B.SHOP_OP_TIME, B.SHOP_ETC");
				
				if(!isset($shop_list)) {
					$shop_list = array();
				}
				
				return $shop_list;
			} catch (Exception $e) {
				debug_mesg("E", __CLASS__, __FUNCTION__, __LINE__, $this->TAG . "[getFavoriteShopList()] : " . $e->getMessage());
		
				if(getErrorCode($e->getMessage()) != null) {
					throw new Exception($e->getMessage());
				}
		
				throw new Exception("SERVER_INTERNAL_ERROR");
			}
		}
		
		function getMyCouponList($_memberId) {
			try {
				$nowTime = date("YmdHis");
				
				$sql = "SELECT
							T2.SHOP_COUP_ID, T2.SHOP_ID, T2.COUP_NM, T2.COUP_TP_CD, T2.COUP_DISP_PRIC, T2.START_DT, T2.END_DT
						FROM 
							USER_COUP_R T1 INNER JOIN SHOP_COUPON_R T2 ON T1.SHOP_COUP_ID = T2.SHOP_COUP_ID
						WHERE 
							T1.SHOP_COUP_ID NOT IN (SELECT AA.SHOP_COUP_ID FROM ORDER_COUP_R AA, ORDER_M BB WHERE AA.ORDER_ID = BB.ORDER_ID AND BB.USER_ID = " . $_memberId . ")
							AND '" . $nowTime . "' BETWEEN T2.START_DT AND T2.END_DT
							AND T1.USER_ID = " . $_memberId . " AND T1.DEL_YN = 'N' AND T1.USE_DT IS NULL
							AND T2.DEL_YN = 'N' AND T2.USE_YN = 'Y'
						ORDER BY T2.COUP_NM;";
				
				$coupon_list = $this->mysql->rawQuery($sql);
				
// 				$this->mysql->where("T1.USER_ID", $_memberId)->where("T1.DEL_YN", "N")->where("T2.DEL_YN", "N")->where("T2.USE_YN", "Y");//->where("T1.USE_DT", null, "IS");
// 				$this->mysql->where("T1.SHOP_COUP_ID NOT IN (SELECT AA.SHOP_COUP_ID FROM ORDER_COUP_R AA, ORDER_M BB WHERE AA.ORDER_ID = BB.ORDER_ID AND BB.USER_ID = " . $_memberId . ")");
// 				$this->mysql->where("'" . $nowTime . "' BETWEEN T2.START_DT AND T2.END_DT");
// 				$this->mysql->orderBy('T2.COUP_NM', 'asc');
// 				$this->mysql->join("SHOP_COUPON_R T2", "T1.SHOP_COUP_ID = T2.SHOP_COUP_ID", "INNER");
// 				$coupon_list = $this->mysql->get("USER_COUPON_R T1", null, "T2.SHOP_COUP_ID, T2.SHOP_ID, T2.COUP_NM, T2.COUP_TP_CD, T2.COUP_DISP_PRIC, T2.START_DT, T2.END_DT");
		
				if(!isset($coupon_list)) {
					$coupon_list = array();
				}
		
				return $coupon_list;
			} catch (Exception $e) {
				debug_mesg("E", __CLASS__, __FUNCTION__, __LINE__, $this->TAG . "[getMyCouponList()] : " . $e->getMessage());
		
				if(getErrorCode($e->getMessage()) != null) {
					throw new Exception($e->getMessage());
				}
		
				throw new Exception("SERVER_INTERNAL_ERROR");
			}
		}
		
		function getMyCouponListByShopId($_shopId, $_memberId) {
			try {
				$this->mysql->where("SHOP_ID", $_shopId)->where("DEL_YN", "N")->where("USE_YN", "Y");
// 				$this->mysql->where("SHOP_COUP_ID NOT IN (SELECT AA.SHOP_COUP_ID FROM ORDER_COUP_R AA, ORDER_M BB WHERE AA.ORDER_ID = BB.ORDER_ID AND BB.USER_ID = " . $_memberId . ")");
				$this->mysql->where("SHOP_COUP_ID NOT IN (SELECT AA.SHOP_COUP_ID FROM USER_COUP_R AA WHERE AA.USER_ID = " . $_memberId . ")");
				$this->mysql->orderBy('COUP_NM', 'asc');
				$coupon_list = $this->mysql->get("SHOP_COUPON_R", null, "COUP_TP_CD, COUP_DISP_PRIC, SHOP_COUP_ID, SHOP_ID, COUP_NM");
		
				if(!isset($coupon_list)) {
					$coupon_list = array();
				}
		
				return $coupon_list;
			} catch (Exception $e) {
				debug_mesg("E", __CLASS__, __FUNCTION__, __LINE__, $this->TAG . "[getMyCouponList()] : " . $e->getMessage());
		
				if(getErrorCode($e->getMessage()) != null) {
					throw new Exception($e->getMessage());
				}
		
				throw new Exception("SERVER_INTERNAL_ERROR");
			}
		}
		
		function getCouponList($_shopId, $_memberId) {
			try {
				$this->mysql->where("A.DEL_YN", "N")->where("A.SHOP_ID", $_shopId)->where("A.USE_YN", "Y");
				$this->mysql->where("A.SHOP_COUP_ID NOT IN (SELECT AA.SHOP_COUP_ID FROM ORDER_COUP_R AA, ORDER_M BB WHERE AA.ORDER_ID = BB.ORDER_ID AND BB.USER_ID = " . $_memberId . ")");
				$this->mysql->orderBy('A.COUP_NM', 'asc');
				$coupon_list = $this->mysql->get("SHOP_COUPON_R A", null, "A.COUP_TP_CD, A.COUP_DISP_PRIC, A.SHOP_COUP_ID, A.SHOP_ID, A.COUP_NM");
		
				if(!isset($coupon_list)) {
					$coupon_list = array();
				}
		
				return $coupon_list;
			} catch (Exception $e) {
				debug_mesg("E", __CLASS__, __FUNCTION__, __LINE__, $this->TAG . "[getCouponList()] : " . $e->getMessage());
		
				if(getErrorCode($e->getMessage()) != null) {
					throw new Exception($e->getMessage());
				}
		
				throw new Exception("SERVER_INTERNAL_ERROR");
			}
		}
		
		function getCouponMasterList($_shopId) {
			try {
				$this->mysql->where("A.DEL_YN", "N")->where("A.SHOP_ID", $_shopId)->where("A.USE_YN", "Y");
				$this->mysql->orderBy('A.COUP_NM', 'asc');
				$coupon_list = $this->mysql->get("SHOP_COUPON_R A", null, "A.COUP_TP_CD, A.COUP_DISP_PRIC, A.SHOP_COUP_ID, A.SHOP_ID, A.COUP_NM");
		
				if(!isset($coupon_list)) {
					$coupon_list = array();
				}
		
				return $coupon_list;
			} catch (Exception $e) {
				debug_mesg("E", __CLASS__, __FUNCTION__, __LINE__, $this->TAG . "[getCouponMasterList()] : " . $e->getMessage());
		
				if(getErrorCode($e->getMessage()) != null) {
					throw new Exception($e->getMessage());
				}
		
				throw new Exception("SERVER_INTERNAL_ERROR");
			}
		}
		
		function getAvailableCouponCount($_memberId) {
			try {
				$this->mysql->where("DEL_YN", "N")->where("USE_YN", "Y")->where("SHOP_COUP_ID NOT IN (SELECT AA.SHOP_COUP_ID FROM ORDER_COUP_R AA, ORDER_M BB WHERE AA.ORDER_ID = BB.ORDER_ID AND BB.USER_ID = " . $_memberId . ")");
				$coupon_count = $this->mysql->getValue("SHOP_COUPON_R", "COUNT(1)");
		
				return $coupon_count;
			} catch (Exception $e) {
				debug_mesg("E", __CLASS__, __FUNCTION__, __LINE__, $this->TAG . "[getAvailableCouponCount()] : " . $e->getMessage());
		
				if(getErrorCode($e->getMessage()) != null) {
					throw new Exception($e->getMessage());
				}
		
				throw new Exception("SERVER_INTERNAL_ERROR");
			}
		}
		
		function getShopBookingList($_shopId) {
			try {
				$startDate = date("Ymd") . "000000";
				$endDate = date("Ymd", strtotime("+14 days")) . "000000";
				
				$this->mysql->where("T1.SHOP_ID", $_shopId)->where("ORDER_TP_CD", "000")->where("T1.ORDER_DT BETWEEN '" . $startDate . "' AND '" . $endDate . "'");
				$this->mysql->orderBy('T1.ORDER_DT', 'asc');
				$this->mysql->join('SHOP_PRODUCT_M T2', 'T1.PDT_ID = T2.PDT_ID', 'INNER');
				$this->mysql->join('USER_M T3', 'T1.MANAGER_ID = T3.USER_ID', 'INNER');
				$shop_booking_list = $this->mysql->get("ORDER_M T1", null, "T1.ORDER_ID, T1.ORDER_DT, T2.PDT_NM, T2.ELAPSE_TM, T3.USER_NM");
				
				if(!isset($shop_booking_list)) {
					$shop_booking_list = array();
				}
		
				return $shop_booking_list;
			} catch (Exception $e) {
				debug_mesg("E", __CLASS__, __FUNCTION__, __LINE__, $this->TAG . "[getShopBookingList()] : " . $e->getMessage());
		
				if(getErrorCode($e->getMessage()) != null) {
					throw new Exception($e->getMessage());
				}
		
				throw new Exception("SERVER_INTERNAL_ERROR");
			}
		}
		
		function getShopManagerList($_shopId) {
			try {
				$this->mysql->where("T1.SHOP_ID", $_shopId);
				$this->mysql->orderBy('T2.USER_NM', 'asc');
				$this->mysql->join('USER_M T2', 'T1.MANAGER_ID = T2.USER_ID', 'INNER');
				$shop_manager_list = $this->mysql->get("USER_MANAGER_R T1", null, "T1.MANAGER_ID, T2.USER_NM");
				
				if(!isset($shop_manager_list) ) {
					$shop_manager_list = array();
				}
		
				return $shop_manager_list;
			} catch (Exception $e) {
				debug_mesg("E", __CLASS__, __FUNCTION__, __LINE__, $this->TAG . "[getShopManagerList()] : " . $e->getMessage());
		
				if(getErrorCode($e->getMessage()) != null) {
					throw new Exception($e->getMessage());
				}
		
				throw new Exception("SERVER_INTERNAL_ERROR");
			}
		}
		
		function getShopProductDetail($_shopId, $_pdtId) {
			try {
				$this->mysql->where("SHOP_ID", $_shopId)->where("PDT_ID", $_pdtId)->where("DEL_YN", "N");
				$this->mysql->orderBy('PDT_IDX', 'asc')->orderBy('PDT_NM', 'asc');
				$shop_pdt_detail = $this->mysql->getOne("SHOP_PRODUCT_M", "PDT_ID, SHOP_ID, PDT_NM, PDT_DESC, PDT_TP_CD, (SELECT CODE_NM FROM CODE_M C WHERE C.TBL_NM = 'SHOP_PRODUCT_M' AND C.COL_NM = 'PDT_TP_CD' AND C.SUB_CD = PDT_TP_CD) AS PDT_TP_CD_TXT,
																		PDT_IDX, PDT_PRIC, ELAPSE_TM, PDT_AVG, PDT_HIT");
		
				return $shop_pdt_detail;
			} catch (Exception $e) {
				debug_mesg("E", __CLASS__, __FUNCTION__, __LINE__, $this->TAG . "[getShopProductDetail()] : " . $e->getMessage());
				throw new Exception("SERVER_INTERNAL_ERROR");
			}
		}
		
		function getShopManagerGcmIdList($_shopId) {
			try {
				$this->mysql->where("T1.SHOP_ID", $_shopId);
				$this->mysql->orderBy('T2.USER_NM', 'asc');
				$this->mysql->join('USER_M T2', 'T1.MANAGER_ID = T2.USER_ID', 'INNER');
				$shop_manager_list = $this->mysql->get("USER_MANAGER_R T1", null, "T1.MANAGER_ID, T2.USER_NM, T2.USER_REAL_NM, T2.GCM_ID");
		
				$retShopManagerList = null;
				if(isset($shop_manager_list) ) {
					$retShopManagerList = array();
					foreach ($shop_manager_list as $row) {
						$retShopManagerList[] = $row["GCM_ID"];
					}
				}
		
				return $retShopManagerList;
			} catch (Exception $e) {
				debug_mesg("E", __CLASS__, __FUNCTION__, __LINE__, $this->TAG . "[getShopManagerList()] : " . $e->getMessage());
		
				if(getErrorCode($e->getMessage()) != null) {
					throw new Exception($e->getMessage());
				}
		
				throw new Exception("SERVER_INTERNAL_ERROR");
			}
		}
		
		function saveBooking($_userId, $_shopId, $_pdtId, $_managerId, $_orderDt, $_orderGubunCd, $_userNm, $_userPhoneNumber, $_couponId, $_fromBooking) {
			try {
				$orderedPrice = 0;
				
				$shopDetail = $this->getShopDetail($_shopId);
				if(!isset($shopDetail)) {
					throw new Exception("INVALID_SHOP_ID");
				}
				
				$pdtDetail = $this->getShopProductDetail($_shopId, $_pdtId);
				if(!isset($pdtDetail)) {
					throw new Exception("INVALID_PDT_ID");
				}
				
				$orderedPrice = $pdtDetail["PDT_PRIC"];
				
				$couponDetail = null;
				if(isset($_couponId) && !empty($_couponId)) {
					$couponDetail = $this->getShopCouponDetail($_shopId, $_couponId);
					if(!isset($couponDetail)) {
						throw new Exception("INVALID_COUPON_ID");
					}
					
					if($couponDetail["COUP_TP_CD"] == "000") {
						$orderedPrice = $orderedPrice - $couponDetail["COUP_DISP_PRIC"];
					} else if($couponDetail["COUP_TP_CD"] == "001") {
						$orderedPrice = 0;
					} else if($couponDetail["COUP_TP_CD"] == "002") {
						$orderedPrice = $orderedPrice - ($orderedPrice * ($couponDetail["COUP_DISP_PRIC"] / 100));
					}
				}
				
				if($this->checkShopHolidayHours($_shopId, $_orderDt, $pdtDetail["ELAPSE_TM"])) {
					throw new Exception("INVALID_BOOKING_TIME");
				}
				
				$this->mysql->startTransaction();
				
				if($_orderGubunCd == "001" && $_userId == "-1") {
					$newData = array(
						'USER_NM' => $_userNm,
						'USER_REAL_NM' => $_userNm,
						"USER_TP_CD" => "004",
						"USER_DESC" => "비회원",
						"USER_PHONE1" => $_userPhoneNumber,
						"PHONE_CERT_YN" => "N",
						"DEL_YN" => "N",
						"REG_DT" => date("YmdHis")
					);
					
					$ret = $this->mysql->insert("user_m", $newData);
					if(!$ret) {
						debug_mesg("E", __CLASS__, __FUNCTION__, __LINE__, $this->TAG . "[saveBooking()] : " . $this->mysql->getLastError());
						throw new Exception("FAIL_TO_JOIN");
					}
					
					$_userId = $this->mysql->getInsertId();
					
// 					$newData = array(
// 						"SHOP_ID" => $_shopId,
// 						"USER_ID" => $_userId,
// 						"SHOP_GR_CD" => "000",
// 						"SHOP_GR_MEMO" => "",
// 						"INTERCEPTION_YN" => "N",
// 						"REG_DT" => date("YmdHis")
// 					);
					
// 					$ret = $this->mysql->insert("shop_user_grade_r", $newData);
// 					if(!$ret) {
// 						debug_mesg("E", __CLASS__, __FUNCTION__, __LINE__, $this->TAG . "[saveBooking()] : " . $this->mysql->getLastError());
// 						throw new Exception("FAIL_TO_JOIN");
// 					}
				}
				
				$this->getMemberServiceClass();
					
				$memberInfo = $this->MemberService->getMemberInfo($_userId);
				if(!isset($memberInfo) || empty($memberInfo)) {
					throw new Exception("FAIL_TO_SEARCH_ID");
				}
					
				$_userNm = $memberInfo["USER_REAL_NM"];
				$_userPhoneNumber = $memberInfo["USER_PHONE1"];
				
				$newData = array(
					"SHOP_ID" => $_shopId,
					"PDT_ID" => $_pdtId,
					"USER_ID" => $_userId,
					"MANAGER_ID" => $_managerId,
					"USER_NM" => $_userNm,
					"USER_PHONE1" => $_userPhoneNumber,
					"ORDER_DT" => $_orderDt,
					"ORDER_TP_CD" => "000",
					"PAY_TP_CD" => "000",
					"ORDER_GUBUN_CD" => $_orderGubunCd,
					"ORDERED_PRICE" => $orderedPrice,
					"REG_DT" => date("YmdHis")
				);
				
				$ret = $this->mysql->insert("order_m", $newData);
				if(!$ret) {
					debug_mesg("E", __CLASS__, __FUNCTION__, __LINE__, $this->TAG . "[saveBooking()] : " . $this->mysql->getLastError());
					throw new Exception("FAIL_TO_ORDER");
				}
				
				$orderId = $this->mysql->getInsertId();
				
				if(isset($couponDetail) && !empty($couponDetail)) {
					$newCoupData = array(
						"ORDER_ID" => $orderId,
						"SHOP_COUP_ID" => $_couponId,
						"USER_ID" => $_userId
					);
					
					$ret = $this->mysql->insert("order_coup_r", $newCoupData);
					if(!$ret) {
						debug_mesg("E", __CLASS__, __FUNCTION__, __LINE__, $this->TAG . "[saveBooking()] : " . $this->mysql->getLastError());
						throw new Exception("FAIL_TO_ORDER");
					}
					
					$updateCoupData = array(
						"USER_DT" => date("YmdHis")
					);
					
					$this->mysql->where("USER_ID", $_userId)->where("SHOP_COUP_ID", $_couponId);
					$ret = $this->mysql->update("USER_COUP_R", $updateCoupData);
					if(!$ret) {
						debug_mesg("E", __CLASS__, __FUNCTION__, __LINE__, $this->TAG . "[saveBooking()] : " . $this->mysql->getLastError());
						throw new Exception("FAIL_TO_ORDER");
					}
				}
				
				$this->mysql->commit();
				
				if($_fromBooking == "customer") {
					$memberGcmId = $this->MemberService->getMemberGcmId($_managerId);
					
					$notification = array(
						"title" => "상품 예약",
						"body" => $pdtDetail["PDT_NM"] . "상품이 예약되었습니다."
					);
						
					$data = array(
						"messageType" => "shopBooking",
						"customerName" => $_userNm,
						"bookingDate" => $_orderDt,
						"productName" => $pdtDetail["PDT_NM"],
						"elapsedTime" => $pdtDetail["ELAPSE_TM"]
					);
					
					$dataStr = "";
					$dataStr .= "messageType:" . $data["messageType"] . ",";
					$dataStr .= "customerName:" . $data["customerName"] . ",";
					$dataStr .= "bookingDate:" . $data["bookingDate"] . ",";
					$dataStr .= "productName:" . $data["productName"] . ",";
					$dataStr .= "elapsedTime:" . $data["elapsedTime"] . ",";
					$dataStr .= "shopName:" . $shopDetail["SHOP_NM"];
					
					$sendDt = date("YmdHis", strtotime("+6 hours", strtotime($data["bookingDate"])));
					
					$this->getPushServiceClass();
					$this->PushService->savePushData($memberInfo["USER_ID"], $memberInfo["GCM_ID"], $notification["title"], $notification["body"], $dataStr, "customer", $sendDt, "N", "N");
					
					if(isset($memberGcmId)) {
						send_notification(GOOGLE_SERVER_KEY_MANAGER, $memberGcmId, $data, $notification);
					}
				}
				
				return $orderId;
			} catch (Exception $e) {
				$this->mysql->rollback();
				debug_mesg("E", __CLASS__, __FUNCTION__, __LINE__, $this->TAG . "[saveBooking()] : " . $e->getMessage());
			
				if(getErrorCode($e->getMessage()) != null) {
					throw new Exception($e->getMessage());
				}
			
				throw new Exception("SERVER_INTERNAL_ERROR");
			}
		}
		
		function editBooking($_userId, $_bookingId, $_shopId, $_pdtId, $_managerId, $_orderDt, $_couponId) {
			try {
				$orderedPrice = 0;
				
				$pdtDetail = $this->getShopProductDetail($_shopId, $_pdtId);
				if(!isset($pdtDetail)) {
					throw new Exception("INVALID_PDT_ID");
				}
				
				$orderedPrice = $pdtDetail["PDT_PRIC"];
				
				$couponDetail = null;
				if(isset($_couponId) && !empty($_couponId)) {
					$couponDetail = $this->getShopCouponDetail($_shopId, $_couponId);
					if(!isset($couponDetail)) {
						throw new Exception("INVALID_COUPON_ID");
					}
						
					if($couponDetail["COUP_TP_CD"] == "000") {
						$orderedPrice = $orderedPrice - $couponDetail["COUP_DISP_PRIC"];
					} else if($couponDetail["COUP_TP_CD"] == "001") {
						$orderedPrice = 0;
					} else if($couponDetail["COUP_TP_CD"] == "002") {
						$orderedPrice = $orderedPrice - ($orderedPrice * ($couponDetail["COUP_DISP_PRIC"] / 100));
					}
				}
				
				$this->mysql->startTransaction();
		
				$updateData = array(
					"PDT_ID" => $_pdtId,
					"MANAGER_ID" => $_managerId,
					"ORDER_DT" => $_orderDt . "00",
					"ORDERED_PRICE" => $orderedPrice
				);
				
				$this->mysql->where("ORDER_ID", $_bookingId);
		
				$ret = $this->mysql->update("order_m", $updateData);
				if(!$ret) {
					debug_mesg("E", __CLASS__, __FUNCTION__, __LINE__, $this->TAG . "[editBooking()] : " . $this->mysql->getLastError());
					throw new Exception("FAIL_TO_UPDATE_BOOKING");
				}
				
				$this->mysql->where("ORDER_ID", $_bookingId);
				$this->mysql->delete("order_coup_r");
				
				if(isset($couponDetail) && !empty($couponDetail)) {
					$newCoupData = array(
						"ORDER_ID" => $_bookingId,
						"SHOP_COUP_ID" => $_couponId,
						"USER_ID" => $_userId
					);
						
					$ret = $this->mysql->insert("order_coup_r", $newCoupData);
					if(!$ret) {
						debug_mesg("E", __CLASS__, __FUNCTION__, __LINE__, $this->TAG . "[editBooking()] : " . $this->mysql->getLastError());
						throw new Exception("FAIL_TO_ORDER");
					}
				}
				
				$this->mysql->commit();
			} catch (Exception $e) {
				$this->mysql->rollback();
				debug_mesg("E", __CLASS__, __FUNCTION__, __LINE__, $this->TAG . "[editBooking()] : " . $e->getMessage());
					
				if(getErrorCode($e->getMessage()) != null) {
					throw new Exception($e->getMessage());
				}
					
				throw new Exception("SERVER_INTERNAL_ERROR");
			}
		}
		
		function getBookingDetail($_orderId) {
			try {
				$this->mysql->where("T1.ORDER_ID", $_orderId);
				$this->mysql->join('SHOP_PRODUCT_M T2', 'T1.PDT_ID = T2.PDT_ID', 'INNER');
				$this->mysql->join('SHOP_M T3', 'T1.SHOP_ID = T3.SHOP_ID', 'INNER');
				$shop_booking_detail = $this->mysql->getOne("ORDER_M T1", "T1.ORDER_ID, T1.ORDER_DT, T1.ORDERED_PRICE, T1.ORDER_TP_CD, T2.PDT_NM, T2.ELAPSE_TM, T3.SHOP_NM, T3.SHOP_TEL, 
																		T3.SHOP_ADDR, T3.SHOP_ID, T2.PDT_ID");
		
				return $shop_booking_detail;
			} catch (Exception $e) {
				debug_mesg("E", __CLASS__, __FUNCTION__, __LINE__, $this->TAG . "[getBookingDetail()] : " . $e->getMessage());
				throw new Exception("SERVER_INTERNAL_ERROR");
			}
		}
		
		function getMyBookingList($_memberId) {
			try {
				$this->mysql->where("T1.USER_ID", $_memberId)->where("T1.ORDER_DT >= '" . date("YmdHis") . "'");
				$this->mysql->join('SHOP_PRODUCT_M T2', 'T1.PDT_ID = T2.PDT_ID', 'INNER');
				$this->mysql->join('SHOP_M T3', 'T1.SHOP_ID = T3.SHOP_ID', 'INNER');
				$this->mysql->orderBy('T1.ORDER_ID', 'desc');
				$my_booking_list = $this->mysql->get("ORDER_M T1", null, "T1.ORDER_ID, T1.ORDER_DT, T1.ORDER_TP_CD, T1.ORDER_GUBUN_CD, T1.PAY_TP_CD, T2.PDT_NM, T2.ELAPSE_TM, T3.SHOP_NM, T3.SHOP_TEL, 
																			T3.SHOP_ADDR, T3.SHOP_ID, T3.SHOP_IMG1, T3.SHOP_IMG2, T3.SHOP_IMG3, 
																			(SELECT CODE_NM FROM CODE_M C WHERE C.TBL_NM = 'ORDER_M' AND C.COL_NM = 'ORDER_TP_CD' AND C.SUB_CD = T1.ORDER_TP_CD) AS ORDER_TP_CD_TXT,
																			(SELECT CODE_NM FROM CODE_M C WHERE C.TBL_NM = 'SHOP_M' AND C.COL_NM = 'SHOP_LOC_CD' AND C.SUB_CD = SHOP_LOC_CD) AS SHOP_LOC_CD_TXT, 
																			T2.PDT_ID");
				
				if(!isset($my_booking_list)) {
					$my_booking_list = array();
				}
				
				return $my_booking_list;
			} catch (Exception $e) {
				debug_mesg("E", __CLASS__, __FUNCTION__, __LINE__, $this->TAG . "[getMyBookingList()] : " . $e->getMessage());
		
				if(getErrorCode($e->getMessage()) != null) {
					throw new Exception($e->getMessage());
				}
		
				throw new Exception("SERVER_INTERNAL_ERROR");
			}
		}
		
		function getMyHistoryList($_memberId, $_startDate, $_endDate) {
			try {
				$this->mysql->where("T1.USER_ID", $_memberId)->where("T1.ORDER_DT BETWEEN '" . $_startDate . "' AND '" . $_endDate . "'");
				$this->mysql->join('SHOP_PRODUCT_M T2', 'T1.PDT_ID = T2.PDT_ID', 'INNER');
				$this->mysql->join('SHOP_M T3', 'T1.SHOP_ID = T3.SHOP_ID', 'INNER');
				$this->mysql->join('PAYMENT_M T4', 'T1.ORDER_ID = T4.ORDER_ID', 'INNER');
				$this->mysql->orderBy('T1.ORDER_ID', 'desc');
				$my_history_list = $this->mysql->get("ORDER_M T1", null, "T1.ORDER_ID, T1.ORDER_DT, T2.PDT_NM, T2.ELAPSE_TM, T3.SHOP_NM, T3.SHOP_TEL,
																			T3.SHOP_ADDR, T3.SHOP_ID, T3.SHOP_IMG1, T3.SHOP_IMG2, T3.SHOP_IMG3, (SELECT CODE_NM FROM CODE_M C WHERE C.TBL_NM = 'SHOP_M' AND C.COL_NM = 'SHOP_LOC_CD' AND C.SUB_CD = SHOP_LOC_CD) AS SHOP_LOC_CD_TXT,
																			T2.PDT_ID");
		
				if(!isset($my_history_list)) {
					$my_history_list = array();
				}
		
				return $my_history_list;
			} catch (Exception $e) {
				debug_mesg("E", __CLASS__, __FUNCTION__, __LINE__, $this->TAG . "[getMyHistoryList()] : " . $e->getMessage());
		
				if(getErrorCode($e->getMessage()) != null) {
					throw new Exception($e->getMessage());
				}
		
				throw new Exception("SERVER_INTERNAL_ERROR");
			}
		}
		
		function getShopBookingCountListByMonth($_shopId, $_start, $_end) {
			try {
				$startDate = $_start . "000000";
				$endDate = $_end . "235959";
		
				$this->mysql->orderBy('ORDER_DT', 'asc');
				$this->mysql->groupBy('LEFT(T1.ORDER_DT, 8)');
				$this->mysql->join('SHOP_PRODUCT_M T2', 'T1.PDT_ID = T2.PDT_ID', 'INNER');
				$this->mysql->join('USER_M T3', 'T1.MANAGER_ID = T3.USER_ID', 'INNER');
				$this->mysql->where("T1.SHOP_ID", $_shopId)->where("T1.ORDER_DT BETWEEN '" . $startDate . "' AND '" . $endDate . "'")->where("T1.ORDER_TP_CD IN ('000', '002', '003')");
				$shop_booking_list = $this->mysql->get("ORDER_M T1", null, "left(T1.ORDER_DT, 8) AS ORDER_DT, COUNT(1) AS BOOKING_COUNT");
		
				if(!isset($shop_booking_list)) {
					$shop_booking_list = array();
				}
		
				return $shop_booking_list;
			} catch (Exception $e) {
				debug_mesg("E", __CLASS__, __FUNCTION__, __LINE__, $this->TAG . "[getShopBookingCountListByMonth()] : " . $e->getMessage());
		
				if(getErrorCode($e->getMessage()) != null) {
					throw new Exception($e->getMessage());
				}
		
				throw new Exception("SERVER_INTERNAL_ERROR");
			}
		}
		
		function getShopBookingListByDate($_shopId, $_date) {
			try {
				$startDate = $_date . "000000";
				$endDate = $_date . "235959";
		
				$shop_booking_list = $this->mysql->rawQuery("SELECT T1.ORDER_ID, T1.ORDER_DT, T1.USER_NM AS NON_MEMBER_NM, T1.USER_PHONE1 AS NON_MEMBER_PHONE1, T1.ORDER_TP_CD, T2.PDT_NM, T2.ELAPSE_TM, 
																	T3.USER_NM AS MANAGER_NM, T4.USER_NM, T4.USER_REAL_NM, T4.USER_PHONE1, 'BOOKING' AS LIST_TYPE, 'N' AS ALLDAY_YN, T1.ORDER_DT AS START_DT, T1.ORDER_DT AS END_DT
															FROM ORDER_M T1 INNER JOIN SHOP_PRODUCT_M T2 ON T1.PDT_ID = T2.PDT_ID
																INNER JOIN USER_M T3 ON T1.MANAGER_ID = T3.USER_ID
																LEFT OUTER JOIN USER_M T4 ON T1.USER_ID = T4.USER_ID
															WHERE T1.SHOP_ID = " . $_shopId . " AND T1.ORDER_DT BETWEEN '" . $startDate . "' AND '" . $endDate . "' AND T1.ORDER_TP_CD IN ('000', '002', '003')
															UNION ALL
															SELECT OFF_ID AS ORDER_ID, START_DT AS ORDER_DT, '휴무' AS NON_MEMBER_NM, '' AS NON_MEMBER_PHONE1, '' AS ORDER_TP_CD, '' AS PDT_NM, 
																	0 AS ELAPSE_TM, '' AS MANAGER_NM, '' AS USER_NM, '' AS USER_REAL_NM, '' AS USER_PHONE1, 
																	'DAYOFF' AS LIST_TYPE, ALLDAY_YN, START_DT, END_DT
															FROM SHOP_OFFDAY_R
															WHERE SHOP_ID = " . $_shopId . "
																AND START_DT BETWEEN '" . $startDate . "' AND '" . $endDate . "'
																AND END_DT BETWEEN '" . $startDate . "' AND '" . $endDate . "'
																AND DEL_YN = 'N'
															ORDER BY ORDER_DT ASC");
		
				if(!isset($shop_booking_list)) {
					$shop_booking_list = array();
				} else {
					foreach ($shop_booking_list as &$row) {
						if(!isset($row["USER_NM"]) || empty($row["USER_NM"])) {
							$row["USER_NM"] = $row["NON_MEMBER_NM"];
							$row["USER_PHONE1"] = $row["NON_MEMBER_PHONE1"];
							
							if($row["LIST_TYPE"] == "DAYOFF") {
								$startTime = strtotime(substr($row["START_DT"], 0, 4) . "-" . substr($row["START_DT"], 4, 2) . "-" . substr($row["START_DT"], 6, 2) . " " . substr($row["START_DT"], 8, 2) . ":" . substr($row["START_DT"], 10, 2) . ":" . substr($row["START_DT"], 12, 2));
								$endTime = strtotime(substr($row["END_DT"], 0, 4) . "-" . substr($row["END_DT"], 4, 2) . "-" . substr($row["END_DT"], 6, 2) . " " . substr($row["END_DT"], 8, 2) . ":" . substr($row["END_DT"], 10, 2) . ":" . substr($row["END_DT"], 12, 2));
								$row["ELAPSE_TM"] = ($endTime - $startTime) / 60;
							}
						}
					}
				}
		
				return $shop_booking_list;
			} catch (Exception $e) {
				debug_mesg("E", __CLASS__, __FUNCTION__, __LINE__, $this->TAG . "[getShopBookingListByDate()] : " . $e->getMessage());
		
				if(getErrorCode($e->getMessage()) != null) {
					throw new Exception($e->getMessage());
				}
		
				throw new Exception("SERVER_INTERNAL_ERROR");
			}
		}
		
		function getShopBookingDetail($_shopId, $_bookingId) {
			try {		
				$this->mysql->join('SHOP_PRODUCT_M T2', 'T1.PDT_ID = T2.PDT_ID', 'INNER');
				$this->mysql->join('USER_M T3', 'T1.MANAGER_ID = T3.USER_ID', 'INNER');
				$this->mysql->join('USER_M T4', 'T1.USER_ID = T4.USER_ID', 'LEFT OUTER');
				$this->mysql->join('PAYMENT_M T5', 'T1.ORDER_ID = T5.ORDER_ID', 'LEFT OUTER');
				$this->mysql->join('ORDER_COUP_R T6', 'T1.ORDER_ID = T6.ORDER_ID', 'LEFT OUTER');
				$this->mysql->where("T1.SHOP_ID", $_shopId)->where("T1.ORDER_ID", $_bookingId);
				$shop_booking_detail = $this->mysql->getOne("ORDER_M T1", "T1.ORDER_ID, T1.ORDER_DT, T1.MANAGER_ID, T1.PDT_ID, T1.USER_NM AS NON_MEMBER_NM, T1.USER_PHONE1 AS NON_MEMBER_PHONE1, 
																			T2.PDT_NM, T2.ELAPSE_TM, T2.PDT_PRIC, T3.USER_NM AS MANAGER_NM, T4.USER_ID, T4.USER_NM, T4.USER_ACCOUNT_ID, T4.USER_PHONE1, T5.PAY_ID,
																			T6.SHOP_COUP_ID");
				
				if(!isset($shop_booking_detail)) {
					throw new Exception("FAIL_TO_GET_BOOKING_DETAIL");
				} else {
					if(!isset($shop_booking_detail["USER_NM"]) || empty($shop_booking_detail["USER_NM"])) {
						$shop_booking_detail["USER_NM"] = $shop_booking_detail["NON_MEMBER_NM"];
						$shop_booking_detail["USER_PHONE1"] = $shop_booking_detail["NON_MEMBER_PHONE1"];
					}
				}
		
				return $shop_booking_detail;
			} catch (Exception $e) {
				debug_mesg("E", __CLASS__, __FUNCTION__, __LINE__, $this->TAG . "[getShopBookingDetail()] : " . $e->getMessage());
		
				if(getErrorCode($e->getMessage()) != null) {
					throw new Exception($e->getMessage());
				}
		
				throw new Exception("SERVER_INTERNAL_ERROR");
			}
		}
		
		function getPaymentDetail($_orderId) {
			try {
				$this->mysql->where("ORDER_ID", $_orderId);
				$payment_detail = $this->mysql->getOne("PAYMENT_M T1", "PAY_ID, ORDER_ID, PAY_TP_CD, PAY_PRICE, REG_DT");
		
				return $payment_detail;
			} catch (Exception $e) {
				debug_mesg("E", __CLASS__, __FUNCTION__, __LINE__, $this->TAG . "[getPaymentDetail()] : " . $e->getMessage());
		
				if(getErrorCode($e->getMessage()) != null) {
					throw new Exception($e->getMessage());
				}
		
				throw new Exception("SERVER_INTERNAL_ERROR");
			}
		}
		
		function cancelShopBooking($_shopId, $_bookingId, $_cancelFrom) {
			try {				
				$updateData = array(
					"ORDER_TP_CD" => "001"
				);
				
				$this->mysql->where("SHOP_ID", $_shopId)->where("ORDER_ID", $_bookingId);
				$ret = $this->mysql->update("order_m", $updateData);
				if(!$ret) {
					debug_mesg("E", __CLASS__, __FUNCTION__, __LINE__, $this->TAG . "[cancelShopBooking()] : " . $this->mysql->getLastError());
					throw new Exception("FAIL_TO_CANCEL_BOOKING");
				}
				
				if($_cancelFrom == "customer") {
					$this->mysql->where("T1.ORDER_ID", $_bookingId);
					$this->mysql->join("USER_M T2", "T1.USER_ID = T2.USER_ID", "INNER");
					$orderDetail = $this->mysql->getOne("ORDER_M T1", "T1.ORDER_ID, T1.SHOP_ID, T1.PDT_ID, T1.USER_ID, T1.MANAGER_ID, T1.USER_NM, T1.USER_PHONE1, T1.ORDER_DT, T1.ORDER_TP_CD, T1.PAY_TP_CD, T1.ORDERED_PRICE,
																	T1.REG_DT, T1.ORDER_GUBUN_CD, T2.USER_REAL_NM");
					if(!isset($orderDetail)) {
						return;
					}
					
					$pdtDetail = $this->getShopProductDetail($_shopId, $orderDetail["PDT_ID"]);
					if(!isset($pdtDetail)) {
						return;
					}
					
					$managerGcmIdList = $this->getShopManagerGcmIdList($_shopId);
					
					if(isset($managerGcmIdList)) {
						$notification = array(
							"title" => "상품 예약 취소",
							"body" => $pdtDetail["PDT_NM"] . "상품 예약이 취소되었습니다."
						);
					
						$data = array(
							"messageType" => "shopBookingCancel",
							"customerName" => $orderDetail["USER_REAL_NM"],
							"bookingDate" => $orderDetail["ORDER_DT"],
							"productName" => $pdtDetail["PDT_NM"],
							"elapsedTime" => $pdtDetail["ELAPSE_TM"]
						);
					
						send_notification(GOOGLE_SERVER_KEY_MANAGER, $managerGcmIdList, $data, $notification);
					}
				}
			} catch (Exception $e) {
				debug_mesg("E", __CLASS__, __FUNCTION__, __LINE__, $this->TAG . "[cancelShopBooking()] : " . $e->getMessage());
		
				if(getErrorCode($e->getMessage()) != null) {
					throw new Exception($e->getMessage());
				}
		
				throw new Exception("SERVER_INTERNAL_ERROR");
			}
		}
		
		function addShopDayOff($_shopId, $_startDt, $_endDt, $_alldayYn) {
			try {
				if(!$this->hasBookingInTime($_shopId, $_startDt, $_endDt)) {
					throw new Exception("INVAILID_DAYOFF");
				}
				
				$newData = array(
					"SHOP_ID" => $_shopId,
					"START_DT" => $_startDt,
					"END_DT" => $_endDt,
					"ALLDAY_YN" => $_alldayYn,
					"REG_DT" => date("YmdHis"),
					"DEL_YN" => "N"
				);
		
				$ret = $this->mysql->insert("shop_offday_r", $newData);
				if(!$ret) {
					debug_mesg("E", __CLASS__, __FUNCTION__, __LINE__, $this->TAG . "[addShopDayOff()] : " . $this->mysql->getLastError());
					throw new Exception("FAIL_TO_SAVE_SHOP_DAYOFF");
				}
			} catch (Exception $e) {
				debug_mesg("E", __CLASS__, __FUNCTION__, __LINE__, $this->TAG . "[addShopDayOff()] : " . $e->getMessage());
		
				if(getErrorCode($e->getMessage()) != null) {
					throw new Exception($e->getMessage());
				}
		
				throw new Exception("SERVER_INTERNAL_ERROR");
			}
		}
		
		function editShopDayOff($_shopId, $_startDt, $_endDt, $_alldayYn) {
			try {
				if(!$this->hasBookingInTime($_shopId, $_startDt, $_endDt)) {
					throw new Exception("INVAILID_DAYOFF");
				}
				
				$updateData = array(
					"START_DT" => $_startDt,
					"END_DT" => $_endDt,
					"ALLDAY_YN" => $_alldayYn
				);
		
				$this->mysql->where("SHOP_ID", $_shopId);
				$ret = $this->mysql->update("shop_offday_r", $updateData);
				if(!$ret) {
					debug_mesg("E", __CLASS__, __FUNCTION__, __LINE__, $this->TAG . "[editShopDayOff()] : " . $this->mysql->getLastError());
					throw new Exception("FAIL_TO_EDIT_SHOP_DAYOFF");
				}
			} catch (Exception $e) {
				debug_mesg("E", __CLASS__, __FUNCTION__, __LINE__, $this->TAG . "[editShopDayOff()] : " . $e->getMessage());
		
				if(getErrorCode($e->getMessage()) != null) {
					throw new Exception($e->getMessage());
				}
		
				throw new Exception("SERVER_INTERNAL_ERROR");
			}
		}
		
		function hasBookingInTime($_shopId, $_startDt, $_endDt) {
			try {
				$count = $this->mysql->rawQuery("SELECT COUNT(1) AS cnt
										FROM (
											SELECT T1.ORDER_ID, T1.ORDER_DT AS START_DT, DATE_FORMAT(TIMESTAMP(T1.ORDER_DT) + INTERVAL T2.ELAPSE_TM MINUTE, '%Y%m%d%H%i%s') AS END_DT
											FROM order_m T1 INNER JOIN shop_product_m T2 ON T1.PDT_ID = T2.PDT_ID AND T2.SHOP_ID = " . $_shopId . "
											WHERE T1.SHOP_ID = " . $_shopId . " AND T1.ORDER_TP_CD IN ('000', '002', '003')) A
										WHERE A.START_DT BETWEEN '" . $_startDt . "' AND '" . $_endDt . "'
											OR A.END_DT BETWEEN '" . $_startDt . "' AND '" . $_endDt . "'");
				
				if(isset($count) && isset($count[0])) {
					return $count[0]["cnt"] == 0;
				} else {
					return false;
				}
			} catch (Exception $e) {
				debug_mesg("E", __CLASS__, __FUNCTION__, __LINE__, $this->TAG . "[hasBookingInTime()] : " . $e->getMessage());
		
				if(getErrorCode($e->getMessage()) != null) {
					throw new Exception($e->getMessage());
				}
		
				throw new Exception("SERVER_INTERNAL_ERROR");
			}
		}
		
		function addShopProduct($_shopId, $_pdtNm, $_pdtDesc, $_pdtTpCd, $_pdtIdx, $_pdtPrice, $_elapseTm, $_pdtAvg, $_pdtHit, $_eventId) {
			try {
				$newData = array(
					"SHOP_ID" => $_shopId,
					"PDT_NM" => $_pdtNm,
					"PDT_DESC" => $_pdtDesc,
					"PDT_TP_CD" => $_pdtTpCd,
					"PDT_IDX" => $_pdtIdx,
					"PDT_PRIC" => $_pdtPrice,
					"ELAPSE_TM" => $_elapseTm,
					"PDT_AVG" => $_pdtAvg,
					"PDT_HIT" => $_pdtHit,
					"REG_DT" => date("YmdHis"),
					"DEL_YN" => "N"
				);
		
				$ret = $this->mysql->insert("shop_product_m", $newData);
				if(!$ret) {
					debug_mesg("E", __CLASS__, __FUNCTION__, __LINE__, $this->TAG . "[addShopProduct()] : " . $this->mysql->getLastError());
					throw new Exception("FAIL_TO_SAVE_SHOP_PDT");
				}
				
				$pdtId = $this->mysql->getInsertId();
				
				if(isset($_eventId) && !empty($_eventId)) {
					$this->mysql->where("EVT_ID", $_eventId);
					$eventInfo = $this->mysql->getOne("EVENT_M", "EVT_ID, EVT_TP_CD, EVT_NM, EVT_DESC, EVT_IDX, EVT_DISC_PRIC, EVT_BAN_IMG, EVT_START_DT, EVT_END_DT, DEL_YN, REG_DT");
					if(isset($eventInfo)) {
						$newData = array(
							"SHOP_ID" => $_shopId,
							"EVT_ID" => $_eventId,
							"PDT_ID" => $pdtId,
							"EVT_DISCOUNT_PRIC" => $eventInfo["EVT_DISC_PRIC"],
							"EVT_DISP_NM" => $eventInfo["EVT_NM"],
							"EVT_DISP_DESC" => $eventInfo["EVT_DESC"],
							"REG_DT" => date("YmdHis"),
							"DEL_YN" => "N"
						);
						
						$ret = $this->mysql->insert("PRODUCT_EVENT_R", $newData);
						if(!$ret) {
							debug_mesg("E", __CLASS__, __FUNCTION__, __LINE__, $this->TAG . "[addShopProduct()] : " . $this->mysql->getLastError());
							throw new Exception("FAIL_TO_SAVE_SHOP_PDT");
						}
					}
				}
			} catch (Exception $e) {
				debug_mesg("E", __CLASS__, __FUNCTION__, __LINE__, $this->TAG . "[addShopProduct()] : " . $e->getMessage());
		
				if(getErrorCode($e->getMessage()) != null) {
					throw new Exception($e->getMessage());
				}
		
				throw new Exception("SERVER_INTERNAL_ERROR");
			}
		}
		
		function getLastPdtIdx($_shopId) {
			try {
				$this->mysql->where("SHOP_ID", $_shopId);
				$lastPdtIdx = $this->mysql->getValue("shop_product_m", "MAX(PDT_IDX)");
					
				return $lastPdtIdx;
			} catch (Exception $e) {
				debug_mesg("E", __CLASS__, __FUNCTION__, __LINE__, $this->TAG . "[getLastPdtIdx()] : " . $e->getMessage());
				throw new Exception("SERVER_INTERNAL_ERROR");
			}
		}
		
		function deleteShopProduct($_shopId, $_pdtId) {
			try {
				$updateData = array(
					"DEL_YN" => "Y"
				);
				
				$this->mysql->where("PDT_ID", intval($_pdtId))->where("SHOP_ID", $_shopId);
				$ret = $this->mysql->update("shop_product_m", $updateData);
				if(!$ret) {
					debug_mesg("E", __CLASS__, __FUNCTION__, __LINE__, $this->TAG . "[deleteShopProduct()] : " . $this->mysql->getLastError());
					throw new Exception("FAIL_TO_DELETE_SHOP_PDT");
				}
			} catch (Exception $e) {
				debug_mesg("E", __CLASS__, __FUNCTION__, __LINE__, $this->TAG . "[deleteShopProduct()] : " . $e->getMessage());
				throw new Exception("SERVER_INTERNAL_ERROR");
			}
		}
		
		function addShopEvent($_shopId, $_evtNm, $_evtType, $_evtDiscPrice, $_evtDesc) {
			try {
				$newData = array(
					"SHOP_ID" => $_shopId,
					"PDT_NM" => $_pdtNm,
					"PDT_DESC" => $_pdtDesc,
					"PDT_TP_CD" => $_pdtTpCd,
					"PDT_IDX" => $_pdtIdx,
					"PDT_PRIC" => $_pdtPrice,
					"ELAPSE_TM" => $_elapseTm,
					"PDT_AVG" => $_pdtAvg,
					"PDT_HIT" => $_pdtHit,
					"REG_DT" => date("YmdHis"),
					"DEL_YN" => "N"
				);
		
				$ret = $this->mysql->insert("shop_product_m", $newData);
				if(!$ret) {
					debug_mesg("E", __CLASS__, __FUNCTION__, __LINE__, $this->TAG . "[addShopEvent()] : " . $this->mysql->getLastError());
					throw new Exception("FAIL_TO_SAVE_SHOP_PDT");
				}
			} catch (Exception $e) {
				debug_mesg("E", __CLASS__, __FUNCTION__, __LINE__, $this->TAG . "[addShopEvent()] : " . $e->getMessage());
		
				if(getErrorCode($e->getMessage()) != null) {
					throw new Exception($e->getMessage());
				}
		
				throw new Exception("SERVER_INTERNAL_ERROR");
			}
		}
		
		function getTotalSalesAmountByYear($_shopId, $_year) {
			try {
				$this->mysql->join('ORDER_M T2', 'T1.ORDER_ID = T2.ORDER_ID', 'INNER');
				$this->mysql->join('SHOP_M T3', 'T2.SHOP_ID = T3.SHOP_ID', 'INNER');
				$this->mysql->where("T3.SHOP_ID", $_shopId)->where("LEFT(T1.REG_DT, 4)", $_year);
				$totalAmount = $this->mysql->getValue("payment_m T1", "SUM(T1.PAY_PRIC)");
				
				if(!isset($totalAmount) || empty($totalAmount)) {
					$totalAmount = 0;
				}
					
				return $totalAmount;
			} catch (Exception $e) {
				debug_mesg("E", __CLASS__, __FUNCTION__, __LINE__, $this->TAG . "[getTotalSalesAmountByYear()] : " . $e->getMessage());
				throw new Exception("SERVER_INTERNAL_ERROR");
			}
		}
		
		function getSalesMonthListByYear($_shopId, $_year) {
			try {
				$this->mysql->groupBy('LEFT(T1.REG_DT, 6)');
				$this->mysql->orderBy('month', 'asc');
				$this->mysql->join('ORDER_M T2', 'T1.ORDER_ID = T2.ORDER_ID', 'INNER');
				$this->mysql->join('SHOP_M T3', 'T2.SHOP_ID = T3.SHOP_ID', 'INNER');
				$this->mysql->where("T3.SHOP_ID", $_shopId)->where("LEFT(T1.REG_DT, 4)", $_year);
				$list = $this->mysql->get("payment_m T1", null, "LEFT(T1.REG_DT, 6) AS month, SUM(T1.PAY_PRIC) AS amount");
		
				if(!isset($list) || empty($list)) {
					$list = array();
				}
					
				return $list;
			} catch (Exception $e) {
				debug_mesg("E", __CLASS__, __FUNCTION__, __LINE__, $this->TAG . "[getSalesMonthListByYear()] : " . $e->getMessage());
				throw new Exception("SERVER_INTERNAL_ERROR");
			}
		}
		
		function getTotalSalesAmountByMonth($_shopId, $_month) {
			try {
				$this->mysql->join('ORDER_M T2', 'T1.ORDER_ID = T2.ORDER_ID', 'INNER');
				$this->mysql->join('SHOP_M T3', 'T2.SHOP_ID = T3.SHOP_ID', 'INNER');
				$this->mysql->where("T3.SHOP_ID", $_shopId)->where("LEFT(T1.REG_DT, 6)", $_month);
				$totalAmount = $this->mysql->getValue("payment_m T1", "SUM(T1.PAY_PRIC)");
		
				if(!isset($totalAmount) || empty($totalAmount)) {
					$totalAmount = 0;
				}
					
				return $totalAmount;
			} catch (Exception $e) {
				debug_mesg("E", __CLASS__, __FUNCTION__, __LINE__, $this->TAG . "[getTotalSalesAmountByMonth()] : " . $e->getMessage());
				throw new Exception("SERVER_INTERNAL_ERROR");
			}
		}
		
		function getSalesMonthListByMonth($_shopId, $_month) {
			try {
				$this->mysql->groupBy('LEFT(T1.REG_DT, 8)');
				$this->mysql->orderBy('day', 'asc');
				$this->mysql->join('ORDER_M T2', 'T1.ORDER_ID = T2.ORDER_ID', 'INNER');
				$this->mysql->join('SHOP_M T3', 'T2.SHOP_ID = T3.SHOP_ID', 'INNER');
				$this->mysql->where("T3.SHOP_ID", $_shopId)->where("LEFT(T1.REG_DT, 6)", $_month);
				$list = $this->mysql->get("payment_m T1", null, "LEFT(T1.REG_DT, 8) AS day, SUM(T1.PAY_PRIC) AS amount");
		
				if(!isset($list) || empty($list)) {
					$list = array();
				}
					
				return $list;
			} catch (Exception $e) {
				debug_mesg("E", __CLASS__, __FUNCTION__, __LINE__, $this->TAG . "[getSalesMonthListByMonth()] : " . $e->getMessage());
				throw new Exception("SERVER_INTERNAL_ERROR");
			}
		}
		
		function getSalesInfoByProduct($_shopId, $_month) {
			try {
				$this->mysql->groupBy('LEFT(T2.REG_DT, 6), T1.PDT_ID');
				$this->mysql->orderBy('T1.PDT_ID', 'asc');
				$this->mysql->join('ORDER_M T2', 'T1.SHOP_ID = T2.SHOP_ID AND T1.PDT_ID = T2.PDT_ID', 'RIGHT OUTER');
				$this->mysql->join('PAYMENT_M T3', 'T2.ORDER_ID = T3.ORDER_ID', 'RIGHT OUTER');
				$this->mysql->where("T1.SHOP_ID", $_shopId)->where("T1.DEL_YN", "N")->where("LEFT(T2.REG_DT, 6)", $_month);
				$list = $this->mysql->get("shop_product_m T1", null, "T1.PDT_ID, T1.PDT_NM, COUNT(1) AS cnt, CASE WHEN SUM(T3.PAY_PRIC) IS NULL THEN 0 ELSE SUM(T3.PAY_PRIC) END AS amount");
		
				if(!isset($list) || empty($list)) {
					$list = array();
				}
					
				return $list;
			} catch (Exception $e) {
				debug_mesg("E", __CLASS__, __FUNCTION__, __LINE__, $this->TAG . "[getSalesInfoByProduct()] : " . $e->getMessage());
				throw new Exception("SERVER_INTERNAL_ERROR");
			}
		}
		
		function getSalesInfoByOnOffline($_shopId, $_month) {
			try {
				$this->mysql->groupBy('LEFT(T1.REG_DT, 6), T1.ORDER_GUBUN_CD');
				$this->mysql->orderBy('T1.ORDER_GUBUN_CD', 'asc');
				$this->mysql->join('PAYMENT_M T2', 'T1.ORDER_ID = T2.ORDER_ID', 'INNER');
				$this->mysql->where("T1.SHOP_ID", $_shopId)->where("LEFT(T1.REG_DT, 6)", $_month);
				$list = $this->mysql->get("ORDER_M T1", null, "CASE WHEN T1.ORDER_GUBUN_CD = '000' THEN '온라인매출' ELSE '오프라인매출' END AS ORDER_GUBUN_TXT, COUNT(1) AS cnt, SUM(T2.PAY_PRIC) AS amount");
		
				if(!isset($list) || empty($list)) {
					$list = array();
				}
					
				return $list;
			} catch (Exception $e) {
				debug_mesg("E", __CLASS__, __FUNCTION__, __LINE__, $this->TAG . "[getSalesInfoByOnOffline()] : " . $e->getMessage());
				throw new Exception("SERVER_INTERNAL_ERROR");
			}
		}
		
		function getSalesInfoByPayType($_shopId, $_month) {
			try {
				$list = $this->mysql->rawQuery("select PAY_TP_TXT, SUM(cnt) AS cnt, SUM(amount) AS amount
												from (
													select CASE WHEN T2.PAY_TP_CD = '002' THEN '카드결제' ELSE '현금결제' END AS PAY_TP_TXT, COUNT(1) AS cnt, SUM(T2.PAY_PRIC) AS amount
													from ORDER_M T1 inner join PAYMENT_M T2 on T1.ORDER_ID = T2.ORDER_ID
													where T1.SHOP_ID = " . $_shopId . " and LEFT(T1.REG_DT, 6) = '" . $_month . "'
													group by LEFT(T1.REG_DT, 6), T2.PAY_TP_CD
													order by T2.PAY_TP_CD asc) a
												group by a.PAY_TP_TXT");
		
				if(!isset($list) || empty($list)) {
					$list = array();
				}
					
				return $list;
			} catch (Exception $e) {
				debug_mesg("E", __CLASS__, __FUNCTION__, __LINE__, $this->TAG . "[getSalesInfoByPayType()] : " . $e->getMessage());
				throw new Exception("SERVER_INTERNAL_ERROR");
			}
		}
		
		function getSalesInfoByMemberType($_shopId, $_month) {
			try {
				$list = $this->mysql->rawQuery("SELECT MEMBER_TYPE_TXT, SUM(cnt) AS cnt, SUM(amount) as amount
													FROM (												
													select CASE WHEN COUNT(1) > 1 THEN '기존회원' ELSE '신규회원' END AS MEMBER_TYPE_TXT, COUNT(1) AS cnt, SUM(T2.PAY_PRIC) AS amount
													from ORDER_M T1 inner join PAYMENT_M T2 on T1.ORDER_ID = T2.ORDER_ID
													where T1.SHOP_ID = " . $_shopId . " and LEFT(T1.REG_DT, 6) = '" . $_month . "'
													group by LEFT(T1.REG_DT, 6), T1.USER_ID
													order by T1.USER_ID asc) A
													GROUP BY A.MEMBER_TYPE_TXT");
			
				if(!isset($list) || empty($list)) {
					$list = array();
				}
					
				return $list;
			} catch (Exception $e) {
				debug_mesg("E", __CLASS__, __FUNCTION__, __LINE__, $this->TAG . "[getSalesInfoByPayType()] : " . $e->getMessage());
				throw new Exception("SERVER_INTERNAL_ERROR");
			}
		}
		
		function getSalesListByDate($_shopId, $_date) {
			try {
				$list = $this->mysql->rawQuery("SELECT T1.ORDER_ID, T1.SHOP_ID, T1.PDT_ID, T1.USER_ID, T1.MANAGER_ID, CASE WHEN T4.USER_NM IS NULL THEN T1.USER_NM ELSE T4.USER_NM END AS USER_NM, 
											CASE WHEN T4.USER_NM IS NULL THEN T1.USER_PHONE1 ELSE T4.USER_PHONE1 END AS USER_PHONE1, T1.ORDER_DT, T1.ORDER_TP_CD, T1.PAY_TP_CD,
											T1.ORDERED_PRICE, T1.REG_DT, CASE WHEN T1.ORDER_GUBUN_CD = '000' THEN 'on' ELSE 'off' END AS ORDER_GUBUN_TXT, T3.USER_NM AS MANAGER_NM, T5.PDT_NM, T5.PDT_PRIC,
											T2.PAY_PRIC, T7.SHOP_COUP_ID, T7.COUP_NM, T7.COUP_TP_CD, T7.COUP_DISP_PRIC
										FROM ORDER_M T1 INNER JOIN PAYMENT_M T2 ON T1.ORDER_ID = T2.ORDER_ID
											INNER JOIN USER_M T3 ON T1.MANAGER_ID = T3.USER_ID
											LEFT OUTER JOIN USER_M T4 ON T1.USER_ID = T4.USER_ID
											INNER JOIN SHOP_PRODUCT_M T5 ON T1.PDT_ID = T5.PDT_ID
											LEFT OUTER JOIN ORDER_COUP_R T6 ON T1.ORDER_ID = T6.ORDER_ID
											LEFT OUTER JOIN SHOP_COUPON_R T7 ON T6.SHOP_COUP_ID = T7.SHOP_COUP_ID
										WHERE T1.SHOP_ID = " . $_shopId . " AND LEFT(T2.REG_DT, 8) = '" . $_date . "'
										ORDER BY T1.REG_DT ASC;");
				
				if(!isset($list) || empty($list)) {
					$list = array();
				}
					
				return $list;
			} catch (Exception $e) {
				debug_mesg("E", __CLASS__, __FUNCTION__, __LINE__, $this->TAG . "[getSalesListByDate()] : " . $e->getMessage());
				throw new Exception("SERVER_INTERNAL_ERROR");
			}
		}
		
		function getTotalShopEventList() {
			try {
				$list = $this->mysql->rawQuery("SELECT EVT_ID, EVT_TP_CD, EVT_NM, EVT_DESC, EVT_IDX, EVT_DISC_PRIC, EVT_BAN_IMG, EVT_START_DT, EVT_END_DT, DEL_YN, REG_DT
												FROM EVENT_M
												WHERE DEL_YN = 'N'");
		
				if(!isset($list) || empty($list)) {
					$list = array();
				}
					
				return $list;
			} catch (Exception $e) {
				debug_mesg("E", __CLASS__, __FUNCTION__, __LINE__, $this->TAG . "[getTotalShopEventList()] : " . $e->getMessage());
				throw new Exception("SERVER_INTERNAL_ERROR");
			}
		}
		
		function getAvailableShopEventList($_shopId) {
			try {
				$list = $this->mysql->rawQuery("SELECT SHOP_ID, EVT_ID FROM SHOP_EVENT_R WHERE SHOP_ID = " . $_shopId);
		
				if(!isset($list) || empty($list)) {
					$list = array();
				}
					
				return $list;
			} catch (Exception $e) {
				debug_mesg("E", __CLASS__, __FUNCTION__, __LINE__, $this->TAG . "[getAvailableShopEventList()] : " . $e->getMessage());
				throw new Exception("SERVER_INTERNAL_ERROR");
			}
		}
		
		function deleteShopCoupon($_shopId, $_shopCouponId) {
			try {
				$updateData = array(
					"DEL_YN" => "Y"
				);
		
				$this->mysql->where("SHOP_ID", $_shopId)->where("SHOP_COUP_ID", $_shopCouponId);
				$ret = $this->mysql->update("shop_coupon_r", $updateData);
				if(!$ret) {
					debug_mesg("E", __CLASS__, __FUNCTION__, __LINE__, $this->TAG . "[deleteShopCoupon] : " . $this->mysql->getLastError());
					throw new Exception("FAIL_TO_DELETE_COUPON");
				}
			} catch (Exception $e) {
				debug_mesg("E", __CLASS__, __FUNCTION__, __LINE__, $this->TAG . "[deleteShopCoupon()] : " . $e->getMessage());
					
				if(getErrorCode($e->getMessage()) != null) {
					throw new Exception($e->getMessage());
				}
					
				throw new Exception("SERVER_INTERNAL_ERROR");
			}
		}
		
		function registerShopCoupon($_shopId, $_couponNm, $_startDt, $_endDt, $_couponTypeCd, $_couponDispPrice, $_useYn) {
			try {
				$newData = array(
					"SHOP_ID" => $_shopId,
					"COUP_NM" => $_couponNm,
					"START_DT" => $_startDt,
					"END_DT" => $_endDt,
					"COUP_TP_CD" => $_couponTypeCd,
					"COUP_DISP_PRIC" => $_couponDispPrice,
					"USE_YN" => $_useYn,
					"DEL_YN" => "N",
					"REG_DT" => date("YmdHis")
				);
		
				$ret = $this->mysql->insert("shop_coupon_r", $newData);
				if(!$ret) {
					debug_mesg("E", __CLASS__, __FUNCTION__, __LINE__, $this->TAG . "[registerShopCoupon] : " . $this->mysql->getLastError());
					throw new Exception("FAIL_TO_INSERT_COUPON");
				}
			} catch (Exception $e) {
				debug_mesg("E", __CLASS__, __FUNCTION__, __LINE__, $this->TAG . "[registerShopCoupon()] : " . $e->getMessage());
					
				if(getErrorCode($e->getMessage()) != null) {
					throw new Exception($e->getMessage());
				}
					
				throw new Exception("SERVER_INTERNAL_ERROR");
			}
		}
		
		function updateShopCoupon($_shopCouponId, $_shopId, $_couponNm, $_startDt, $_endDt, $_couponTypeCd, $_couponDispPrice, $_useYn) {
			try {
				$updateData = array(
					"SHOP_ID" => $_shopId,
					"COUP_NM" => $_couponNm,
					"START_DT" => $_startDt,
					"END_DT" => $_endDt,
					"COUP_TP_CD" => $_couponTypeCd,
					"COUP_DISP_PRIC" => $_couponDispPrice,
					"USE_YN" => $_useYn,
					"DEL_YN" => "N",
					"REG_DT" => date("YmdHis")
				);
		
				$this->mysql->where("SHOP_ID", $_shopId)->where("SHOP_COUP_ID", $_shopCouponId);
				$ret = $this->mysql->update("shop_coupon_r", $updateData);
				if(!$ret) {
					debug_mesg("E", __CLASS__, __FUNCTION__, __LINE__, $this->TAG . "[updateShopCoupon] : " . $this->mysql->getLastError());
					throw new Exception("FAIL_TO_UPDATE_COUPON");
				}
			} catch (Exception $e) {
				debug_mesg("E", __CLASS__, __FUNCTION__, __LINE__, $this->TAG . "[updateShopCoupon()] : " . $e->getMessage());
					
				if(getErrorCode($e->getMessage()) != null) {
					throw new Exception($e->getMessage());
				}
					
				throw new Exception("SERVER_INTERNAL_ERROR");
			}
		}
		
		function deleteShopSalesData($_shopId, $_orderId) {
			try {
				$this->mysql->where("ORDER_ID", $_orderId)->where("SHOP_ID", $_shopId);
				$orderCount = $this->mysql->getValue("ORDER_M", "COUNT(1)");
				if($orderCount <= 0) {
					debug_mesg("E", __CLASS__, __FUNCTION__, __LINE__, $this->TAG . "[deleteShopSalesData()] : " . $this->mysql->getLastError());
					throw new Exception("FAIL_TO_DELETE_SALES_DATA");
				}
				
				$this->mysql->where("ORDER_ID", $_orderId);
				$ret = $this->mysql->delete("payment_m");
				if(!$ret) {
					debug_mesg("E", __CLASS__, __FUNCTION__, __LINE__, $this->TAG . "[deleteShopSalesData()] : " . $this->mysql->getLastError());
					throw new Exception("FAIL_TO_DELETE_SALES_DATA");
				}
			} catch (Exception $e) {
				debug_mesg("E", __CLASS__, __FUNCTION__, __LINE__, $this->TAG . "[deleteShopSalesData()] : " . $e->getMessage());
				throw new Exception("SERVER_INTERNAL_ERROR");
			}
		}
		
		function getSalesDetail($_shopId, $_orderId) {
			try {
				$list = $this->mysql->rawQuery("SELECT T1.ORDER_ID, T1.SHOP_ID, T1.PDT_ID, T1.USER_ID, T1.MANAGER_ID, T3.USER_ACCOUNT_ID,
													CASE WHEN T3.USER_NM IS NULL THEN T1.USER_NM ELSE T3.USER_NM END AS USER_NM,
													CASE WHEN T3.USER_PHONE1 IS NULL THEN T1.USER_PHONE1 ELSE T3.USER_PHONE1 END AS USER_PHONE1,
													T1.ORDER_DT, T1.PAY_TP_CD, T1.ORDERED_PRICE, T1.ORDER_GUBUN_CD,
													T2.PAY_ID, T2.PAY_TP_CD, T2.PAY_PRIC, T4.USER_NM AS MANAGER_NM,
													T5.PDT_NM, T5.ELAPSE_TM, T7.SHOP_COUP_ID, T7.COUP_NM
												FROM ORDER_M T1 INNER JOIN PAYMENT_M T2 ON T1.ORDER_ID = T2.ORDER_ID
													LEFT OUTER JOIN USER_M T3 ON T1.USER_ID = T3.USER_ID
													INNER JOIN USER_M T4 ON T1.MANAGER_ID = T4.USER_ID
													INNER JOIN SHOP_PRODUCT_M T5 ON T1.PDT_ID = T5.PDT_ID
													LEFT OUTER JOIN ORDER_COUP_R T6 ON T1.ORDER_ID = T6.ORDER_ID
													LEFT OUTER JOIN SHOP_COUPON_R T7 ON T6.SHOP_COUP_ID = T7.SHOP_COUP_ID
												WHERE T1.ORDER_ID = " . $_orderId . " AND T1.SHOP_ID = " . $_shopId);
					
				if(!isset($list) || empty($list)) {
					return null;
				}
					
				return $list[0];
			} catch (Exception $e) {
				debug_mesg("E", __CLASS__, __FUNCTION__, __LINE__, $this->TAG . "[getSalesDetail()] : " . $e->getMessage());
				throw new Exception("SERVER_INTERNAL_ERROR");
			}
		}
		
		function editSalesData($_orderId, $_shopId, $_pdtId, $_managerId, $_orderDt, $_totalPrice, $_shopCouponId) {
			try {
				$pdtDetail = $this->getShopProductDetail($_shopId, $_pdtId);
				if(!isset($pdtDetail)) {
					throw new Exception("INVALID_PDT_ID");
				}
				
				$salesDetail = $this->getSalesDetail($_shopId, $_orderId);
				if(!isset($salesDetail)) {
					throw new Exception("FAIL_TO_UPDATE_SALES_DATA");
				}
		
				$updateData = array(
					"PDT_ID" => $_pdtId,
					"MANAGER_ID" => $_managerId,
					"ORDER_DT" => $_orderDt . "00",
					"ORDERED_PRICE" => $_totalPrice
				);
		
				$this->mysql->startTransaction();
				
				$this->mysql->where("ORDER_ID", $_orderId);
				$ret = $this->mysql->update("order_m", $updateData);
				if(!$ret) {
					debug_mesg("E", __CLASS__, __FUNCTION__, __LINE__, $this->TAG . "[editSalesData()] : " . $this->mysql->getLastError());
					throw new Exception("FAIL_TO_UPDATE_SALES_DATA");
				}
				
				$paymentUpdateData = array(
					"PAY_PRIC" => $_totalPrice
				);
				
				$this->mysql->where("ORDER_ID", $_orderId);
				$ret = $this->mysql->update("payment_m", $paymentUpdateData);
				if(!$ret) {
					debug_mesg("E", __CLASS__, __FUNCTION__, __LINE__, $this->TAG . "[editSalesData()] : " . $this->mysql->getLastError());
					throw new Exception("FAIL_TO_UPDATE_SALES_DATA");
				}
				
				if($_shopCouponId > 0) {
					$this->mysql->where("ORDER_ID", $_orderId);
					$shop_coupon_detail = $this->mysql->getOne("ORDER_COUP_R", "ORDER_ID, SHOP_COUP_ID, USER_ID");
					
					if(isset($shop_coupon_detail)) {
						$updateData = array(
							"SHOP_COUP_ID" => $_shopCouponId
						);
						
						$this->mysql->where("ORDER_ID", $_orderId);
						$ret = $this->mysql->update("ORDER_COUP_R", $updateData);
						if(!$ret) {
							debug_mesg("E", __CLASS__, __FUNCTION__, __LINE__, $this->TAG . "[editSalesData()] : " . $this->mysql->getLastError());
							throw new Exception("FAIL_TO_UPDATE_SALES_DATA");
						}
					} else {
						$newData = array(
							"ORDER_ID" => $_orderId,
							"SHOP_COUP_ID" => $_shopCouponId,
							"USER_ID" => $salesDetail["USER_ID"]
						);
						
						$ret = $this->mysql->insert("ORDER_COUP_R", $newData);
						if(!$ret) {
							debug_mesg("E", __CLASS__, __FUNCTION__, __LINE__, $this->TAG . "[editSalesData()] : " . $this->mysql->getLastError());
							throw new Exception("FAIL_TO_UPDATE_SALES_DATA");
						}
					}
				}
				
				$this->mysql->commit();
			} catch (Exception $e) {
				$this->mysql->rollback();
				
				debug_mesg("E", __CLASS__, __FUNCTION__, __LINE__, $this->TAG . "[saveBooking()] : " . $e->getMessage());
					
				if(getErrorCode($e->getMessage()) != null) {
					throw new Exception($e->getMessage());
				}
					
				throw new Exception("SERVER_INTERNAL_ERROR");
			}
		}
		
		function updateShopInfoTab1($_shopId, $_addedPdtList, $_deletedPdtList, $_selectedEventList, $_deletedShopImages) {
			try {
				$this->mysql->startTransaction();
				
				foreach ($_addedPdtList as $key => $row) {
					$this->addShopProduct($_shopId, $row["PDT_NM"], $row["PDT_DESC"], $row["PDT_TP_CD"], $key, $row["PDT_PRIC"], $row["ELAPSE_TM"], 0, 0, $row["EVT_ID"]);
				}
				
				foreach ($_deletedPdtList as $key => $row) {
					$this->deleteShopProduct($_shopId, $row["PDT_ID"]);
				}
				
				$cols = array();
				foreach ($_deletedShopImages as $row) {
					$cols[$row] = "";
				}
				
				if(count($cols) > 0) {
					$this->mysql->where("SHOP_ID", $_shopId);
					$this->mysql->update("SHOP_M", $cols);
				}
				
				$this->mysql->where("SHOP_ID", $_shopId);
				$this->mysql->delete("SHOP_EVENT_R");
				
				foreach ($_selectedEventList as $key => $row) {
					$ret = $this->mysql->insert("SHOP_EVENT_R", array("SHOP_ID" => $_shopId, "EVT_ID" => $row));
				}
			
				$this->mysql->commit();
			} catch (Exception $e) {
				$this->mysql->rollback();
			
				debug_mesg("E", __CLASS__, __FUNCTION__, __LINE__, $this->TAG . "[updateShopInfoTab1()] : " . $e->getMessage());
					
				if(getErrorCode($e->getMessage()) != null) {
					throw new Exception($e->getMessage());
				}
					
				throw new Exception("SERVER_INTERNAL_ERROR");
			}
		}
		
		function updateShopInfoTab2($_shopId, $_shopAddr, $_shopPostalCd, $_shopLat, $_shopLng, $_shopTel, $_shopMobile, $_shopOpTime, $_shopOpTime2, $_shopEtc, $_deletedShopImages, $_shopLocCd) {
			try {
				$this->mysql->startTransaction();
				
				$cols = array();
				foreach ($_deletedShopImages as $row) {
					$cols[$row] = "";
				}
				
				if(count($cols) > 0) {
					$this->mysql->where("SHOP_ID", $_shopId);
					$this->mysql->update("SHOP_M", $cols);
				}
				
				if(!isset($_shopLocCd) || empty($_shopLocCd)) {
					$_shopLocCd = '';
				}
				
				$updateData = array(
					"SHOP_ADDR" => $_shopAddr,
					"SHOP_POSTAL_CD" => $_shopPostalCd,
					"SHOP_LAT" => $_shopLat,
					"SHOP_LNG" => $_shopLng,
					"SHOP_TEL" => $_shopTel,
					"SHOP_MOBILE" => $_shopMobile,
					"SHOP_OP_TIME" => $_shopOpTime,
					"SHOP_OP_TIME2" => $_shopOpTime2,
					"SHOP_ETC" => $_shopEtc,
					"SHOP_LOC_CD" => $_shopLocCd
				);
				
				$this->mysql->where("SHOP_ID", $_shopId);
				$ret = $this->mysql->update("SHOP_M", $updateData);
				
				if(!$ret) {
					debug_mesg("E", __CLASS__, __FUNCTION__, __LINE__, $this->TAG . "[updateShopInfoTab2()] : " . $this->mysql->getLastError());
					throw new Exception("FAIL_TO_UPDATE_SHOP_DATA");
				}
				
				$this->mysql->commit();
			} catch (Exception $e) {
				$this->mysql->rollback();
				
				debug_mesg("E", __CLASS__, __FUNCTION__, __LINE__, $this->TAG . "[updateShopInfoTab2()] : " . $e->getMessage());
					
				if(getErrorCode($e->getMessage()) != null) {
					throw new Exception($e->getMessage());
				}
					
				throw new Exception("SERVER_INTERNAL_ERROR");
			}
		}
		
		function updateShopImage($_shopId, $_imageColumn, $_shopImg) {
			try {
				$updateData = array(
					$_imageColumn => $_shopImg
				);
		
				$this->mysql->where("SHOP_ID", $_shopId);
				$ret = $this->mysql->update("SHOP_M", $updateData);
		
				if(!$ret) {
					debug_mesg("E", __CLASS__, __FUNCTION__, __LINE__, $this->TAG . "[updateShopImage()] : " . $this->mysql->getLastError());
					throw new Exception("FAIL_TO_UPDATE_SHOP_DATA");
				}
			} catch (Exception $e) {
				debug_mesg("E", __CLASS__, __FUNCTION__, __LINE__, $this->TAG . "[updateShopImage()] : " . $e->getMessage());
					
				if(getErrorCode($e->getMessage()) != null) {
					throw new Exception($e->getMessage());
				}
					
				throw new Exception("SERVER_INTERNAL_ERROR");
			}
		}
		
		function deleteReview($_shopId, $_cmtId) {
			try {
				$updateData = array(
					"DEL_YN" => "Y"
				);
 
				$this->mysql->where("SHOP_ID", $_shopId)->where("CMT_ID", $_cmtId);
				$ret = $this->mysql->update("SHOP_COMMENT_S", $updateData);
				if(!$ret) {
					debug_mesg("E", __CLASS__, __FUNCTION__, __LINE__, $this->TAG . "[deleteReview()] : " . $this->mysql->getLastError());
					throw new Exception("FAIL_TO_DELETE_REVIEW");
				}
			} catch (Exception $e) {
				debug_mesg("E", __CLASS__, __FUNCTION__, __LINE__, $this->TAG . "[deleteReview()] : " . $e->getMessage());
				throw new Exception("SERVER_INTERNAL_ERROR");
			}
		}
		
		function registerReviewReply($_shopId, $_cmtId, $_reply) {
			try {
				$newData = array(
					"CMT_ID" => $_cmtId,
					"SHOP_ID" => $_shopId,
					"RPL_TITLE" => "",
					"RPL_DESC" => $_reply,
					"RPL_GRADE" => 0,
					"REG_DT" => date("YmdHis"),
					"DEL_YN" => "N"
				);
		
				$ret = $this->mysql->insert("SHOP_COMMENT_REPLY_S", $newData);
				if(!$ret) {
					debug_mesg("E", __CLASS__, __FUNCTION__, __LINE__, $this->TAG . "[registerReviewReply()] : " . $this->mysql->getLastError());
					throw new Exception("FAIL_TO_SAVE_REVIEW_REPLY");
				}
				
				$newData["RPL_ID"] = $this->mysql->getInsertId();
				
				return $newData;
			} catch (Exception $e) {
				debug_mesg("E", __CLASS__, __FUNCTION__, __LINE__, $this->TAG . "[registerReviewReply()] : " . $e->getMessage());
				throw new Exception("SERVER_INTERNAL_ERROR");
			}
		}
		
		function updateReviewReply($_shopId, $_cmtId, $_rplId, $_reply) {
			try {
				$updateData = array(
					"RPL_TITLE" => "",
					"RPL_DESC" => $_reply
				);
		
				$this->mysql->where("SHOP_ID", $_shopId)->where("CMT_ID", $_cmtId)->where("RPL_ID", $_rplId);
				$ret = $this->mysql->update("SHOP_COMMENT_REPLY_S", $updateData);
				if(!$ret) {
					debug_mesg("E", __CLASS__, __FUNCTION__, __LINE__, $this->TAG . "[updateReviewReply()] : " . $this->mysql->getLastError());
					throw new Exception("FAIL_TO_UPDATE_REVIEW_REPLY");
				}
		
				return $this->getReviewReplyDetail($_shopId, $_cmtId, $_rplId);
			} catch (Exception $e) {
				debug_mesg("E", __CLASS__, __FUNCTION__, __LINE__, $this->TAG . "[updateReviewReply()] : " . $e->getMessage());
				throw new Exception("SERVER_INTERNAL_ERROR");
			}
		}
		
		function getReviewReplyDetail($_shopId, $_cmtId, $_rplId) {
			try {
				$this->mysql->where("SHOP_ID", $_shopId)->where("CMT_ID", $_cmtId)->where("RPL_ID", $_rplId)->where("DEL_YN", "N");
				$reply_detail = $this->mysql->getOne("SHOP_COMMENT_REPLY_S", "RPL_ID, CMT_ID, SHOP_ID, USER_ID, RPL_TITLE, RPL_DESC, RPL_GRADE, REG_DT");
		
				return $reply_detail;
			} catch (Exception $e) {
				debug_mesg("E", __CLASS__, __FUNCTION__, __LINE__, $this->TAG . "[getReviewReplyDetail()] : " . $e->getMessage());
				throw new Exception("SERVER_INTERNAL_ERROR");
			}
		}
		
		function deleteReviewReply($_shopId, $_cmtId, $_rplId) {
			try {
				$updateData = array(
					"DEL_YN" => "Y"
				);
		
				$this->mysql->where("SHOP_ID", $_shopId)->where("CMT_ID", $_cmtId)->where("RPL_ID", $_rplId);
				$ret = $this->mysql->update("SHOP_COMMENT_REPLY_S", $updateData);
				if(!$ret) {
					debug_mesg("E", __CLASS__, __FUNCTION__, __LINE__, $this->TAG . "[deleteReviewReply()] : " . $this->mysql->getLastError());
					throw new Exception("FAIL_TO_DELETE_REVIEW");
				}
			} catch (Exception $e) {
				debug_mesg("E", __CLASS__, __FUNCTION__, __LINE__, $this->TAG . "[deleteReviewReply()] : " . $e->getMessage());
				throw new Exception("SERVER_INTERNAL_ERROR");
			}
		}
		
		function getLastPortfolioIndex($_shopId) {
			try {
				$this->mysql->where("SHOP_ID", $_shopId)->where("DEL_YN", "N");
				$lastIdx = $this->mysql->getValue("shop_portfolio_m", "MAX(PORT_IDX)");
			
				return $lastIdx;
			} catch (Exception $e) {
				debug_mesg("E", __CLASS__, __FUNCTION__, __LINE__, $this->TAG . "[getLastPortfolioIndex()] : " . $e->getMessage());
				throw new Exception("SERVER_INTERNAL_ERROR");
			}
		}
		
		function isDuplicateHashTag($_tagNm) {
			try {
				$this->mysql->where("TAG_NM", $_tagNm);
				$tagCount = $this->mysql->getValue("hashtag_m", "COUNT(1)");
					
				return $tagCount > 0;
			} catch (Exception $e) {
				debug_mesg("E", __CLASS__, __FUNCTION__, __LINE__, $this->TAG . "[isDuplicateHashTag()] : " . $e->getMessage());
				throw new Exception("SERVER_INTERNAL_ERROR");
			}
		}
		
		function savePortfolio($_shopId, $_portImg1, $_portNm, $_portDesc, $_portTpCd, $_tags) {
			try {
				$lastIdx = $this->getLastPortfolioIndex($_shopId);
				
				$newData = array(
					"SHOP_ID" => $_shopId,
					"PORT_NM" =>$_portNm,
					"PORT_DESC" => $_portDesc,
					"PORT_IMG1" => $_portImg1,
					"PORT_TP_CD" => $_portTpCd,
					"PORT_IDX" => $lastIdx,
					"DEL_YN" => "N",
					"REG_DT" => date("YmdHis")
				);
				
				$this->mysql->startTransaction();
		
				$ret = $this->mysql->insert("shop_portfolio_m", $newData);
				if(!$ret) {
					debug_mesg("E", __CLASS__, __FUNCTION__, __LINE__, $this->TAG . "[savePortfolio()] : " . $this->mysql->getLastError());
					throw new Exception("FAIL_TO_INSERT_PORTFOLIO");
				}
				
				$portId = $this->mysql->getInsertId();
				
				if(isset($_tags) && !empty($_tags)) {
					$tagList = explode(" ", $_tags);
					
					foreach($tagList as $row) {
						if(!$this->isDuplicateHashTag($row)) {
							$newData = array("TAG_NM" => $row);
							$ret = $this->mysql->insert("hashtag_m", $newData);
							if(!$ret) {
								debug_mesg("E", __CLASS__, __FUNCTION__, __LINE__, $this->TAG . "[savePortfolio()] : " . $this->mysql->getLastError());
								throw new Exception("FAIL_TO_INSERT_PORTFOLIO");
							}
						}
						
						$newData = array("TAG_NM" => $row, "PORT_ID" => $portId);
						$ret = $this->mysql->insert("portfolio_hashtag_r", $newData);
						if(!$ret) {
							debug_mesg("E", __CLASS__, __FUNCTION__, __LINE__, $this->TAG . "[savePortfolio()] : " . $this->mysql->getLastError());
							throw new Exception("FAIL_TO_INSERT_PORTFOLIO");
						}
					}
				}
				
				$this->mysql->commit();
			} catch (Exception $e) {
				$this->mysql->rollback();
				
				debug_mesg("E", __CLASS__, __FUNCTION__, __LINE__, $this->TAG . "[savePortfolio()] : " . $e->getMessage());
				throw new Exception("SERVER_INTERNAL_ERROR");
			}
		}
		
		function registerPayment($_shopId, $_bookingId) {
			try {
				$this->mysql->where("ORDER_ID", $_bookingId);
				$orderDetail = $this->mysql->getOne("ORDER_M", "ORDER_ID, SHOP_ID, PDT_ID, USER_ID, MANAGER_ID, USER_NM, USER_PHONE1, ORDER_DT, ORDER_TP_CD, PAY_TP_CD, ORDERED_PRICE, REG_DT, ORDER_GUBUN_CD");
				
				$this->mysql->where("ORDER_ID", $_bookingId);
				$paymentDetail = $this->mysql->getOne("PAYMENT_M", "PAY_ID, ORDER_ID, PAY_TP_CD, PAY_PRIC, REG_DT");
				
				$this->mysql->startTransaction();
				
				$updateData = array(
					"ORDER_TP_CD" => "003"
				);
				
				$this->mysql->where("ORDER_ID", $_bookingId);
				$ret = $this->mysql->update("ORDER_M", $updateData);
				if(!$ret) {
					debug_mesg("E", __CLASS__, __FUNCTION__, __LINE__, $this->TAG . "[registerPayment() - update] : " . $this->mysql->getLastError());
					throw new Exception("FAIL_TO_UPDATE_SALES_DATA");
				}
				
				if(isset($paymentDetail)) {
					$updateData = array(
						"PAY_TP_CD" => "000",
						"PAY_PRIC" => $orderDetail["ORDERED_PRICE"]
					);
					
					$this->mysql->where("PAY_ID", $paymentDetail["PAY_ID"])->where("ORDER_ID", $paymentDetail["ORDER_ID"]);
					$ret = $this->mysql->update("PAYMENT_M", $updateData);
					if(!$ret) {
						debug_mesg("E", __CLASS__, __FUNCTION__, __LINE__, $this->TAG . "[registerPayment() - update] : " . $this->mysql->getLastError());
						throw new Exception("FAIL_TO_UPDATE_SALES_DATA");
					}
				} else {
					$newData = array(
						"ORDER_ID" => $orderDetail["ORDER_ID"],
						"PAY_TP_CD" => "000",
						"PAY_PRIC" => $orderDetail["ORDERED_PRICE"],
						"REG_DT" => date("YmdHis")
					);
					
					$ret = $this->mysql->insert("PAYMENT_M", $newData);
					if(!$ret) {
						debug_mesg("E", __CLASS__, __FUNCTION__, __LINE__, $this->TAG . "[registerPayment() - insert] : " . $this->mysql->getLastError());
						throw new Exception("FAIL_TO_UPDATE_SALES_DATA");
					}
				}
				
				$this->mysql->where("SHOP_ID", $_shopId)->where("USER_ID", $orderDetail["USER_ID"]);
				$shopUserGradeDetail = $this->mysql->getOne("SHOP_USER_GRADE_R", "SHOP_ID, USER_ID, SHOP_GR_CD, SHOP_GR_MEMO, INTERCEPTION_YN, REG_DT");
				
				if(!isset($shopUserGradeDetail)) {
					$newData = array(
						"SHOP_ID" => $_shopId,
						"USER_ID" => $orderDetail["USER_ID"],
						"SHOP_GR_CD" => "000",
						"SHOP_GR_MEMO" => "",
						"INTERCEPTION_YN" => "N",
						"REG_DT" => date("YmdHis")
					);
			
					$ret = $this->mysql->insert("SHOP_USER_GRADE_R", $newData);
					if(!$ret) {
						debug_mesg("E", __CLASS__, __FUNCTION__, __LINE__, $this->TAG . "[registerPayment() - SHOP_USER_GRADE_R insert] : " . $this->mysql->getLastError());
						throw new Exception("FAIL_TO_JOIN");
					}
				}
				
				$this->mysql->commit();
			} catch (Exception $e) {
				$this->mysql->rollback();
				
				debug_mesg("E", __CLASS__, __FUNCTION__, __LINE__, $this->TAG . "[registerPayment()] : " . $e->getMessage());
				throw new Exception("SERVER_INTERNAL_ERROR");
			}
		}
		
		function savePortfolioSort($_shopId, $_portId, $_portIdx) {
			try {
				$updateData = array(
					"PORT_IDX" => $_portIdx
				);
		
				$this->mysql->where("SHOP_ID", $_shopId)->where("PORT_ID", $_portId);
		
				$ret = $this->mysql->update("shop_portfolio_m", $updateData);
				if(!$ret) {
					debug_mesg("E", __CLASS__, __FUNCTION__, __LINE__, $this->TAG . "[savePortfolioSort()] : " . $this->mysql->getLastError());
					throw new Exception("FAIL_TO_SAVE_SORT_PORTFOLIO");
				}
			} catch (Exception $e) {
				debug_mesg("E", __CLASS__, __FUNCTION__, __LINE__, $this->TAG . "[savePortfolioSort()] : " . $e->getMessage());
					
				if(getErrorCode($e->getMessage()) != null) {
					throw new Exception($e->getMessage());
				}
					
				throw new Exception("SERVER_INTERNAL_ERROR");
			}
		}
		
		function checkShopHoliday($_shopId, $_date) {
			try {
				$startDate = $_date . "000000";
				$endDate = $_date . "235959";
				$this->mysql->where("SHOP_ID", $_shopId)->where("START_DT", $startDate)->where("END_DT", $endDate)->where("DEL_YN", "N");
				$count = $this->mysql->getValue("shop_offday_r", "COUNT(1)");
					
				return $count > 0;
			} catch (Exception $e) {
				debug_mesg("E", __CLASS__, __FUNCTION__, __LINE__, $this->TAG . "[checkShopHoliday()] : " . $e->getMessage());
				throw new Exception("SERVER_INTERNAL_ERROR");
			}
		}
		
		function checkShopHolidayHours($_shopId, $_bookingDate, $_elapsedTime) {
			try {
				$this->mysql->where("SHOP_ID", $_shopId)->where($_bookingDate . " BETWEEN START_DT AND END_DT")->where("DEL_YN", "N");
				$count = $this->mysql->getValue("shop_offday_r", "COUNT(1)");
					
				return $count > 0;
			} catch (Exception $e) {
				debug_mesg("E", __CLASS__, __FUNCTION__, __LINE__, $this->TAG . "[checkShopHolidayHours()] : " . $e->getMessage());
				throw new Exception("SERVER_INTERNAL_ERROR");
			}
		}
		
		function deleteShopHoliday($_shopId, $_offId) {
			try {
				$this->mysql->where("SHOP_ID", $_shopId)->where("OFF_ID", $_offId);
				$ret = $this->mysql->update("shop_offday_r", array("DEL_YN" => "Y"));
					
				if(!$ret) {
					debug_mesg("E", __CLASS__, __FUNCTION__, __LINE__, $this->TAG . "[checkShopHolidayHours()] : " . $this->mysql->getLastError());
					throw new Exception("FAIL_TO_DELETE_DAYOFF");
				}
			} catch (Exception $e) {
				debug_mesg("E", __CLASS__, __FUNCTION__, __LINE__, $this->TAG . "[checkShopHolidayHours()] : " . $e->getMessage());
				throw new Exception("SERVER_INTERNAL_ERROR");
			}
		}
		
		function deleteShopPortfolio($_shopId, $_portId) {
			try {
				$this->mysql->where("SHOP_ID", $_shopId)->where("PORT_ID", $_portId);
				$ret = $this->mysql->update("shop_portfolio_m", array("DEL_YN" => "Y"));
					
				if(!$ret) {
					debug_mesg("E", __CLASS__, __FUNCTION__, __LINE__, $this->TAG . "[deleteShopPortfolio()] : " . $this->mysql->getLastError());
					throw new Exception("FAIL_TO_DELETE_PORTFOLIO");
				}
			} catch (Exception $e) {
				debug_mesg("E", __CLASS__, __FUNCTION__, __LINE__, $this->TAG . "[deleteShopPortfolio()] : " . $e->getMessage());
				throw new Exception("SERVER_INTERNAL_ERROR");
			}
		}
		
		function getShopBookingListForPush() {
			try {
				$startDate = date("YmdH") . "0000";
				$endDate = date("YmdH", strtotime("+6 hours")) . "0000";
		
				$this->mysql->where("T1.SHOP_ID", $_shopId)->where("T1.ORDER_DT BETWEEN '" . $startDate . "' AND '" . $endDate . "'");
				$this->mysql->orderBy('T1.ORDER_DT', 'asc');
				$this->mysql->join('SHOP_PRODUCT_M T2', 'T1.PDT_ID = T2.PDT_ID', 'INNER');
				$this->mysql->join('USER_M T3', 'T1.USER_ID = T3.USER_ID', 'INNER');
				$shop_booking_list = $this->mysql->get("ORDER_M T1", null, "T1.ORDER_ID, T1.ORDER_DT, T2.PDT_NM, T2.ELAPSE_TM, T3.USER_NM");
		
				if(!isset($shop_booking_list)) {
					$shop_booking_list = array();
				}
		
				return $shop_booking_list;
			} catch (Exception $e) {
				debug_mesg("E", __CLASS__, __FUNCTION__, __LINE__, $this->TAG . "[getShopBookingList()] : " . $e->getMessage());
		
				if(getErrorCode($e->getMessage()) != null) {
					throw new Exception($e->getMessage());
				}
		
				throw new Exception("SERVER_INTERNAL_ERROR");
			}
		}
	}
?>
