<?php
	/**
	 * require_once_classes(Array("CUtil"));
	 */
	function require_once_classes($_classNameArr) {
		foreach ($_classNameArr AS $className) {
			require_once(CONF_PATH_CLASS . $className . ".php");
		}
	}
	
	/**
	 * require_once_services(Array("CUtil"));
	 */
	function require_once_services($_classNameArr) {
		foreach ($_classNameArr AS $className) {
			require_once(CONF_PATH_SERVICE . $className . ".php");
		}
	}

	/**
	 * debug_mesg("E", __CLASS__, __FUNCTION__, __LINE__, "can not connect to memcached");
	 */
	function debug_mesg($_type, $_class, $_function, $_line, $_mesg) {
		if (!CONF_DEBUG) {
			return;
		}
	
		if ((strcmp($_type, "E") == 0) && !CONF_DEBUG_ERROR) {
			return;
		} else if ((strcmp($_type, "I")) && !CONF_DEBUG_INFO) {
			return;
		}
	
// 		$_mesg .= "@1-". whereCalled(1);
// 		$_mesg .= "@2-". whereCalled(2);
// 		$_mesg .= "@3-". whereCalled(3);
		
		if (CONF_DEBUG_DISPLAY) {
			printf("%s|%s|%s|%s|%d|%s<br/>\r\n", date("Y-m-d H:i:s"), $_type, $_class, $_function, $_line, substr($_mesg, 0, 51200));
		}
	
		if (CONF_DEBUG_SAVE) {
			$save_file = CONF_DEBUG_SAVE_FILE_ETC;
			if ($_type == "E") {
				$save_file = CONF_DEBUG_SAVE_FILE_ERROR;
			}
			
			$f = sprintf("%s%d_%s", CONF_PATH_DEBUG_FILE, date("YmdH"), $save_file);
	
			$is_new = true;
			if (file_exists($f)) {
				$is_new = false;
			}
	
			$fp = fopen($f, "a");
	
			if ($is_new) {
				chmod($f, 0666);
			}
	
			if ($fp) {
				printf("%s|%s|%s|%s|%d|%s\r\n", date("Y-m-d H:i:s"), $_type, $_class, $_function, $_line, substr($_mesg, 0, 51200));
				fclose($fp);
			}
		}
	}

	function whereCalled($_level = 1) {
		$trace = debug_backtrace();
		
		if (!isset($trace[$_level])) {
			return "nothing";
		}
			
		$file = $trace[$_level]["file"];
		$line = $trace[$_level]["line"];
		$object = isset($trace[$_level]["object"]) ? $trace[$_level]["object"] : null;
		
		$args = "";
		if ($trace[$_level]["args"]) {
			unSet($trace[$_level]["args"]["_query"]);
			$args = serialize($trace[$_level]["args"]);
		}
	
		if (is_object($object)) {
			$object = get_class($object);
		}
	
		return "Where called [" . $_level . "]: line " . $line . " of " . $object . " with args[" . $args . "] (in " . $file . ")";
	}

	function snc_return($_result = "OK", $_message = "SUCCESS", $_data = null) {
	    return json_encode(Array("result" => $_result, "message" => $_message, "data" => $_data), JSON_UNESCAPED_UNICODE);
	}
	
	function snc_error($_message) {
		global $_SNC_ERROR;
	
		$_SNC_ERROR = Array('message' => $_message);
	}
	
	function generateMemberToken($_seed) {
		$options = [
			'cost' => 11,
			'salt' => mcrypt_create_iv(22, MCRYPT_DEV_URANDOM),
		];
		
		return password_hash($_seed, PASSWORD_BCRYPT, $options);
	}
	
	function getErrorCode($_errorCode) {
		global $ERROR_CODES;
		
		if(isset($ERROR_CODES[$_errorCode])) {
			return $ERROR_CODES[$_errorCode];
		}
		
		return null;
	}
	
	function get_web_page( $url )
	{
		$options = array(
			CURLOPT_RETURNTRANSFER => true,     // return web page
			CURLOPT_HEADER         => false,    // don't return headers
			CURLOPT_FOLLOWLOCATION => true,     // follow redirects
			CURLOPT_ENCODING       => "",       // handle all encodings
			CURLOPT_USERAGENT      => "spider", // who am i
			CURLOPT_AUTOREFERER    => true,     // set referer on redirect
			CURLOPT_CONNECTTIMEOUT => 120,      // timeout on connect
			CURLOPT_TIMEOUT        => 120,      // timeout on response
			CURLOPT_MAXREDIRS      => 10,       // stop after 10 redirects
			CURLOPT_SSL_VERIFYPEER => false     // Disabled SSL Cert checks
		);
	
		$ch      = curl_init( $url );
		curl_setopt_array( $ch, $options );
		$content = curl_exec( $ch );
		$err     = curl_errno( $ch );
		$errmsg  = curl_error( $ch );
		$header  = curl_getinfo( $ch );
		curl_close( $ch );
	
		$header['errno']   = $err;
		$header['errmsg']  = $errmsg;
		$header['content'] = $content;
		
		return $content;
	}
	
	function send_notification ($_serverKey, $_gcmId, $_data, $_notification)
	{
		$url = 'https://fcm.googleapis.com/fcm/send';
		
		$headers = array (
			'Authorization: key=' . $_serverKey,
			'Content-Type: application/json'
		);
		
		$_notification["click_action"] = "FCM_PLUGIN_ACTIVITY";
		
		$fields = array (
			'data' => $_data,
			'notification' => $_notification,
			'priority' => "high"
		);
		
		if(is_array($_gcmId)) {
			$fields['registration_ids'] = $_gcmId;
		} else {
			$fields['to'] = $_gcmId;
		}
		
		$fields = json_encode ($fields);
		
		$ch = curl_init ();
		curl_setopt ( $ch, CURLOPT_URL, $url );
		curl_setopt ( $ch, CURLOPT_POST, true );
		curl_setopt ( $ch, CURLOPT_HTTPHEADER, $headers );
		curl_setopt ( $ch, CURLOPT_RETURNTRANSFER, true );
		curl_setopt ( $ch, CURLOPT_POSTFIELDS, $fields );
		
		$result = curl_exec ( $ch );
		if ($result === FALSE) {
			//die('FCM Send Error: ' . curl_error($ch));
		}
		
		curl_close ( $ch );
		return $result;
	}
?>
