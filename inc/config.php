<?php
	session_start();
	
	date_default_timezone_set('Asia/Seoul');
	
	/**
	 * DATABASE
	 */
	define("DEF_MYSQL_IP",		"dev.sncinteractive.com");
	define("DEF_MYSQL_PORT",	3306);
	define("DEF_MYSQL_USER",	"binjari");
	define("DEF_MYSQL_PASS",	"binjari.1!");
	define("DEF_MYSQL_DB",		"binjaridb");
	
	/**
	 * PATH
	 */
// 	define('CONF_SERVER_HOST',		"http://" . $_SERVER['HTTP_HOST']);
	
	define('CONF_URL_ROOT',			"/");
	define('CONF_PATH_ASSETS', 		CONF_URL_ROOT . "assets/");
	define('CONF_URL_UPLOAD',		CONF_URL_ROOT . "upload/");
	
	define("CONF_PATH_ROOT",		"/Users/youngmincho/01_Projects/10_Binjari/Sources/WEB/BinjariServer" . CONF_URL_ROOT);
	define("CONF_PATH_CLASS",		CONF_PATH_ROOT . "inc/classes/");
	define("CONF_PATH_SERVICE",		CONF_PATH_ROOT . "inc/classes/services/");
	define('CONF_PATH_DEBUG_FILE',	CONF_PATH_ROOT . "logs/");
	define('CONF_PATH_UPLOAD',		CONF_PATH_ROOT . "upload/");
	
	/**
	 * DEBUG
	 */
	define('CONF_DEBUG',			true);
	define('CONF_DEBUG_INFO',		false);
	define('CONF_DEBUG_ERROR',		true);
	define('CONF_DEBUG_DISPLAY',	false);
	define('CONF_DEBUG_SAVE',		true);
	define('CONF_DEBUG_SAVE_FILE_ERROR',	'snc_error.log');
	define('CONF_DEBUG_SAVE_FILE_ETC',		'snc_debug.log');
	
	define("CONF_SITE_TITLE", "빈자리");
	
	/**
	 * ERROR CODES
	 */
	include_once("error_codes.php");
	
	/**
	 * DEFINE
	 */
	include_once('config_master.php');
	
	/**
	 * GLOBAL FUNCTIONS
	 */
	include_once("global_function.php");
?>