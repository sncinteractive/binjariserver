<?php
	header("Content-Type:text/html; charset=utf-8");

	try {
		require_once ("D:/Binjari/BinjariServer/inc/config.php");
		require_once_classes(array("CMysqlManager"));
		require_once_services(array("CPushService"));
	
		$mysql_manager = new CMysqlManager();
		$pushService = new CPushService($mysql_manager->getDb());
		
		$pushList = $pushService->getPushList();
		
		if(isset($pushList)) {
			foreach ($pushList as $row) {
				$googleServerKey = "";
				if($row["SEND_TARGET"] == "customer") {
					$googleServerKey = GOOGLE_SERVER_KEY_USER;
				} else if($row["SEND_TARGET"] == "manager") {
					$googleServerKey = GOOGLE_SERVER_KEY_MANAGER;
				}
				
				$notification = array(
					"title" => $row["TITLE"],
					"body" => $row["BODY"]
				);
				
				$data = array();
				$dataArray = explode(",", $row["DATA"]);
				foreach ($dataArray as $val) {
					$dataItem = explode(":", $val);
					$data[$dataItem[0]] = $dataItem[1];
				}
				
				send_notification($googleServerKey, $row["USER_GCM_ID"], $data, $notification);
			}
		}
	} catch (Exception $e) {
		debug_mesg("E", __CLASS__, __FUNCTION__, __LINE__, "api member error : " . $e->getMessage());
	
		exit;
	}
?>