<?php
header('Content-Type: text/html; charset=euc-kr');
//**************************************************************************
// 파일명 : hs_cnfrm_popup1.php
//
// 본인확인서비스 요청 정보 입력 화면
//
//**************************************************************************

$token = $_REQUEST["phoneAuthToken"];
?>
<html>
	<body>
		<form name="form1" action="/okname/confirm_2.php?phoneAuthToken=<?php echo $token; ?>" method="post">
			<input type="hidden" name="rqst_caus_cd" value="00"/>
			<input type="hidden" name="in_tp_bit" value="0"/>
		</form>
		
		<!-- 본인확인 처리결과 정보 -->
		<form name="kcbResultForm" method="post" >
			<input type="hidden" name="mem_id" 					value="" 	/>
			<input type="hidden" name="svc_tx_seqno"			value=""	/>
			<input type="hidden" name="rqst_caus_cd"			value="" 	/>
			<input type="hidden" name="result_cd" 				value="" 	/>
			<input type="hidden" name="result_msg" 				value="" 	/>
			<input type="hidden" name="cert_dt_tm" 				value="" 	/>
			<input type="hidden" name="di" 						value="" 	/>
			<input type="hidden" name="ci" 						value="" 	/>
			<input type="hidden" name="name" 					value="" 	/>
			<input type="hidden" name="birthday" 				value="" 	/>
			<input type="hidden" name="sex" 					value="" 	/>
			<input type="hidden" name="nation" 					value="" 	/>
			<input type="hidden" name="tel_com_cd" 				value="" 	/>
			<input type="hidden" name="tel_no" 					value="" 	/>
			<input type="hidden" name="return_msg" 				value="" 	/>
		</form>
		<script>
			document.form1.submit();
		</script>
	</body>
</html>
